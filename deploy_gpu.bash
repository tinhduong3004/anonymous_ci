#!/bin/zsh
#set -e
set -xe
MACHINE=zdeploy@10.50.9.12
PROJECT_NAME="anonymoususer"

[[ $(basename $PWD) == $PROJECT_NAME ]]

MACHINE="${MACHINE:-zdeploy@10.50.9.12}"
UPLOADDIR="/zserver/java-projects/apps/${PROJECT_NAME}"
DESTURL="${MACHINE}:${UPLOADDIR}"

RSYNC=rsync
SSH=(ssh $MACHINE)
MKDIR=($SSH mkdir -p)

$MKDIR $UPLOADDIR

# Uploading luigi source code
if [[ -d luigi-orchestration ]]; then
    $RSYNC -r luigi-orchestration "${DESTURL}/"
    # Upload a copy of the luigi source code, making it like a virtual environment
    # implemented with PYTHONPATH
    # Note@26/06/2017 Assume that we already have luigi on the server
#    $RSYNC -r $HOME/vng/repos/luigi/luigi "${DESTURL}/luigi-orchestration/"
else
    notfound Luigi
fi

#Uploading cron files
if [[ -d crond-schedules ]]; then
    # Note, to see the installed crontab on the end machine run:
    #    $ crontab -l | less
    () {
        local CRON_TEMPDIR=$(mktemp --dir)
        # echo "DEBUG: Will create cronfiles in $CRON_TEMPDIR"
        for cronfile in crond-schedules/*.gpucron
        do
            local newfile="${CRON_TEMPDIR}/${PROJECT_NAME}__$(basename $cronfile)"
            # echo "DEBUG: Will create cronfile in $newfile"
            echo "##### BEGIN file $(realpath $cronfile)" > $newfile
            cat $cronfile >> $newfile
            echo "##### END file $(realpath $cronfile)" >> $newfile
        done
        # Delete the old files. This is crucial in case there's been renames as old
        # files will not be just merely overwritten.
        # $SSH "rm /home/zdeploy/cron.d/${PROJECT_NAME}__*.cron" || true
        # Now upload our new files.
        $RSYNC -r $CRON_TEMPDIR/*.gpucron "$MACHINE:/home/zdeploy/cron.d/"
        # Update the crontab for the zreader user
        $SSH 'cat /home/zdeploy/cron.d/*.*cron | crontab'
    }
else
    notfound Cron
fi

