// http://webcache.googleusercontent.com/a/29324977
// sbt-assembly is needed for having scopt included in the jar
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.7")
addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "5.2.4")
