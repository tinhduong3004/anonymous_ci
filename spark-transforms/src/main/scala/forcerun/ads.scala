package forcerun

import java.nio.file.{Files, Paths}

import org.apache.http.HttpEntity
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.DefaultHttpClient

import scala.io.Source

object ads {
  def getRestContentColumn(id: Int) : String = {

//    val httpClient = buildHttpClientWithProxy()
    var httpClient = new DefaultHttpClient
    val httpGet = buildAdtimaHttpGet(id)

    var entity: HttpEntity = null

    var content = "{data:{}}"
    try {
      val httpResponse = httpClient.execute(httpGet)
      entity = httpResponse.getEntity

      if (entity != null) {
        val inputStream = entity.getContent
        content = scala.io.Source.fromInputStream(inputStream).getLines.mkString
        inputStream.close
      }

      httpClient.getConnectionManager.shutdown
    } catch {
      case ex: Throwable => {
      }
    }
    content
  }

  def buildAdtimaHttpGet(contentId: Int): HttpGet = {
    val url = "https://api.adtima.vn/rest/banner/load?id="
    val httpGet = new HttpGet(url + contentId)

    val akey = "5oe2GQIW4OJlodRD93OM"
    val ti = System.currentTimeMillis().toString()
    val tk = md5Hash(ti + "@@stJm0e000" + akey)

    httpGet.setHeader("aKey", akey)
    httpGet.setHeader("ti", ti)
    httpGet.setHeader("tk", tk)
    httpGet
  }

  def md5Hash(text: String): String = {
    java.security.MessageDigest
      .getInstance("MD5")
      .digest(text.getBytes())
      .map(0xFF & _)
      .map {
        "%02x".format(_)
      }
      .foldLeft("") {
        _ + _
      }
  }

  def main(args: Array[String]): Unit = {
    val itemIds = Array(1281463,388168,388169,388178,388186,429550,4981784,5673329,5971865,5971867,6414712,6414744,6614135,6614158,6797681,6797684,6867435,6945988,6991401,7016028,7016066,7036044,7045042,7082971,7086241,7086243,7112701,7112767,7113583,7114739,7115337,7115566,7121545,7121778,7126570,7126606,7126743,7127158,7127251,7127258,7127334,7127338,7127431,7127555,7127556,7127557,7127558,7127560,7127561,7127562,7127563,7127567,7127568,7127569,7128495,7128535,7128538,7128558,7130073,7130598)
    itemIds.foreach(itemId=>println(itemId +"\t"+getRestContentColumn(itemId)))
//    val writer = Files.newBufferedWriter(Paths.get("/home/quydm/item_content_08.tsv"))
//    Source.fromFile("/home/quydm/item08.csv")
//      .getLines
//      .map(line=>line.toInt)
//      .foreach(itemId=>writer.write(itemId +"\t"+getRestContentColumn(itemId)+"\n"))
//    writer.close()
    println("Done")
  }
}
