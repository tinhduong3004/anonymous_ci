package forcerun

import com.vng.zalo.zte.rnd.anonymoususer.webvisitor.anonymous.hourly.RawExtract.columnNames
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.json4s._
import org.json4s.native.JsonMethods._

import scala.io.Source
/**
 * Created by quydm on 1/30/18.
 */

case class StorageContent(id: Long, content: String)

object InspectParquetFile {
  val rawLogSchema = Array("log_time", "app_name", "app_mode", "timestamp", "article_id",
    "article_pub_id", "article_cate_id", "user_id_from", "user_id_to", "referer",
    "user_agent", "client_ip", "client_type", "client_version", "client_block",
    "client_pos", "network_operator", "network_type", "device_model", "device_imei",
    "command", "sub_command", "params", "output_params", "result", "zalo_id", "source",
    "device_id", "type", "operator", "network", "model", "imei", "version", "ip", "port",
    "server_id", "router_name", "country", "birthday", "age", "gender", "living_location",
    "phonenumber_detail", "register_date", "avatar_version", "gy", "index", "server_index","_")


  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .master("local[*]")
      .appName("RR")
      .getOrCreate()
    val inputPath = "/home/quydm/Datasets/zep"
    val input = spark.read.format("csv").option("delimiter","\t").load(inputPath).toDF(rawLogSchema:_*)
    input.show()

  }
}
