package forcerun

import java.util.Date

object GlobalMerge {
  def classifyDuration(last: Long): String = {
      val now = new Date().getTime
      val duration = (now - last) * 1.0 / 86400000 // 24 * 60 * 60 * 1000
      duration match {
        case x if x <= 3 => "A03"
        case x if x <= 7 => "A07"
        case _ => "A30"
      }
  }

  def main(args: Array[String]): Unit = {
    val timeStamp = 1551340417882L
    println(classifyDuration(timeStamp))
  }
}
