package forcerun

import java.util.Date

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{avg, col, countDistinct, udf}
object StatPreprocess {

  def classifyDuration(last: Long): String = {
    val now = new Date().getTime
    val duration = (now - last) * 1.0 / 86400000 // 24 * 60 * 60 * 1000
    duration match {
      case x if x <= 2 => "D02"
      case x if x <= 7 => "D07"
      case _ => "D30"
    }
  }

  def classifyPlatform(globalId: String): String = {
    val prefix = globalId.substring(0,4)
    prefix match {
      case "2000" => "Web"
      case "2001" => "iOs"
      case "2002" => "Android"
      case "2003" => "WP"
      case _ => "Others"
    }
  }
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("Test")
      .master("local[*]")
      .getOrCreate()

    val udfClassifyDuration = udf((last:Long) => classifyDuration(last))
    val udfClassifyPlatform = udf((globalId:String) => classifyPlatform(globalId))
    val filePath = "/home/quydm/Datasets/anonymous/map.parquet"
    val df = spark.read.parquet(filePath)
    df.withColumn("duration",udfClassifyDuration(col("created_time")))
      .withColumn("platform",udfClassifyPlatform(col("anonymous_gid")))
      .filter(col("zalo_gender") === col("dmp_gender"))
      .groupBy("zalo_id","dmp_id","anonymous_id","duration","platform")
      .agg(
        avg(col("zalo_gender")).alias("zalo_gender"),
        countDistinct(col("zalo_gender")).alias("zalo_gender_distinct"),
        avg(col("dmp_gender")).alias("dmp_gender"),
        countDistinct(col("dmp_gender")).alias("dmp_gender_distinct"),
        avg(col("anonymous_gender")).alias("anonymous_gender"),
        countDistinct(col("anonymous_gender")).alias("anonymous_gender_distinct"),
        avg(col("zalo_age")).alias("zalo_age"),
        countDistinct(col("zalo_age")).alias("zalo_age_distinct"),
        avg(col("dmp_age")).alias("dmp_age"),
        countDistinct(col("dmp_age")).alias("dmp_age_distinct"),
        avg(col("anonymous_age")).alias("anonymous_age"),
        countDistinct(col("anonymous_age")).alias("anonymous_age_distinct")
      )
      .show(200,false)
  }
}
