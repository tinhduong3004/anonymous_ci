package forcerun
import java.sql.Timestamp
import java.time.{Instant, ZoneId, ZoneOffset}

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.{col, collect_list, struct, udf}
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.concurrent.TimeUnit
import java.util.{Calendar, Locale}

import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods

object Grb {
  def extractField(jsonStr: String,field:String): String = {
    try {
      if (jsonStr != null) {
        implicit val formats = DefaultFormats
        val json = JsonMethods.parse(jsonStr)
        (json \ field).extractOrElse[String]("")
      } else {
        ""
      }
    }catch{
      case e:Exception => ""
    }
  }

  def extractArray(jsonStr: String,field:String): String = {
    try {
      if (jsonStr != null) {
        implicit val formats = DefaultFormats
        val json = JsonMethods.parse(jsonStr)
        (json \ field).extractOrElse[Seq[String]](Seq()).mkString(",")
      } else {
        ""
      }
    }catch{
      case e:Exception => ""
    }
  }

  val udfGlobalIdExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"zDeviceId")
  })

  val udfSrcExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"src")
  })

  val udfConNameExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"conName")
  })

  val udfConTypeExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"conType")
  })

  val udfIdExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"id")
  })

  val udfQualityExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"quality")
  })
  val udfDurationExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"duration")
  })
  val udfPosExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"pos")
  })

  val udfVolumeExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"volume")
  })

  val udfArtistExtractor = udf((jsonStr:String) => {
    extractArray(jsonStr,"artistIds")
  })
  val udfComposerExtractor = udf((jsonStr:String) => {
    extractArray(jsonStr,"composerIds")
  })
  val udfGenresExtractor = udf((jsonStr:String) => {
    extractArray(jsonStr,"genreIds")
  })
  val udfListenTime = udf((jsonStr:String) => {
    val timestamp = extractField(jsonStr,"timestamp")
    val instant = Instant.ofEpochMilli(timestamp.toLong)
    println(timestamp.toLong)
    println(instant)
    DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
                      .format(instant)
  })

  val currentTime = Calendar.getInstance().getTimeInMillis
  val actions = List(5,6,18,19)
  def calculateAgeFromBirthday = udf((birthDay: Long) => {
    TimeUnit.MILLISECONDS.toDays(currentTime - birthDay * 1000) / 365.0
  })

  def readAndJoin(inputData: DataFrame): DataFrame = {
    inputData
      .drop("global_id")
      .withColumn("global_id",udfGlobalIdExtractor(col("device_info")))
      .filter(!col("global_id").startsWith("3000."))
      .filter(col("action_id").isin(actions:_*))
      .withColumn("src",udfSrcExtractor(col("action_params")))
      .withColumn("song_id",udfIdExtractor(col("action_params")))
      .withColumn("composer_ids",udfComposerExtractor(col("action_params")))
      .withColumn("artist_ids",udfArtistExtractor(col("action_params")))
      .withColumn("gendre_ids",udfGenresExtractor(col("action_params")))
      .withColumn("con_name",udfConNameExtractor(col("action_params")))
      .withColumn("con_type",udfConTypeExtractor(col("action_params")))
      .withColumn("quality",udfQualityExtractor(col("action_params")))
      .withColumn("duration",udfDurationExtractor(col("action_params")))
      .withColumn("pos",udfPosExtractor(col("action_params")))
      .withColumn("volume",udfVolumeExtractor(col("action_params")))
      .withColumn("listen_time",udfListenTime(col("action_params")))
      .selectExpr(
        "log_time",
        "platform",
        "cast (action_id as int) action_id",
        "cast (song_id as int) song_id",
        "composer_ids",
        "artist_ids",
        "gendre_ids",
        "con_name",
        "con_type",
        "quality",
        "cast (duration as int) duration",
        "cast (pos as int) pos",
        "cast (volume as int) volume",
        "listen_time"
      )
  }
  def main(args: Array[String]): Unit = {
//    val spark = SparkSession.builder()
//      .appName("Test")
//      .master("local[*]")
//      .getOrCreate()
//    val df = spark.read.parquet("/home/quydm/Datasets/tmp/mp3.parquet")
//    readAndJoin(df).show()
    val timestamp = 1583978408000l
    val instant = Instant.ofEpochMilli(timestamp)
    println(timestamp.toLong)
    println(instant)
    val dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS").withZone(ZoneId.systemDefault()).format(instant)

      print(dtf.format(instant))

  }
}
