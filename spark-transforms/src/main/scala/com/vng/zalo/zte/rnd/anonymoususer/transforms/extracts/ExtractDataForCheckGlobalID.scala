package com.vng.zalo.zte.rnd.anonymoususer.transforms.extracts

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, udf}
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods

object ExtractDataForCheckGlobalID extends GenericIOTransformation{
  def extractGlobalIdInMP3StatV4(jsonStr: String): String = {
    if (jsonStr != null) {
      implicit val formats = DefaultFormats
      val json = JsonMethods.parse(jsonStr)
      (json \ "zDeviceId").extractOrElse[String]("")
    } else {
      ""
    }
  }
  val udfExtractor = udf((jsonStr: String) => extractGlobalIdInMP3StatV4(jsonStr))

  def fromSetToString(set: Set[String]): String = {
    set.mkString(",")
  }

  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Daily Aggregated Data (Extract)")
      .master("local[4]")
      .getOrCreate()
    val ZEP_BAOMOI_TRACKING = spark.read.parquet(inputPaths.head).select("device_id").withColumnRenamed("device_id", "global_id")
    val GLOBAL_ID_FOR_DEVICE = spark.read.parquet(inputPaths(1)).select("g_id").withColumnRenamed("g_id", "global_id")
    val ZMP3_API_STATS = spark.read.parquet(inputPaths(2)).select("global_id")
    val ZINGTV_ACCESS = spark.read.parquet(inputPaths(3)).select("visitor").withColumnRenamed("visitor", "global_id")
    val ZINGTV_LOG_V2 = spark.read.parquet(inputPaths(4)).select("visitor_id").withColumnRenamed("visitor_id", "global_id")
    val CENTRALIZED_GET_VISITOR = spark.read.parquet(inputPaths(5)).select("gid").withColumnRenamed("gid", "global_id")
    val ADTIMA_ADS_ACTION = spark.read.parquet(inputPaths(6)).select("uid").withColumnRenamed("uid", "global_id")
    val ADTIMA_ADS_ACTION_V2 = spark.read.parquet(inputPaths(7)).select("uid").withColumnRenamed("uid", "global_id")
    val ZMP3_WEB_STATS = spark.read.parquet(inputPaths(8)).select("global_id")
    val ZNEWS_LOG_EVENT = spark.read.option("delimiter", "\t").option("header", false).csv(inputPaths(9)).select("_c13").withColumnRenamed("_c13", "global_id")
    val inputData = ZEP_BAOMOI_TRACKING.union(GLOBAL_ID_FOR_DEVICE).union(ZMP3_API_STATS).union(ZINGTV_ACCESS).union(ZINGTV_LOG_V2)
    .union(CENTRALIZED_GET_VISITOR).union(ZNEWS_LOG_EVENT)
    .union(ADTIMA_ADS_ACTION).union(ADTIMA_ADS_ACTION_V2).union(ZMP3_WEB_STATS)
    val a = inputData.filter(col("global_id").startsWith("200")).distinct().count()
    val d = inputData.filter(col("global_id").startsWith("3000")).distinct().count()
    val inputAnonymous = spark.read.parquet(inputPaths(10)).select("global_id")
    val b = inputAnonymous.distinct().count()
    val inputIdentified = spark.read.parquet(inputPaths(11)).select("global_id")
    val c = inputIdentified.distinct().count()
    println("GlobalID from logs (200x): " + a +" -----------------	GlobalID from anonymous:" + b +"\nGlobalID from logs (3000): "+ d+" -----------------	GlobalID from identified: " +c)
 }
}