package com.vng.zalo.zte.rnd.anonymoususer.zingmp3.identified.hourly

import com.vng.zalo.zte.rnd.anonymoususer.zingmp3.GenericExtract
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object RawExtract extends GenericExtract {

  def readAndJoin(globalIdMap: DataFrame, inputData: DataFrame): DataFrame = {
    inputData
      .drop("global_id")
      .withColumn("global_id",udfGlobalIdExtractor(col("device_info")))
      .filter(col("global_id").startsWith("3000."))
      .filter(col("action_id").isin(actions:_*))
      .drop("zalo_id","birthday","gender")
      .join(globalIdMap.withColumnRenamed("src", "source"), Seq("global_id"), "left")
      .withColumn("age", calculateAgeFromBirthday(col("birthday")))
      .withColumn("src",udfSrcExtractor(col("action_params")))
      .withColumn("song_id",udfIdExtractor(col("action_params")))
      .withColumn("composer_ids",udfComposerExtractor(col("action_params")))
      .withColumn("artist_ids",udfArtistExtractor(col("action_params")))
      .withColumn("gendre_ids",udfGenresExtractor(col("action_params")))
      .withColumn("con_name",udfConNameExtractor(col("action_params")))
      .withColumn("con_type",udfConTypeExtractor(col("action_params")))
      .withColumn("quality",udfQualityExtractor(col("action_params")))
      .withColumn("duration",udfDurationExtractor(col("action_params")))
      .withColumn("pos",udfPosExtractor(col("action_params")))
      .withColumn("volume",udfVolumeExtractor(col("action_params")))
      .withColumn("listen_time",udfListenTime(col("action_params")))
      .withColumn("type",udfTypeExtractor(col("action_params")))
      .selectExpr(
        "log_time",
        "zalo_id",
        "cast (user_id as int) zing_id",
        "age",
        "gender",
        "platform",
        "cast (action_id as int) action_id",
        "cast (song_id as int) song_id",
        "composer_ids",
        "artist_ids",
        "gendre_ids",
        "con_name",
        "con_type",
        "quality",
        "cast (duration as int) duration",
        "cast (pos as int) pos",
        "cast (volume as int) volume",
        "cast (listen_time as string) action_time",
        "birthday",
        "cast (type as int) type"
      )
  }


  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Extract Centralized Get Visitor")
      .getOrCreate()
    val inputDatas = inputPaths.grouped(2) map {
      inputPath =>
        readAndJoin(spark.read.parquet(inputPath(0)),
          spark.read.parquet(inputPath(1)))
    }
    val outputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    outputData
      .coalesce(8)
      .write
      .mode(SaveMode.Overwrite)
      .parquet(outputPath)
  }
}