package com.vng.zalo.zte.rnd.anonymoususer.device.identified

import java.text.SimpleDateFormat

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.json4s.native.JsonParser.parse
import org.json4s.native.Serialization.write
import org.json4s.{DefaultFormats, Formats}

abstract class GenericUnion extends GenericIOTransformation{
  def toEpocsTime(timestamp:String): Long = {
    new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").parse(timestamp).getTime
  }
  def toDeviceInfoSeq(jsonSeq:Seq[String]) : Seq[DeviceInfo] = {
    implicit val formats: Formats = DefaultFormats.withCompanions(classOf[DeviceInfo] -> DeviceInfo)
    jsonSeq map { case jsonStr:String => {parse(jsonStr).extract[DeviceInfo]}}
  }
  def toJsonSeq(deviceSeq:Seq[DeviceInfo]) : Seq[String] = {
    implicit val formats: Formats = DefaultFormats.withCompanions(classOf[DeviceInfo] -> DeviceInfo)
    deviceSeq.toSeq map { case device:DeviceInfo => write(device)}
  }
  def maxByLastUsed(s1: DeviceInfo, s2: DeviceInfo): DeviceInfo = if (s1.lastUsed > s2.lastUsed) s1 else s2
  def maxByStringSize(s1: String, s2: String): String = if (s1.length > s2.length) s1 else s2

  def reduceDeviceByOs(devices: Seq[DeviceInfo]): DeviceInfo = {
    var lastDevice = devices.reduceLeft(maxByLastUsed)
    // 'Cause sometime we couldn't get the brand name (!)
    val brandName = devices map (x=>x.brandName) reduceLeft(maxByStringSize)
    lastDevice.copy(brandName=brandName)
  }

  def reduceDeviceListByOs(devices: Seq[DeviceInfo] ): Seq[DeviceInfo] = {
    val groupByOs:Map[String,Seq[DeviceInfo]] = devices.groupBy(_.identifyOs())
    val deviceWithLatestTime = groupByOs.values map {case seqDevice => seqDevice.reduceLeft(maxByLastUsed)}
    deviceWithLatestTime.toSeq
  }
  def sortDescByLastUsed(deviceInfos:Seq[DeviceInfo]) : Seq[DeviceInfo] ={
    deviceInfos.sortBy(_.lastUsed)(Ordering[Long].reverse)
  }
  def reduceJsonList(jsonSeq:Seq[String]): (Seq[String],String) = {
    val reducedList = sortDescByLastUsed(
      reduceDeviceListByOs(
        toDeviceInfoSeq(jsonSeq)
      )
    )
    (toJsonSeq(reducedList),encodeDeviceList(reducedList))
  }

  def encodeDeviceList(deviceInfos:Seq[DeviceInfo]) : String = {
    (deviceInfos map { case deviceInfo: DeviceInfo => deviceInfo.identifyOsVersion() }
      map { case deviceId:String => deviceId.trim}
      reduceRight( (s1,s2) => s1+"#"+s2))
  }
}
