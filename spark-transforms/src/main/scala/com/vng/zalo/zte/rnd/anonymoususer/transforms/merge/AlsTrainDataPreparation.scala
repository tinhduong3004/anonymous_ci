package com.vng.zalo.zte.rnd.anonymoususer.transforms.merge

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.{col, lit, sum, udf}
import org.apache.spark.sql.{DataFrame, SparkSession}

object AlsAdsTrainDataPreparation extends GenericIOTransformation {
  val ConvertGender = udf((gender: Int) => -gender)

  val ConvertAge = udf((age: Double) => {
    if (age < 13) {
      -9912
    } else if (age <= 17) {
      -1317
    } else if (age <= 24) {
      -1824
    } else if (age <= 34) {
      -2534
    } else if (age <= 44) {
      -3544
    } else if (age <= 54) {
      -4554
    } else if (age <= 64) {
      -5564
    } else {
      -6599
    }
  })

  def genderTransform(inputData: DataFrame): DataFrame = {
    inputData.select("zalo_id", "gender")
      .distinct()
      .withColumn("item_id", ConvertGender(col("gender")))
      .withColumn("score", lit(1))
      .select("zalo_id", "item_id", "score")
  }
  def ageTransform(inputData: DataFrame): DataFrame = {
    inputData.select("zalo_id", "age")
      .withColumn("item_id", ConvertAge(col("age")))
      .withColumn("score", lit(1))
      .select("zalo_id", "item_id", "score")
      .distinct()
  }

  def itemTransform(inputData: DataFrame): DataFrame = {
    inputData.select("zalo_id", "itemid")
      .withColumnRenamed("itemid", "item_id")
      .withColumn("score", lit(1))
      .groupBy("zalo_id", "item_id")
      .agg(sum("score") as "score")
  }

  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Extract als data trainning")
      .getOrCreate()

    val inputData = spark.read
      .parquet(inputPaths: _*)
      .filter(col("gender")
        .isin(Seq(1, 2): _*))
      .filter(col("kind").isin(Seq("click"): _*))
    genderTransform(inputData).write.parquet(outputPath + "/gender")
    ageTransform(inputData).write.parquet(outputPath + "/age")
    itemTransform(inputData).write.parquet(outputPath + "/item")
  }
}
