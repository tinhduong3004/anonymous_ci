package com.vng.zalo.zte.rnd.anonymoususer.device.identified

case class DeviceInfo(brandName:String,deviceName:String,osName:String,osVersion:String,lastUsed:Long) {
  def identifyOs() = deviceName+"_"+osName
  def identifyOsVersion() = brandName+"_"+deviceName+"_"+osName+"_"+osVersion
}
