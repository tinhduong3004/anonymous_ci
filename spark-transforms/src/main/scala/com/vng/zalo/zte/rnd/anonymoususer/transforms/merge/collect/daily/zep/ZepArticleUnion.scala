package com.vng.zalo.zte.rnd.anonymoususer.transforms.merge.collect.daily.zep

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.{SaveMode, SparkSession}

object ZepArticleUnion extends GenericIOTransformation {

  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Merge articles for ... days")
      .getOrCreate()
    val inputDatas = spark.read.parquet(inputPaths: _*)
    val data = inputDatas
      .distinct()
//    print("Output length: " + data.count())
//    print(data.show(20))
    data
      .select("article_id", "keywords", "article_cate")
      .dropDuplicates()
      .coalesce(1)
      .write
      .mode(SaveMode.Overwrite)
      .parquet(outputPath)
  }
}
