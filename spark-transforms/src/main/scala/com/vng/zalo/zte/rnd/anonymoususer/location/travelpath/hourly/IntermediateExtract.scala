package com.vng.zalo.zte.rnd.anonymoususer.location.travelpath.hourly

import com.vng.zalo.zte.rnd.anonymoususer.location.travelpath.GenericTravelPath
import org.apache.spark.sql.{DataFrame, SparkSession}

object IntermediateExtract extends GenericTravelPath {
  def transformation(inputData: DataFrame): DataFrame = {
    import inputData.sparkSession.implicits._
    inputData.rdd.map(row => (row.getLong(1), row.getLong(0).toString + "~" + row.getLong(0).toString + "~" + "1" + "~" + row.getString(2)))
      .mapValues(x => List(x))
      .reduceByKey((a, b) => a ++ b)
      .mapValues(ls => listReduce(ls))
      .mapValues(ls => ls.mkString(","))
      .toDF()
  }
  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("HourlyExtractTravelpath")
      .getOrCreate()
    val inputDatas = inputPaths map { inputPath => spark.read.parquet(inputPath) }
    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = transformation(inputData)
    outputData.coalesce(16).write.parquet(outputPath)
  }
}