package com.vng.zalo.zte.rnd.anonymoususer.location.travelpath.daily

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object IP2LocationJoin extends GenericIOTransformation{
  val udfSplit = udf((info: String) => info.split(",").toList)

  def transformTravelPath(inputData: DataFrame): DataFrame = {
    inputData
      .withColumnRenamed("_1", "id")
      .withColumnRenamed("_2", "info")
      .withColumn("info", explode(udfSplit(col("info"))))
      .withColumn("_tmp", split(col("info"), "~"))
      .select(
        col("id"),
        col("_tmp").getItem(0).as("start"),
        col("_tmp").getItem(1).as("end"),
        col("_tmp").getItem(2).as("count"),
        col("_tmp").getItem(3).as("ip")).drop("_tmp", "info")
  }
  def transformJoinData(inputData: DataFrame): DataFrame = {
    val toString = udf((seq: Seq[String]) => seq.mkString(","))
    inputData
      .na
      .fill(0, Seq("administrationAreaID"))
      .withColumn(
        "concat",
        concat(col("start"), lit("_"),
          col("end"), lit("_"),
          col("count"), lit("_"),
          col("ip"), lit("_"),
          col("administrationAreaID"))).groupBy("id")
      .agg(collect_list("concat") as "info")
      .withColumn("info", toString(col("info")))
      .select("id", "info")
  }
  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("DailyUnionsTravelPath")
      .getOrCreate()

    val travel_path = transformTravelPath(spark.read.parquet(inputPaths(0)))
    val ip2Location = spark.read.parquet(inputPaths(1)).select("ip", "administrationAreaID")
    val outputData = transformJoinData(travel_path.join(ip2Location, Seq("ip"), "left"))
    outputData.write.mode(SaveMode.Overwrite).parquet(outputPath)
  }
}
