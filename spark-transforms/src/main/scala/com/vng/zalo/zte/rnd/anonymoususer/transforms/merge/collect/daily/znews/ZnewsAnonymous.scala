package com.vng.zalo.zte.rnd.anonymoususer.transforms.merge.collect.daily.znews

import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SaveMode, SparkSession}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.math.max

object ZnewsAnonymous extends GenericIOTransformation {

  val MAX_LEN = 500
  val RIGHT_HOURS = 24

  // Notice: PARTITIONS = 128. Changing this will cause disaster!
  val PARTITIONS = 128

  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    assert(inputPaths.size > RIGHT_HOURS)
    val spark = SparkSession.builder()
      .appName("Merge train data for 14 days")
//            .master("local[*]")
      .getOrCreate()

    val schema = new StructType()
      .add(StructField("global_id", StringType, true))
      .add(StructField("article_id", StringType, true))
      .add(StructField("client_os", StringType, true))
      .add(StructField("client_browser", StringType, true))
      .add(StructField("platform", StringType, true))
      .add(StructField("interval", StringType, true))

    val right = readAndClean(spark, inputPaths.slice(inputPaths.size - RIGHT_HOURS, inputPaths.size))
      .rdd
      .repartition(256)
      .map(r => (r.getString(0), (r.getString(1), r.getInt(2), r.getString(3), r.getString(4), r.getString(5))))
      .groupByKey()
      .mapValues(r => r.toList.slice(0, MAX_LEN).sorted)

    val left = readAndClean(spark, inputPaths.slice(0, inputPaths.size - RIGHT_HOURS))
      .rdd
      .repartition(256)
      .map(r => (r.getString(0), (r.getString(1), r.getInt(2), r.getString(3), r.getString(4), r.getString(5))))
      .groupByKey()
      .mapValues(r => r.toList.slice(max(0, r.size - MAX_LEN), r.size).sorted)
      .rightOuterJoin(right)
      .mapValues(r => concatRightOuterJoinResult(r._1, r._2))
      .mapValues(r => r.slice(max(0, r.size - MAX_LEN), r.size))
      .mapValues(r => (retrieveByIndex(r, 2),  retrieveByIndex(r, 3), retrieveByIndex(r, 4), retrieveByIndex(r, 5), retrieveByIndex(r, 1)))
      .map(r => Row(r._1, r._2._1, r._2._2, r._2._3, r._2._4, r._2._5))

    spark.createDataFrame(left, schema)
      .coalesce(PARTITIONS)
      .write
      .mode(SaveMode.Overwrite)
      .csv(outputPath)
  }

  def readAndClean(spark: SparkSession, inputPaths: Seq[String]): DataFrame = {
    spark.read.parquet(inputPaths: _*)
      .na.drop()
      .withColumn("time_at_minute", date_format(col("log_time"), "yyyy-MM-dd HH:mm"))
      .dropDuplicates(Seq("id", "article_id", "time_at_minute"))
      .filter(col("article_id")>1000)
      .selectExpr(
        "cast (id as string) global_id",
        "log_time",
        "cast (article_id as int) article_id",
        "cast (client_os as string) client_os",
        "cast (client_browser as string) client_browser",
        "cast (platform as string) platform"
      )
  }


  def retrieveByIndex(list: List[(Any, Any, Any, Any, Any)], index: Int): String = {
    var result = ListBuffer[String]()
    index match {
      case 1 => list.foreach(item => result += item._1.toString)
      case 2 => list.foreach(item => result += item._2.toString)
      case 3 => result += list.head._3.toString
      case 4 => result += list.head._4.toString
      case 5 => result += list.head._5.toString
    }
    if (index == 1)
      getTimeInterval(result.toArray.mkString(";"))
    else
      result.toArray.mkString(" ")
  }

  def concatRightOuterJoinResult(left: Option[List[(Any, Any, Any, Any, Any)]], right: List[(Any, Any, Any, Any, Any)]): List[(Any, Any, Any, Any, Any)] = {
    if (left.isEmpty)
      right
    else
      left.head ++ right
  }

  def batchDataReader(spark: SparkSession, inputPath: Seq[String]): DataFrame = {
    spark.read.parquet(inputPath:_*)
      .select("id", "article_id", "log_time")
      .withColumn("time_at_minute", date_format(col("log_time"), "yyyy-MM-dd HH:mm"))
      .dropDuplicates(Seq("id", "article_id", "time_at_minute"))
      .withColumnRenamed("id", "global_id")
  }

  def getTimeInterval(rawTimeList: String): String = {
    val timeFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
    val input = rawTimeList.split(";").map(x => timeFormater.parse(x))
    var resultList = List(timeFormater.format(input.head))
    for (i <- 1 until input.length) resultList = resultList :+ TimeUnit.MILLISECONDS.toMillis(input(i).getTime - input(i - 1).getTime).toString
    resultList.mkString(";")
  }

  def concatIntArrays(firstarray: mutable.WrappedArray[Int],
                      secondarray: mutable.WrappedArray[Int]) : mutable.WrappedArray[Int] =
  {
    if (firstarray != null & secondarray != null)
      firstarray ++ secondarray
    else if (firstarray == null)
      secondarray
    else
      firstarray
  }

  def concatStringArrays(firstarray: mutable.WrappedArray[String],
                         secondarray: mutable.WrappedArray[String]) : mutable.WrappedArray[String] =
  {
    if (firstarray != null & secondarray != null)
      firstarray ++ secondarray
    else if (firstarray == null)
      secondarray
    else
      firstarray
  }
}
