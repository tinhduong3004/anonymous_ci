package com.vng.zalo.zte.rnd.anonymoususer.unusual.run
import com.vng.zalo.zte.rnd.dbingestionmw.client.KeyRemoveClient
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMW
import com.vng.zing.zudm_userinfocache.thrift.ZUDM_UserInfoCacheThrift
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.thrift.protocol.TBinaryProtocol
import org.apache.thrift.transport.{TFramedTransport, TSocket}

object KeyRemove {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("RemoveUnUsedKey")
//            .master("local[*]")
      .getOrCreate()
    val path = "/data/jobs/rnd/user_profile/sample/thanhnm3/tmp/old_firstname"
    val data = spark.read.parquet(path)

    data.foreachPartition(
      iterator => {
        val socket = new TSocket("10.30.65.168", 18782)
        val transport = new TFramedTransport(socket)
        val protocol = new TBinaryProtocol(transport)
        val client = new DBIngestionMW.Client(protocol)
        transport.open()
        val output = iterator.map(
          row => {
            val zaloId = row.getString(0).toInt
            client.removeKey("Anonymous@Mining",zaloId)
          }
        ).toList
        transport.close()
        output.iterator
      }
    )
  }
}
