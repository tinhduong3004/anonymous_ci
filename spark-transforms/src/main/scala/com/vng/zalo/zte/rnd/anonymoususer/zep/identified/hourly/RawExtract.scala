package com.vng.zalo.zte.rnd.anonymoususer.zep.identified.hourly

import java.util.Calendar
import java.util.concurrent.TimeUnit

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.{DataFrame, SparkSession}

object RawExtract extends GenericIOTransformation {
  val columnNamesBeforeJoin = Array("log_time", "global_id", "cmd_type", "client_type", "obj_id", "cate_id", "client_ip", "source")
  val columnNamesFinal = Array("log_time", "zalo_id", "gender", "age", "cmd_type", "client_type", "obj_id", "cate_id", "client_ip", "source")
  val commands = Array(1, 2, 3, 4, 5, 3001)
  val currentTime = Calendar.getInstance().getTimeInMillis
  def calculateAgeFromBirthday = udf((birthDay: Long) => {
    TimeUnit.MILLISECONDS.toDays(currentTime - birthDay * 1000) / 365.0
  })
  def readAndJoin(globalIdMap: DataFrame, inputData: DataFrame): DataFrame = {
    inputData
      .filter(col("global_id").startsWith("3000"))
      .select(columnNamesBeforeJoin.head, columnNamesBeforeJoin.tail: _*)
      .join(globalIdMap, Seq("global_id"), "inner")
      .withColumn("age", calculateAgeFromBirthday(col("birthday")))
      .select(columnNamesFinal.head, columnNamesFinal.tail: _*)
      .sort("log_time")
  }
  def transformation(inputData: DataFrame): DataFrame = {
    inputData.select(columnNamesFinal.head, columnNamesFinal.tail: _*)
      .filter(col("cmd_type").isin(commands: _*))
      .selectExpr(
        "log_time",
        "cast (zalo_id as string) zalo_id",
        "gender",
        "age",
        "cast (cmd_type as int) command",
        "cast (client_type as int) client_type",
        "client_ip",
        "cast (obj_id as int) article_id",
        "cast (cate_id as int) article_cate_id",
        "source")
  }
  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Extract ZEP")
      .getOrCreate()
    val inputDatas = inputPaths.grouped(2) map {
      inputPath =>
        readAndJoin(
          spark.read.parquet(inputPath(0)),
          spark.read.parquet(inputPath(1)))

    }
    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = transformation(inputData)

    outputData
      .coalesce(8)
      .write
      .parquet(outputPath)
  }
}