package com.vng.zalo.zte.rnd.anonymoususer.all.identified.hourly

import com.vng.zalo.zte.rnd.anonymoususer.all.GenericExtract
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods

import scala.io.Source

object RawExtract extends GenericExtract {
  val globalIdConvertAPI = "http://10.30.65.168:18703/globalid_map?globalid="
  def getJson(globalId: String): String = {
    try {
      if (!globalId.isEmpty) {
        Source.fromURL(globalIdConvertAPI + globalId)("UTF-8").mkString
      } else {
        "{}"
      }
    }catch {
      case e: Exception => {
        println("Error globalId:" + globalId)
        "{}"
      }
    }
  }

  def decodeGlobalId( globalId: String): String = {
    if ( !globalId.contains("3000") ){
      "0,0"
    }else {
      implicit val formats = DefaultFormats
      val json =  JsonMethods.parse(getJson(globalId))
      val zaloId = (json \ "rawId").extractOrElse[Int](0)
      val id = (json \ "id").extractOrElse[String]("0").toLong
      zaloId.toString+","+id.toString
    }
  }


  def transformation(spark:SparkSession,input:DataFrame): DataFrame = {
    import spark.implicits._
    input.rdd
      .map(row => (row.getString(0),(row.getString(1),row.getString(2))))
      .mapValues{ case (ip,src) => (Set(ip),Set(src)) }
      .reduceByKey((a,b)=> (a._1 | b._1,a._2 | b._2) )
      .map{ case (global_id,(ip,src)) => (global_id,fromSetToString(ip),fromSetToString(src))}
      .toDF("global_id","ip","src")
  }


  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Daily Aggregated Data (Extract)")
      .getOrCreate()
    val inputDatas = inputPaths.map { inputPath => readAndTransform(spark,inputPath).select("global_id","client_ip","src") }
    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = transformation(spark,inputData.filter(col("global_id").startsWith("3000")))

    val udfDecodeGlobalId = udf((global_id:String) => decodeGlobalId(global_id))
    val udfExtractZaloId = udf((ids:String)=> ids.split(",")(0).toInt)
    val udfExtractId = udf((ids:String) => ids.split(",")(1).toLong)
    outputData
      .withColumn("ids",udfDecodeGlobalId(col("global_id")))
      .withColumn("zalo_id",udfExtractZaloId(col("ids")))
      .withColumn("id",udfExtractId(col("ids")))
      .select("global_id","id","zalo_id","ip", "src")
      .coalesce(64)
      .write
      .parquet(outputPath)
  }
}