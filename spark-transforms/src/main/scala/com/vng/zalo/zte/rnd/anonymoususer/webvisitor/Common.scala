package com.vng.zalo.zte.rnd.anonymoususer.webvisitor

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.col

object Common {
  val LOG_SCHEMA = Array("log_time", "app_name", "app_mode", "gid", "client_ip",
    "url", "js_version", "cookie", "status", "device_type", "referer", "user_agent",
    "browser_type", "uid", "date_alive", "platform", "product", "new_gid", "gid_src", "recovered_gid")
  val SELECTED_COLUMNS = Array("log_time", "gid", "client_ip", "url", "status", "device_type", "referer", "browser_type", "platform", "product","gid_src")

  def normalizeRawLog(inputData:DataFrame): DataFrame = {
    inputData
      .sort("log_time")
      .select(Common.SELECTED_COLUMNS.head, Common.SELECTED_COLUMNS.tail: _*)
      .filter(!col("gid").===(""))
      .selectExpr(
        "log_time",
        "cast (gid as string) global_id",
        "client_ip",
        "cast (url as string) url",
        "cast (status as string) status",
        "cast (device_type as string) device_model",
        "referer",
        "browser_type",
        "platform",
        "product",
        "gid_src"
      )
  }

}
