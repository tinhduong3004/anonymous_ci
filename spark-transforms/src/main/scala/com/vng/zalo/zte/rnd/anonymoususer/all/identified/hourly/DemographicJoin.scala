package com.vng.zalo.zte.rnd.anonymoususer.all.identified.hourly

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import com.vng.zing.zudm_userinfocache.thrift.ZUDM_UserInfoCacheThrift
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row, SaveMode, SparkSession}
import org.apache.thrift.protocol.TBinaryProtocol
import org.apache.thrift.transport.{TFramedTransport, TSocket}

object DemographicJoin extends GenericIOTransformation {
  def readOldprofile(spark:SparkSession,inputPaths:Seq[String]): DataFrame = {
    spark.read
      .parquet(inputPaths:_*)
      .select("zalo_id","gender","birthday")
      .distinct()
  }
  def fillDemography(input:DataFrame): DataFrame = {
    val encoder = RowEncoder(
      StructType(Seq(
        StructField("global_id", StringType),
        StructField("id", LongType),
        StructField("zalo_id", IntegerType),
        StructField("gender", IntegerType),
        StructField("birthday", IntegerType),
        StructField("src", StringType)
      )
      )
    )
    input.mapPartitions(
      iterator => {
        val profileSocket = new TSocket("10.30.65.105", 18782)
        val profileTransport = new TFramedTransport(profileSocket)
        val profileProtocol = new TBinaryProtocol(profileTransport)
        val profileClient = new ZUDM_UserInfoCacheThrift.Client(profileProtocol)
        profileTransport.open()
        val output = iterator.map(
          row => {
            val globalId = row.getString(0)
            val id = row.getLong(1)
            val zaloId = row.getInt(2)
            val src = row.getString(3)
            val userProfile = profileClient.getUserInfo(zaloId)
            var birthday = -1
            var gender = -1
            if ( userProfile != null && userProfile.gender != null && userProfile.error == 0 ) {
              birthday = userProfile.getBirthDate
              gender = userProfile.getGender.getValue
            }
            Row.fromSeq(Seq(globalId,id,zaloId,gender,birthday,src))
          }
        ).toList
        profileTransport.close()
        output.iterator
      }
    )(encoder)
  }
  val genderFilter = List(1,2)
  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Daily Aggregated Data (Extract)")
      .getOrCreate()

    val zProfiler = readOldprofile(spark,inputPaths.tail)
    val globalId = spark.read.parquet(inputPaths.head)
      .join(zProfiler,Seq("zalo_id"),"left")

    val hasDemography = globalId
      .filter(col("gender").isNotNull && col("gender").isin(genderFilter:_*) && col("birthday").isNotNull)
      .select("global_id","id","zalo_id","gender","birthday","src")
    val noDemography = globalId
      .filter(col("gender").isNull ||  !col("gender").isin(genderFilter:_*) || col("birthday").isNull)
      .select("global_id","id","zalo_id","src")

    val filledDemography = fillDemography(noDemography)
    val outputData = hasDemography.union(filledDemography)

    outputData
      .select("global_id","id","zalo_id","gender","birthday","src")
      .distinct()
      .coalesce(8)
      .write
      .mode(SaveMode.Overwrite)
      .parquet(outputPath)
  }
}