package com.vng.zalo.zte.rnd.anonymoususer.all

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.{col, lit, udf}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods

abstract class GenericExtract extends GenericIOTransformation {
  def extractGlobalIdInMP3Stat( jsonStr: String): String = {
    try {
      if (jsonStr != null) {
        implicit val formats = DefaultFormats
        val json = JsonMethods.parse(jsonStr)
        (json \ "zDeviceId").extractOrElse[String]("")
      } else {
        ""
      }
    }catch{
      case e:Exception => ""
    }
  }
  val udfExtractor = udf((jsonStr:String) => extractGlobalIdInMP3Stat(jsonStr))
  def readAndTransform(spark:SparkSession,inputPath:String): DataFrame = {
    val src = inputPath.split("/")(3)
    val globalIdDF = src match {
      case "BAOMOI" => {
        spark.read.parquet(inputPath)
      }
      case "ZNEWS-COUNTER" => {
        spark.read.option("delimiter","\t")
          .option("header",false)
          .csv(inputPath)
          .withColumnRenamed("_c4","global_id")
          .withColumnRenamed("_c3","client_ip")
      }

      case "ZMP3_WEB_STATS" => {
        spark.read.parquet(inputPath)
          .select("device_info","client_ip")
          .withColumn("global_id",udfExtractor(col("device_info")))
      }

      case "ZMP3_API_STATS" => {
        spark.read.parquet(inputPath)
          .select("device_info","client_ip")
          .withColumn("global_id",udfExtractor(col("device_info")))
      }

      case "ZINGTV_ACCESS" => {
        spark.read.parquet(inputPath)
          .withColumnRenamed("visitor","global_id")
          .select("global_id","user_ip")
          .withColumnRenamed("user_ip","client_ip")
      }
      case "ZINGTV_LOG_V2" => {
        spark.read.parquet(inputPath)
          .withColumnRenamed("visitor_id","global_id")
          .select("global_id","ip_address")
          .withColumnRenamed("ip_address","client_ip")
      }
      case "CENTRALIZED_GET_VISITOR" => {
        spark.read.parquet(inputPath)
          .withColumnRenamed("gid","global_id")
      }
      case "ADTIMA_ADS_ACTION_V2" => {
        spark.read.parquet(inputPath)
          .withColumnRenamed("uid","global_id")
          .withColumnRenamed("ip","client_ip")
      }
      case "GLOBAL_ID_FOR_DEVICE" => {
        spark.read.parquet(inputPath)
          .withColumnRenamed("g_id","global_id")
          .withColumnRenamed("cliet_ip","client_ip")
      }
      case _ => {
        import spark.implicits._
        Seq.empty[(String,String)].toDF("global_id","client_ip")
      }
    }
    globalIdDF.select("global_id","client_ip")
      .distinct()
      .withColumn("src",lit(src))

  }
  def fromSetToString(set:Set[String]): String = {
    set.mkString(",")
  }
}