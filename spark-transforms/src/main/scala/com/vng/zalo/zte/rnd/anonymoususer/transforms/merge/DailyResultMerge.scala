package com.vng.zalo.zte.rnd.anonymoususer.transforms.merge

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{coalesce, col, mean, udf}

object DailyResultMerge extends GenericIOTransformation {
  val THRESHOLD = .5
  val FILTER_THRESHOLD = .006
//  val HIGH_PRIORITY_RESULT_PATHS = Array("/data/jobs/rnd/anonymoususer/manual_upload/baomoi_anon_pred/2019/07/web",
//    "/data/jobs/rnd/anonymoususer/manual_upload/baomoi_anon_pred/2019/07/wap")
    
  def convertAgeClassToNaturalAge(age: Int): Float = {
    age match {
      case 0 => 17 // <18
      case 1 => 23 // 18-24
      case 2 => 30 // 25-34
      case 3 => 40 // 35-44
      case 4 => 50 // 45-54
      case 5 => 60 // 55-64
      case 6 => 65 // >=65
      case _ => -1
    }
  }

  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Merge daily predictions ")
//      .config("spark.master", "local")
//      .master("local[*]")
      .getOrCreate()
    val threshold_udf = udf(thresholding _)
    val convertAgeClassUdf = udf(convertAgeClassToNaturalAge _)

    val inputHighPriority = spark.read.option("delimiter", "\t").option("header", "true").format("csv").load(inputPaths(0), inputPaths(1))
      .withColumnRenamed("global_id", "id")
      .withColumnRenamed("age_range", "age_priority")
      .withColumnRenamed("gender", "gender_priority")
      .withColumn("age_priority", convertAgeClassUdf(col("age_priority")))
      .dropDuplicates()

    val inputData = spark.read.option("header", "true").format("csv").load(inputPaths(2), inputPaths(3),inputPaths(4), inputPaths(5))
    print("Len before groupby: " + inputData.count())
    val outputData = inputData
      //        .filter(col("gender_preds") > FILTER_THRESHOLD)
      .groupBy(col("id")).agg(
      mean(col("age_preds")) as "agg_age",
      mean(col("gender_preds")) as "agg_gender")
      .withColumn("gender", threshold_udf(col("agg_gender")))
      .select("id", "agg_age", "gender")
      .join(inputHighPriority,
        Seq("id"), "fullOuter")
          .withColumn("age_priority", coalesce(col("age_priority"), col("agg_age")))
          .withColumn("gender_priority", coalesce(col("gender_priority"), col("gender")))
        .select("id", "age_priority", "gender_priority")
      .withColumnRenamed("gender_priority", "gender")
      .withColumnRenamed("age_priority", "agg_age")
    print("Len after groupby: " + outputData.count())
    outputData.describe().show()
    outputData.show()
    outputData.groupBy("gender").sum().show()
    outputData
      .coalesce(1)
      .write
      .option("header", "true")
      .mode("overwrite")
      .csv(outputPath)
  }

  def thresholding(input: Double): Integer = {
    if (input > THRESHOLD) {
      1
    } else {
      0
    }
  }
}
