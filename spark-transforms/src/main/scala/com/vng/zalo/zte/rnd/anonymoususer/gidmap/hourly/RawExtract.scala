package com.vng.zalo.zte.rnd.anonymoususer.gidmap.hourly

import java.util.Calendar
import java.util.concurrent.TimeUnit

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import com.vng.zing.zudm_dmpmiddleware.thrift.ZUDM_DMPMiddlewareService
import com.vng.zing.zudm_userinfocache.thrift.ZUDM_UserInfoCacheThrift
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row, SaveMode, SparkSession}
import org.apache.thrift.protocol.TBinaryProtocol
import org.apache.thrift.transport.{TFramedTransport, TSocket}
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods

import scala.io.Source

object RawExtract extends GenericIOTransformation {
  val currentTime = Calendar.getInstance().getTimeInMillis
  val globalIdConvertAPI = "http://10.30.65.168:18703/globalid_map?globalid="
  def getJson(globalId: String): String = {
    if ( globalId != null && !globalId.isEmpty) {
      Source.fromURL(globalIdConvertAPI + globalId)("UTF-8").mkString
    } else {
      "{}"
    }
  }

  def decodeGlobalId(globalId: String): String = {
    implicit val formats = DefaultFormats
    val json = JsonMethods.parse(getJson(globalId))
    val zaloId = (json \ "rawId").extractOrElse[Int](0)
    val id = (json \ "id").extractOrElse[String]("0").toLong
    val createdTime = (json \ "createdTime").extractOrElse[String]("0").toLong
    zaloId.toString + "," + id.toString +"," + createdTime.toString
  }
  def calculateAgeFromBirthday = udf((birthDay: Long) => {
    TimeUnit.MILLISECONDS.toDays(currentTime - birthDay * 1000) / 365.0
  })



  def transform(input:DataFrame): DataFrame = {
    val encoder = RowEncoder(
      StructType(Seq(
        StructField("log_time", StringType),
        StructField("zalo_id", IntegerType),
        StructField("zalo_gender", IntegerType),
        StructField("zalo_birthday", IntegerType),
        StructField("identified_gid", StringType),
        StructField("dmp_id", LongType),
        StructField("dmp_gender", IntegerType),
        StructField("dmp_age", DoubleType),
        StructField("anonymous_gid", StringType),
        StructField("anonymous_id", LongType),
        StructField("anonymous_gender", IntegerType),
        StructField("anonymous_age", DoubleType),
        StructField("created_time", LongType)
      )))

    val zaloId = udf((cid: String) => cid.split(",")(0))
    val Id = udf((cid: String) => cid.split(",")(1))
    val extractCreatedTime = udf((cid: String) => cid.split(",")(2))
    val combinedId = udf((global_id: String) => decodeGlobalId(global_id))

    input
      .filter(col("identified_gid").isNotNull && col("anonymous_gid").isNotNull)
      .withColumn("icid", combinedId(col("identified_gid")))
      .withColumn("zalo_id", zaloId(col("icid")))
      .withColumn("identified_id", Id(col("icid")))
      .withColumn("acid", combinedId(col("anonymous_gid")))
      .withColumn("anonymous_id", Id(col("acid")))
      .withColumn("created_time", extractCreatedTime(col("acid")))
      .select("log_time", "zalo_id", "identified_gid", "identified_id", "anonymous_gid", "anonymous_id", "created_time")
      .mapPartitions(
        iterator => {
          val profileSocket = new TSocket("10.30.65.105", 18782)
          val profileTransport = new TFramedTransport(profileSocket)
          val profileProtocol = new TBinaryProtocol(profileTransport)
          val profileClient = new ZUDM_UserInfoCacheThrift.Client(profileProtocol)

          val dmpSocket = new TSocket("10.30.65.105", 18756)
          val dmpTransport = new TFramedTransport(dmpSocket)
          val dmpProtocol = new TBinaryProtocol(dmpTransport)
          val dmpClient = new ZUDM_DMPMiddlewareService.Client(dmpProtocol)

          profileTransport.open()
          dmpTransport.open()
          val output = iterator.map(
            row => {
              try {
                val logTime = row.getString(0)
                val zaloId = row.getString(1).toInt
                val identified_gid = row.getString(2)
                val identified_id = row.getString(3).toLong
                val anonymous_gid = row.getString(4)
                val anonymous_id = row.getString(5).toLong
                val createdTime = row.getString(6).toLong
                val userProfile = profileClient.getUserInfo(zaloId)
                var birthday = -9
                var gender = -9
                if (userProfile != null && userProfile.gender != null && userProfile.error == 0) {
                  birthday = userProfile.getBirthDate
                  gender = userProfile.getGender.getValue
                }

                var iage = -9.0

                var objAge = dmpClient.getUserAgePredictByGID("AnonymousMap", identified_id)
                if (objAge != null && objAge.error == 0 && objAge.age != 0) {
                  iage = objAge.age
                }

                var aage = -9.0
                objAge = dmpClient.getAUPredictedAge("AnonymousMap", anonymous_id)
                if (objAge != null && objAge.error == 0 && objAge.age != 0) {
                  aage = objAge.age
                }

                var igender = -9

                var objGender = dmpClient.getUserGenderPredictByGID("AnonymousMap", identified_id)
                if (objGender != null && objGender.error == 0 && objGender.gender != 0) {
                  igender = objGender.gender
                }

                var agender = -9

                objGender = dmpClient.getAUPredictedGender("AnonymousMap", anonymous_id)
                if (objGender != null && objGender.error == 0 && objGender.gender != 0) {
                  agender = objGender.gender
                }
                Row.fromSeq(Seq(logTime, zaloId, gender, birthday, identified_gid, identified_id, igender, iage, anonymous_gid, anonymous_id, agender, aage, createdTime))
              }catch{
                case x: NullPointerException => Row.empty
              }
            }).toList
          profileTransport.close()
          dmpTransport.close()
          output.iterator
        })(encoder)
  }

  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      //      .master("local[*]")
      .appName("RR")
      .getOrCreate()

    val inputData = spark.read.option("delimiter", "\t")
      .option("header", false)
      .csv(inputPaths: _*)
      .select(
        col("_c0") as "log_time",
        col("_c3") as "identified_gid",
        col("_c4") as "anonymous_gid"
      )

    val outputData = transform(inputData)

    outputData
      .withColumn("zalo_age", calculateAgeFromBirthday(col("zalo_birthday")))
      .select("log_time", "zalo_id", "zalo_gender", "zalo_age", "zalo_birthday",
        "identified_gid", "dmp_id", "dmp_gender", "dmp_age",
        "anonymous_gid", "anonymous_id", "anonymous_gender", "anonymous_age",
        "created_time")
      .write.mode(SaveMode.Overwrite).parquet(outputPath)
  }
}
