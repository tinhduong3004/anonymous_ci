package com.vng.zalo.zte.rnd.anonymoususer.zingmp3

import java.time.format.DateTimeFormatter
import java.time.{Instant, ZoneId}
import java.util.Calendar
import java.util.concurrent.TimeUnit

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.udf
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods

abstract class GenericExtract extends GenericIOTransformation {



  def extractArray(jsonStr: String,field:String): String = {
    try {
      if (jsonStr != null) {
        implicit val formats = DefaultFormats
        val json = JsonMethods.parse(jsonStr)
        (json \ field).extractOrElse[Seq[String]](Seq()).mkString(",")
      } else {
        ""
      }
    }catch{
      case e:Exception => ""
    }
  }

  def extractField(jsonStr: String, field:String): String = {
    try {
      if (jsonStr != null) {
        implicit val formats = DefaultFormats
        val json = JsonMethods.parse(jsonStr)
        (json \ field).extractOrElse[String]("")
      } else {
        ""
      }
    }catch{
      case e:Exception => ""
    }
  }
  
    def extractFieldBoolean(jsonStr: String, field:String): Boolean = {
    try {
      if (jsonStr != null) {
        implicit val formats = DefaultFormats
        val json = JsonMethods.parse(jsonStr)
        (json \ field).extractOrElse[Boolean](false)
      } else {
        false
      }
    }catch{
      case e:Exception => false
    }
  }

  val udfGlobalIdExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"zDeviceId")
  })

  val udfSrcExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"src")
  })

  val udfConNameExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"conName")
  })

  val udfConTypeExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"conType")
  })

  val udfIdExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"id")
  })

  val udfTypeExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"type")
  })

  val udfQualityExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"quality")
  })
  val udfDurationExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"duration")
  })
  val udfPosExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"pos")
  })

  val udfVolumeExtractor = udf((jsonStr:String) => {
    extractField(jsonStr,"volume")
  })

  val udfArtistExtractor = udf((jsonStr:String) => {
    extractArray(jsonStr,"artistIds")
  })
  val udfComposerExtractor = udf((jsonStr:String) => {
    extractArray(jsonStr,"composerIds")
  })
  val udfGenresExtractor = udf((jsonStr:String) => {
    extractArray(jsonStr,"genreIds")
  })
  val udfListenTime = udf((jsonStr:String) => {
    val timestamp = extractField(jsonStr,"timestamp")
    try {
      DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
        .withZone(ZoneId.systemDefault())
        .format(Instant.ofEpochMilli(timestamp.toLong))
    }catch{
      case e:Exception => timestamp
    }
  })
  
  val udfListenTime2 = udf((jsonStr:String) => {
    extractField(jsonStr,"timestamp")
  })
  
  val udfSrc = udf((jsonStr:String) => {
    extractField(jsonStr,"src")
  })
  
  val udfIsOfficial = udf((jsonStr:String) => {
    extractFieldBoolean(jsonStr,"isOfficial")
  })
  
  val udfIsDownloaded = udf((jsonStr:String) => {
    extractFieldBoolean(jsonStr,"isDownloaded")
  })
  
  val udfZplayer = udf((jsonStr:String) => {
    extractFieldBoolean(jsonStr,"zplayer")
  })
  
  val udfPId = udf((jsonStr:String) => {
    extractField(jsonStr,"pId")
  })
  
  val udfMiniPlayer = udf((jsonStr:String) => {
    extractFieldBoolean(jsonStr,"miniPlayer")
  })
  
  val udfPType = udf((jsonStr:String) => {
    extractField(jsonStr,"pType")
  })
  
  val udfSkip = udf((jsonStr:String) => {
    extractFieldBoolean(jsonStr,"skip")
  })

  val currentTime = Calendar.getInstance().getTimeInMillis
  val actions = List(5,6,18,19)
  def calculateAgeFromBirthday = udf((birthDay: Long) => {
    TimeUnit.MILLISECONDS.toDays(currentTime - birthDay * 1000) / 365.0
  })
}