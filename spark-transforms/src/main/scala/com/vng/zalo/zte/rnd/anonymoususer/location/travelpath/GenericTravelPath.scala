package com.vng.zalo.zte.rnd.anonymoususer.location.travelpath

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation

import scala.collection.mutable.ListBuffer

case class TravelPath(id: Int, info: String)
abstract class GenericTravelPath extends GenericIOTransformation {
  def listReduce(history: List[String]): List[String] = {
    var mergedHistory = new ListBuffer[String]()
    var start = ""
    var end = ""
    var id = ""
    var count = 0
    history.sorted.foreach(str => {
      val cols = str.split("~")
      if (cols.length != 4) {
        println(str)
      } else if (id.equals(cols(3))) {
        count += cols(2).toInt
        end = cols(1)
      } else {
        if (!id.isEmpty) {
          mergedHistory += start + "~" + end + "~" + count.toString + "~" + id
        }
        id = cols(3)
        start = cols(0)
        end = cols(1)
        count = cols(2).toInt
      }
    })
    if (!id.isEmpty) {
      mergedHistory += start + "~" + end + "~" + count.toString + "~" + id
    }
    mergedHistory.toList
  }
}