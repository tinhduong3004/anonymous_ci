package com.vng.zalo.zte.rnd.anonymoususer.zingnews.anonymous.hourly

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{DataFrame, SparkSession}

object RawExtract extends GenericIOTransformation{
  val columnNames = Array("log_time","id", "client_os", "client_browser", "client_ip", "platform","action","article_id")
  def readAndJoin(globalIdMap:DataFrame, inputData: DataFrame): DataFrame = {
    inputData
      .join(globalIdMap, Seq("global_id"), "inner")
      .select(columnNames.head,columnNames.tail:_*)
      .sort("log_time")
  }

  def readCSV(spark:SparkSession, inputPath:String): DataFrame = {
    spark.read
      .option("delimiter","\t")
      .option("header",false)
      .csv(inputPath)
      .select("_c0","_c4","_c5","_c7","_c3","_c9","_c10","_c11")
      .selectExpr(
        "cast (_c0 as string) log_time",
        "cast (_c4 as string) global_id",
        "cast (_c5 as string) client_os",
        "cast (_c7 as string) client_browser",
        "cast (_c3 as string) client_ip",
        "cast (_c9 as string) platform",
        "cast (_c10 as string) action",
        "cast (_c11 as int) article_id"
      )
      .filter(!col("global_id").startsWith("3000"))
  }
  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Extract ZNews")
      .getOrCreate()
    val inputDatas = inputPaths.grouped(2) map {
      inputPath =>
        readAndJoin(
          spark.read.parquet(inputPath(0)),
          readCSV(spark,inputPath(1))
        )
    }
    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = inputData

    outputData
      .coalesce(8)
      .write
      .parquet(outputPath)
  }
}