package com.vng.zalo.zte.rnd.anonymoususer.all.anonymous.hourly

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.{col, lit, udf}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods

object TimelineExtract extends GenericIOTransformation {
  val TIME_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
  val parseTimestamp = udf((timestamp:String) =>{
    TIME_FORMAT.parse(timestamp).getTime
  })
  val extractGlobalIdInMP3StatV4 = udf((jsonStr: String) => {
    if ( jsonStr != null ) {
      implicit val formats = DefaultFormats
      val json = JsonMethods.parse(jsonStr)
      (json \ "zDeviceId").extractOrElse[String]("")
    }else{
      ""
    }
  })
  def readAndTransform(spark:SparkSession,inputPath:String): DataFrame = {
    val src = inputPath.split("/")(3)
    val globalIdDF = src match {
      case "ZEP_BAOMOI_TRACKING" => {
        spark.read.parquet(inputPath)
          .withColumnRenamed("device_id","global_id")
      }
      case "ZNEWS-COUNTER" => {
        spark.read.option("delimiter","\t")
          .option("header",false).csv(inputPath)
          .withColumnRenamed("_c4","global_id")
          .withColumnRenamed("_c3","client_ip")
          .withColumnRenamed("_c0","log_time")

      }
      case "ZMP3_WEB_STATS" => {
        spark.read.parquet(inputPath)
          .select("log_time","global_id","client_ip")
      }

      case "ZMP3_API_STATS" => {
        spark.read.parquet(inputPath)
          .select("log_time","global_id","client_ip")
      }

      case "ZINGTV_ACCESS" => {
        spark.read.parquet(inputPath)
          .withColumnRenamed("visitor","global_id")
          .select("log_time","global_id","user_ip")
          .withColumnRenamed("user_ip","client_ip")
      }
      case "ZINGTV_LOG_V2" => {
        spark.read.parquet(inputPath)
          .withColumnRenamed("visitor_id","global_id")
          .select("log_time","global_id","ip_address")
          .withColumnRenamed("ip_address","client_ip")
      }
      case "CENTRALIZED_GET_VISITOR" => {
        spark.read.parquet(inputPath)
          .withColumnRenamed("gid","global_id")
      }
      case "ADTIMA_ADS_ACTION_V2" => {
        spark.read.parquet(inputPath)
          .withColumnRenamed("uid","global_id")
          .withColumnRenamed("ip","client_ip")
      }
      case "GLOBAL_ID_FOR_DEVICE" => {
        spark.read.parquet(inputPath)
          .withColumnRenamed("g_id","global_id")
          .withColumnRenamed("cliet_ip","client_ip")
      }
      case _ => {
        import spark.implicits._
        Seq.empty[(String,String,String)].toDF("log_time","global_id","client_ip")
      }
    }

    globalIdDF
      .select("log_time","global_id","client_ip")
      .withColumn("log_time",parseTimestamp(col("log_time")))
      .distinct()
      .withColumn("src",lit(src))
  }

  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Daily Aggregated Data (Extract)")
      .getOrCreate()
    val anonymousMap = spark.read.parquet(inputPaths.head).select("global_id","id")
    val inputDatas = inputPaths.drop(1).map { inputPath => readAndTransform(spark,inputPath) }
    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = inputData.filter(col("global_id").startsWith("2000"))
      .join(anonymousMap,Seq("global_id"))
      .select("log_time","id","client_ip","src")
    outputData
      .coalesce(8)
      .orderBy("log_time")
      .write
      .mode(SaveMode.Overwrite)
      .parquet(outputPath)
  }
}