package com.vng.zalo.zte.rnd.anonymoususer.webvisitor.anonymous.hourly

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{DataFrame, SparkSession}

object RawExtract extends GenericIOTransformation {
  val columnNames = Array("log_time", "gid", "client_ip", "url", "status", "device_type", "referer", "browser_type", "platform", "product","gid_src")
  val finalColumnNames = Array("log_time", "id", "client_ip", "url", "status", "device_model", "referer", "browser_type", "platform", "product","gid_src")

  def readAndJoin(globalIdMap: DataFrame, inputData: DataFrame): DataFrame = {
    inputData
      .filter(!col("global_id").startsWith("3000"))
      .filter(!col("global_id").===(""))
      .join(globalIdMap, Seq("global_id"), "inner")
      .select(finalColumnNames.head, finalColumnNames.tail: _*)
      .sort("log_time")
  }


  def transformation(inputData: DataFrame): DataFrame = {
    inputData
      .sort("log_time")
      .select(columnNames.head, columnNames.tail: _*)
      .filter(!col("gid").===(""))
      .selectExpr(
        "log_time",
        "cast (gid as string) global_id",
        "client_ip",
        "cast (url as string) url",
        "cast (status as string) status",
        "cast (device_type as string) device_model",
        "referer",
        "browser_type",
        "platform",
        "product",
        "gid_src"
      )
  }

  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Extract Get Visitor")
      .getOrCreate()
    val inputDatas = inputPaths.grouped(2) map {
      inputPath =>
        readAndJoin(
          spark.read.parquet(inputPath(0)),
          transformation(spark.read.parquet(inputPath(1))))
    }
    val outputData = inputDatas.reduce((id1, id2) => id1.union(id2))

    outputData
      .coalesce(64)
      .write
      .parquet(outputPath)
  }
}
