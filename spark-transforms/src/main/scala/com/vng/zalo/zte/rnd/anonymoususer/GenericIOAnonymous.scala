package com.vng.zalo.zte.rnd.anonymoususer

import org.apache.spark.sql.DataFrame
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods
import org.apache.spark.sql.functions.{udf,col}

import scala.io.Source

abstract class GenericIOAnonymous extends GenericIOTransformation {
  val globalIdConvertAPI = "http://10.30.65.168:18703/globalid_map?globalid="
  def getJson = udf((globalId: String) => {
    try{
      if ( !globalId.isEmpty && (globalId.startsWith("3000.") || globalId.startsWith("200") )) {
        Source.fromURL(globalIdConvertAPI + globalId)("UTF-8").mkString
      }else{
        "{}"
      }
    }catch{
      case e: Exception => "{}"
    }
  })
  def decodeGlobalId = udf((jsonStr: String) => {
    implicit val formats = DefaultFormats
    val json =  JsonMethods.parse(jsonStr)
    (json \ "id").extractOrElse[String]("0").toLong
  })

  def extractCreatedTime = udf(( jsonStr: String) => {
    implicit val formats = DefaultFormats
    val json =  JsonMethods.parse(jsonStr)
    (json \ "createdTime").extractOrElse[String]("0").toLong
  })

  def globalIdMapDF(globalIdDF:DataFrame): DataFrame = {
    globalIdDF
      .distinct()
      .filter(col("global_id").startsWith("200"))
      .withColumn("json",getJson(col("global_id")))
      .withColumn("id",decodeGlobalId(col("json")))
      .withColumn("id_created_time",extractCreatedTime(col("json")))
      .select("global_id","id","id_created_time")
  }
}
