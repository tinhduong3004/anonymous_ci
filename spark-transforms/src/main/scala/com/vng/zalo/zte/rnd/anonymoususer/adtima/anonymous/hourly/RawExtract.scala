package com.vng.zalo.zte.rnd.anonymoususer.adtima.anonymous.hourly

import com.vng.zalo.zte.rnd.anonymoususer.adtima.GenericExtract
import org.apache.spark.sql.functions.{col, lit}
import org.apache.spark.sql.{DataFrame, SparkSession}

object RawExtract extends GenericExtract {
  val columnNames = Array("log_time", "id", "itemid", "item_kind", "siteid", "zoneid", "kind", "time", "platformType", "targetinfo", "index_category")

  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Extract Ads")
      .getOrCreate()
    val inputDatas = inputPaths.grouped(3) map {
      inputPath =>
        readAndJoin(
          spark.read.parquet(inputPath(0)),
          spark.read.parquet(inputPath(1)),
          spark.read.parquet(inputPath(2)))
    }
    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = transformation(inputData)

    outputData
      .coalesce(8)
      .write
      .parquet(outputPath)
  }

  def readAndJoin(globalIdMap: DataFrame, itemIdMap: DataFrame, inputData: DataFrame): DataFrame = {
    var input_ = inputData
    if ( !input_.columns.contains("targetinfo") ){
      input_ = input_.withColumn("targetinfo",lit(""))
    }
    if ( !input_.columns.contains("index_category") ){
      input_ = input_.withColumn("index_category",lit(-1))
    }
    if ( !input_.columns.contains("platform_type") ){
      input_ = input_.withColumn("platform_type",lit(-1))
    }
    if ( !input_.columns.contains("time") ){
      input_ = input_.withColumn("time",lit(-1))
    }

    input_
      .filter(!col("uid").startsWith("3000"))
      .filter(col("kind").isin(kinds: _*))
      .withColumnRenamed("uid", "global_id")
      .join(globalIdMap, Seq("global_id"), "inner")
      .join(itemIdMap.withColumnRenamed("kind", "item_kind"), Seq("itemid"), "left")
      // .filter(col("item_kind").notEqual("networkHtml"))
      .select(columnNames.head, columnNames.tail: _*)
      .sort("log_time")
  }

  def transformation(inputData: DataFrame): DataFrame = {
    inputData
      .selectExpr(
        "log_time",
        "cast (id as long) global_id",
        "cast (itemid as int) itemid",
        "item_kind",
        "cast (siteid as int) siteid",
        "cast (zoneid as int) zoneid",
        "kind",
        "cast (time as long) time",
        "cast (platformType as int) platform_type",
        "cast (targetinfo as string) target_info",
        "cast (index_category as int) index_category")
  }
}