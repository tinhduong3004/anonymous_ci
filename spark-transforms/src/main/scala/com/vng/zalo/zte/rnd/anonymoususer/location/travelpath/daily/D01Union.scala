package com.vng.zalo.zte.rnd.anonymoususer.location.travelpath.daily

import java.util.Date

import com.vng.zalo.zte.rnd.anonymoususer.location.travelpath.GenericTravelPath
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object D01Union extends GenericTravelPath {
  def dropList(ls: List[String]): List[String] = {
    if (ls.size > 100) {
      ls.drop(ls.size - 100)
    } else {
      ls
    }
  }

  def keepA7Users(ls: List[String]): Boolean = {
    if ( !ls.isEmpty ){
      val now = new Date().getTime
      val last = ls.last.split("~")(1).toLong
      (now - last) <= 7 * 24 * 60 * 60 * 1000 // 7 days in miliseconds
    }else{
      false
    }

  }
  def transformation(inputData: DataFrame): DataFrame = {
    import inputData.sparkSession.implicits._
    inputData.rdd.map(row => (row.getLong(0), row.getString(1).split(",").toList))
      .reduceByKey((a, b) => a ++ b)
      .mapValues(ls => listReduce(ls))
      .filter(x => keepA7Users(x._2))
      .mapValues(ls => dropList(ls))
      .mapValues(ls => ls.mkString(","))
      .toDF()
  }
  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("DailyUnionsTravelPath")
      .getOrCreate()
    val inputDatas = inputPaths map { inputPath => spark.read.parquet(inputPath) }
    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = transformation(inputData)
    outputData.write.mode(SaveMode.Overwrite).parquet(outputPath)
  }
}