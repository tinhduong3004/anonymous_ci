package com.vng.zalo.zte.rnd.anonymoususer.transforms.helpers

import java.nio.charset.StandardCharsets
import java.text.SimpleDateFormat
import java.util.Base64
import java.util.concurrent.TimeUnit

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object UnitedIdentifiedDataMerge extends GenericIOTransformation {
  val PARTITIONS = 512

  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val intervalUdf = udf(getTimeInterval _)
    val SEQ_LIMIT = 500
    val udfSliceListStr = udf((array: Seq[String], from: Int, to: Int) => array.slice(from, to))
    val udfSliceStr = udf((array: String, from: Int, to: Int) => array.split(";").slice(from, to))
    val oneDoubleUdf = udf((x: Seq[Double]) => x.head)
    val oneIntUdf = udf((x: Seq[Integer]) => x.head)
    val udfMaxDate = udf((x: Seq[String]) => getMaxDateFromSortedTimeSeq(x))
    val udfSeqLen = udf((x: Seq[Any]) => x.length)
    val spark = SparkSession.builder()
      .appName("Extract CGV Identified")
//      .master("local[*]")
      .getOrCreate()
    println(inputPaths.mkString("\n"))
    val maxDate = getMaxDateFromPaths(inputPaths)
    println("maxDate: " + maxDate)
    readAndPrepare(spark, inputPaths)
      //      .show(100)
      .groupBy("zalo_id")
      .agg(collect_list(struct("log_time", "itemid", "kind", "age", "gender")).as("nested"))
      .withColumn("nested", sort_array(col("nested"), true))
      .select("zalo_id", "nested.itemid", "nested.kind", "nested.log_time", "nested.age", "nested.gender")
      .withColumn("maxDate", udfMaxDate(col("log_time")))
      .filter(col("maxDate").equalTo(maxDate))
      .withColumn("age", oneDoubleUdf(col("age")))
      .withColumn("gender", oneIntUdf(col("gender")))
      .withColumn("itemid", udfSliceListStr(col("itemid"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("kind", udfSliceListStr(col("kind"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("interval", intervalUdf(col("log_time")))
      .withColumn("raw_seq_len", udfSeqLen(col("log_time")))
      .withColumn("interval", udfSliceStr(col("interval"), lit(0), lit(SEQ_LIMIT)))
      .select("zalo_id", "itemid", "kind", "age", "gender", "raw_seq_len", "interval")
      .withColumn("itemid", concat_ws(" ", col("itemid")))
      .withColumn("kind", concat_ws(" ", col("kind")))
      .withColumn("interval", concat_ws(";", col("interval")))
      //      .show(500, false)
      .repartition(PARTITIONS, col("zalo_id"))
      .write.mode(SaveMode.Overwrite).csv(outputPath)
  }

  def readAndPrepare(spark: SparkSession, inputPaths: Seq[String]): DataFrame = {
    val dataFrames = inputPaths.filter(_.nonEmpty) map { inputPath => readSingleLog(spark, inputPath) }
    dataFrames.reduce((df1, df2) => df1.union(df2))
  }

  def readSingleLog(spark: SparkSession, inputPath: String): DataFrame = {
    val udfDecToHex = udf((x: Any) => x.toString.toLong.toHexString)
    val pattern = "(zmp3_web_stats_identified|adtima_ads_action_identified|zep_baomoi_tracking_identified|znews\\-log\\-event_identified)".r
    val name = (pattern findFirstIn inputPath).get
    println(name)
    name match {
      case "zep_baomoi_tracking_identified" => readZep(spark, inputPath, name)
      case "adtima_ads_action_identified" => readAdtima(spark, inputPath, name)
      case "znews-log-event_identified" => readZnews(spark, inputPath, name)
      case "zmp3_web_stats_identified" => readZmp3Mobile(spark, inputPath, name)
    }
  }

  def readZmp3Mobile(spark: SparkSession, inputPath: String, name: String): DataFrame = {
    val udfDecToHex = udf((x: Any) => x.toString.toLong.toHexString.toUpperCase)

    def append(input: String): String = IdType.ZMP3 + "_" + input

    val udfAppend = udf(append _)

    spark.read.parquet(inputPath)
      .withColumn("log_name", lit(name))
      .select("zalo_id", "song_id", "action_id", "log_time", "age", "gender")
      .withColumnRenamed("action_id", "kind")
      .withColumnRenamed("song_id", "itemid")
      .withColumn("zalo_id", udfDecToHex(col("zalo_id")))
      .withColumn("itemid", udfDecToHex(col("itemid")))
      .withColumn("itemid", udfAppend(col("itemid")))
      .withColumn("kind", udfAppend(col("kind")))
    //      .dropDuplicates(Seq("zalo_id", "itemid", "log_time"))
  }

  def readAdtima(spark: SparkSession, inputPath: String, name: String): DataFrame = {
    val udfDecToHex = udf((x: Any) => x.toString.toLong.toHexString.toUpperCase)

    def append(input: String): String = IdType.ZAD + "_" + input

    val udfAppend = udf(append _)

    spark.read.parquet(inputPath)
      .withColumn("log_name", lit(name))
      .select("zalo_id", "itemid", "kind", "log_time", "age", "gender")
      .withColumn("zalo_id", udfDecToHex(col("zalo_id")))
      .withColumn("itemid", udfDecToHex(col("itemid")))
      .withColumn("itemid", udfAppend(col("itemid")))
      .withColumn("kind", udfAppend(col("kind")))
      .dropDuplicates(Seq("zalo_id", "itemid", "log_time"))
  }

  def readZep(spark: SparkSession, inputPath: String, name: String): DataFrame = {
    val udfDecToHex = udf((x: Any) => x.toString.toLong.toHexString.toUpperCase)

    def append(input: String): String = IdType.ZEP + "_" + input

    val udfAppend = udf(append _)
    val zepCommands = Array("1", "2", "3", "4", "5")

    spark.read.parquet(inputPath)
      .filter(col("command").isin(zepCommands: _*))
      .withColumn("log_name", lit(name))
      .select("zalo_id", "article_id", "command", "log_time", "age", "gender")
      .withColumnRenamed("command", "kind")
      .withColumnRenamed("article_id", "itemid")
      .withColumn("zalo_id", udfDecToHex(col("zalo_id")))
      .withColumn("itemid", udfDecToHex(col("itemid")))
      .withColumn("itemid", udfAppend(col("itemid")))
      .withColumn("kind", udfAppend(col("kind")))
      .dropDuplicates(Seq("zalo_id", "itemid", "log_time"))
  }

  def readZnews(spark: SparkSession, inputPath: String, name: String): DataFrame = {
    val udfDecToHex = udf((x: Any) => x.toString.toLong.toHexString.toUpperCase)

    def append(input: String): String = IdType.ZNEWS + "_" + input

    val udfAppend = udf(append _)
    spark.read.parquet(inputPath)
      .withColumn("log_name", lit(name))
      .filter(col("action").equalTo("article"))
      .select("zalo_id", "article_id", "action", "log_time", "age", "gender")
      .withColumnRenamed("action", "kind")
      .withColumnRenamed("article_id", "itemid")
      .withColumn("zalo_id", udfDecToHex(col("zalo_id")))
      .withColumn("itemid", udfDecToHex(col("itemid")))
      .withColumn("itemid", udfAppend(col("itemid")))
      .withColumn("kind", udfAppend(col("kind")))
      .dropDuplicates(Seq("zalo_id", "itemid", "log_time"))
  }

  def getTimeInterval(rawTimeList: Seq[String]): String = {
    val timeFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
    val input = rawTimeList.map(x => timeFormater.parse(x))
    var resultList = List(timeFormater.format(input.head))
    for (i <- 1 until input.length) resultList = resultList :+ TimeUnit.MILLISECONDS.toMillis(input(i).getTime - input(i - 1).getTime).toHexString.toUpperCase
    resultList.mkString(";")
  }

  def getMaxDateFromSortedTimeSeq(timeSeq: Seq[String]): String = {
    val timeFormater = new SimpleDateFormat("yyyy-MM-dd")
    timeFormater.format(Seq(timeFormater.parse(timeSeq.head.toString), timeFormater.parse(timeSeq.last.toString)).max)
  }

  def getMaxDateFromPaths(inputPaths: Seq[String]): String = {
    val dates = inputPaths map (path => getDateFromPath(path))
    dates.max
  }

  def getDateFromPath(path: String): String = {
    val pattern = "\\d{4}/\\d{2}/\\d{2}".r
    (pattern findFirstIn path).get.replace("/", "-")
  }

  def encodeBase64(rawString: String): String = {
    val authBytes = rawString.getBytes(StandardCharsets.UTF_8)
    Base64.getEncoder.encodeToString(authBytes)
  }
}
