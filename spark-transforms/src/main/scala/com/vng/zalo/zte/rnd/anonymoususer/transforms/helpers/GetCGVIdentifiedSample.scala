package com.vng.zalo.zte.rnd.anonymoususer.transforms.helpers

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.{SaveMode, SparkSession}


  object GetCGVIdentifiedSample extends GenericIOTransformation{

    override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit={
      val spark = SparkSession.builder()
        .appName("Extract CGV Identified")
        .getOrCreate()

      spark.read.parquet(inputPaths:_*)
        .dropDuplicates()
        .write.mode(SaveMode.Overwrite).parquet(outputPath)
    }

  }
