package com.vng.zalo.zte.rnd.anonymoususer.transforms.helpers

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import com.vng.zalo.zte.rnd.anonymoususer.transforms.helpers.UnitedAnonymousDataMerge.PATH_NAME_PATTERN
import org.apache.spark.sql.functions.{col, lit, udf}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object UnitedAnonymousIntermediateTask extends GenericIOTransformation {

  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {

    val spark = SparkSession.builder()
      .appName("Extract CGV anonymous")
//                  .master("local[*]")
      .getOrCreate()
    readAndPrepare(spark, inputPaths, outputPath)
  }

  def readAndPrepare(spark: SparkSession, inputPaths: Seq[String], intermediateOutputPath: String): Unit = {
    print(inputPaths.filter(_.contains(PATH_NAME_PATTERN)))
    inputPaths.map({ inputPath => readAndSingleLog(spark, inputPath, intermediateOutputPath) })
//    inputPaths.filter(PATH_NAME_PATTERN.findFirstIn(_).isDefined).map()
  }

  def readAndSingleLog(spark: SparkSession, inputPath: String, intermediatePath: String): Unit = {
    val udfDecToHex = udf((x: Any) => x.toString.toLong.toHexString)
    val name = (PATH_NAME_PATTERN findFirstIn inputPath).get
    println(name)
    (name match {
      case "zep_baomoi_tracking_anonymous" => readZep(spark, inputPath, name).coalesce(2)
      case "adtima_ads_action_anonymous" => readAdtima(spark, inputPath, name).coalesce(2)
      case "znews-log-event_anonymous" => readZnews(spark, inputPath, name).coalesce(1)
      case "zmp3_api_stats_anonymous" => readZmp3Mobile(spark, inputPath, name).coalesce(2)
      case "zmp3_web_stats_anonymous" => readZmp3Mobile(spark, inputPath, name).coalesce(1)
    }).write.mode(SaveMode.Append).parquet(intermediatePath)
  }

  def readZmp3Mobile(spark: SparkSession, inputPath: String, name: String): DataFrame = {
    val udfDecToHex = udf((x: Any) => x.toString.toLong.toHexString.toUpperCase)

    def append(input: String): String = IdType.ZMP3 + "_" + input

    val udfAppend = udf(append _)

    spark.read.parquet(inputPath)
      .withColumn("log_name", lit(name))
      .select("id", "song_id", "action_id", "log_time")
      .na.drop()
      .withColumnRenamed("id", "global_id")
      .withColumnRenamed("action_id", "kind")
      .withColumnRenamed("song_id", "itemid")
      .withColumn("global_id", udfDecToHex(col("global_id")))
      .withColumn("itemid", udfDecToHex(col("itemid")))
      .withColumn("itemid", udfAppend(col("itemid")))
      .withColumn("kind", udfAppend(col("kind")))
    //      .dropDuplicates(Seq("global_id", "itemid", "log_time"))
  }

  def readAdtima(spark: SparkSession, inputPath: String, name: String): DataFrame = {
    val udfDecToHex = udf((x: Any) => x.toString.toLong.toHexString.toUpperCase)

    def append(input: String): String = IdType.ZAD + "_" + input

    val udfAppend = udf(append _)

    spark.read.parquet(inputPath)
      .withColumn("log_name", lit(name))
      .select("global_id", "itemid", "kind", "log_time")
      .withColumn("global_id", udfDecToHex(col("global_id")))
      .withColumn("itemid", udfDecToHex(col("itemid")))
      .withColumn("itemid", udfAppend(col("itemid")))
      .withColumn("kind", udfAppend(col("kind")))
      .dropDuplicates(Seq("global_id", "itemid", "log_time"))
  }

  def readZep(spark: SparkSession, inputPath: String, name: String): DataFrame = {
    val udfDecToHex = udf((x: Any) => x.toString.toLong.toHexString.toUpperCase)

    def append(input: String): String = IdType.ZEP + "_" + input

    val udfAppend = udf(append _)
    val zepCommands = Array("1", "2", "3", "4", "5")

    spark.read.parquet(inputPath)
      .filter(col("command").isin(zepCommands: _*))
      .withColumn("log_name", lit(name))
      .select("global_id", "article_id", "command", "log_time")
      .withColumnRenamed("command", "kind")
      .withColumnRenamed("article_id", "itemid")
      .withColumn("global_id", udfDecToHex(col("global_id")))
      .withColumn("itemid", udfDecToHex(col("itemid")))
      .withColumn("itemid", udfAppend(col("itemid")))
      .withColumn("kind", udfAppend(col("kind")))
      .dropDuplicates(Seq("global_id", "itemid", "log_time"))
  }

  def readZnews(spark: SparkSession, inputPath: String, name: String): DataFrame = {
    val udfDecToHex = udf((x: Any) => x.toString.toLong.toHexString.toUpperCase)

    def append(input: String): String = IdType.ZNEWS + "_" + input

    val udfAppend = udf(append _)
    spark.read.parquet(inputPath)
      .withColumn("log_name", lit(name))
      .filter(col("action").equalTo("article"))
      .select("id", "article_id", "action", "log_time")
      .withColumnRenamed("id", "global_id")
      .withColumnRenamed("action", "kind")
      .withColumnRenamed("article_id", "itemid")
      .withColumn("global_id", udfDecToHex(col("global_id")))
      .withColumn("itemid", udfDecToHex(col("itemid")))
      .withColumn("itemid", udfAppend(col("itemid")))
      .withColumn("kind", udfAppend(col("kind")))
      .dropDuplicates(Seq("global_id", "itemid", "log_time"))
  }

}
