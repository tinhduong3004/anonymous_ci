package com.vng.zalo.zte.rnd.anonymoususer.device.anonymous.hourly

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.{col, json_tuple, lit}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object RawExtract extends GenericIOTransformation{

  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit={
    val spark = SparkSession.builder()
      .appName("Extract Device")
      .getOrCreate()

    val inputDatas = inputPaths.grouped(2) map {
      inputPath =>
        readAndJoin(
          // globalId map
          spark.read.parquet(inputPath(0)),
          // input data
          spark.read.parquet(inputPath(1)),
          // take suffix as year/month/day/hour
          inputPath(0).substring(inputPath(0).size-14,inputPath(0).size-1))
    }
    val outputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    outputData.coalesce(16)
      .write
      .mode(SaveMode.Overwrite)
      .parquet(outputPath)
  }
  def readAndJoin(globalIdMap:DataFrame, inputData:DataFrame, dateTime:String): DataFrame ={
    inputData
      .withColumnRenamed("g_id", "global_id")
      .filter(!col("global_id").startsWith("3000"))
      .join(globalIdMap, Seq("global_id"),"left")
      .withColumn("device_name", json_tuple(col("deviceInfo_wimei"), "mod"))
      .withColumn("brand_name", json_tuple(col("device_data"), "brd"))
      .withColumnRenamed("platform","os_name")
      .select("id","brand_name","device_name","os_name","os_version")
      .distinct()
      .withColumn("datetime", lit(dateTime))
  }
}