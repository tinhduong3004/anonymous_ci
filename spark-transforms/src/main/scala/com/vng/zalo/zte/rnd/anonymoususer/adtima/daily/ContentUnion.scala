package com.vng.zalo.zte.rnd.anonymoususer.adtima.daily

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.SparkSession

object ContentUnion extends GenericIOTransformation {
  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Union Ads")
      .getOrCreate()
    val inputDatas = inputPaths map {
      inputPath => spark.read.parquet(inputPath)
    }
    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = inputData.distinct()

    outputData
      .coalesce(8)
      .write
      .parquet(outputPath)
  }
}
