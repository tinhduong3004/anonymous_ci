package com.vng.zalo.zte.rnd.anonymoususer.transforms.helpers

import java.nio.charset.StandardCharsets
import java.text.SimpleDateFormat
import java.util.Base64
import java.util.concurrent.TimeUnit

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object IdType extends Enumeration {
  val ZAD = Value(1)
  val ZMP3 = Value(2)
  val ZEP = Value(3)
  val ZNEWS = Value(4)

}

object UnitedAnonymousDataMerge extends GenericIOTransformation {
  val PARTITIONS = 2048
  val PATH_NAME_PATTERN = "(zmp3_api_stats_anonymous|zmp3_web_stats_anonymous|adtima_ads_action_anonymous|zep_baomoi_tracking_anonymous|znews\\-log\\-event_anonymous)".r
  val COLUMNS = List("id", "song_id", "action_id", "log_time")

  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val intervalUdf = udf(getTimeInterval _)
    val SEQ_LIMIT = 500
    val udfSliceListStr = udf((array: Seq[String], from: Int, to: Int) => array.slice(from, to))
    val udfSliceStr = udf((array: String, from: Int, to: Int) => array.split(";").slice(from, to))
    val oneDoubleUdf = udf((x: Seq[Double]) => x.head)
    val oneIntUdf = udf((x: Seq[Integer]) => x.head)
    val udfMaxDate = udf((x: Seq[String]) => getMaxDateFromSortedTimeSeq(x))
    val udfSeqLen = udf((x: Seq[Any]) => x.length)
    val spark = SparkSession.builder()
      .appName("Extract CGV anonymous")
//                  .master("local[*]")
      .getOrCreate()
    println(inputPaths.mkString("\n"))
    val maxDate = getMaxDateFromPaths(inputPaths)
    println("maxDate: " + maxDate)
    //    readAndPrepare(spark, inputPaths)
    spark.read.parquet(inputPaths: _*)
      .repartition(PARTITIONS, col("global_id"))
      //      .show(100)
      .groupBy("global_id")
      .agg(collect_list(struct("log_time", "itemid", "kind")).as("nested"))
      .withColumn("nested", sort_array(col("nested"), true))
      .select("global_id", "nested.itemid", "nested.kind", "nested.log_time")
      .withColumn("maxDate", udfMaxDate(col("log_time")))
      .filter(col("maxDate").equalTo(maxDate))
      .withColumn("itemid", udfSliceListStr(col("itemid"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("kind", udfSliceListStr(col("kind"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("interval", intervalUdf(col("log_time")))
      .withColumn("raw_seq_len", udfSeqLen(col("log_time")))
      .withColumn("interval", udfSliceStr(col("interval"), lit(0), lit(SEQ_LIMIT)))
      .select("global_id", "itemid", "kind", "raw_seq_len", "interval")
      .withColumn("itemid", concat_ws(" ", col("itemid")))
      .withColumn("kind", concat_ws(" ", col("kind")))
      .withColumn("interval", concat_ws(";", col("interval")))
      .coalesce(PARTITIONS)
      .write.mode(SaveMode.Overwrite).csv(outputPath)
  }

  def getTimeInterval(rawTimeList: Seq[String]): String = {
    val timeFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
    val input = rawTimeList.map(x => timeFormater.parse(x))
    var resultList = List(timeFormater.format(input.head))
    for (i <- 1 until input.length) resultList = resultList :+ TimeUnit.MILLISECONDS.toMillis(input(i).getTime - input(i - 1).getTime).toHexString.toUpperCase
    resultList.mkString(";")
  }

  def getMaxDateFromSortedTimeSeq(timeSeq: Seq[String]): String = {
    val timeFormater = new SimpleDateFormat("yyyy-MM-dd")
    timeFormater.format(Seq(timeFormater.parse(timeSeq.head.toString), timeFormater.parse(timeSeq.last.toString)).max)
  }

  def getMaxDateFromPaths(inputPaths: Seq[String]): String = {
    val dates = inputPaths map (path => getDateFromPath(path))
    dates.max
  }

  def getDateFromPath(path: String): String = {
    val pattern = "\\d{4}/\\d{2}/\\d{2}".r
    (pattern findFirstIn path).get.replace("/", "-")
  }

  def readAndPrepare(spark: SparkSession, inputPaths: Seq[String]): DataFrame = {
    print(inputPaths.filter(_.contains(PATH_NAME_PATTERN)))
    val dataFrames = inputPaths.filter(PATH_NAME_PATTERN.findFirstIn(_).isDefined) map { inputPath => readSingleLog(spark, inputPath) }
    //    spark.sparkContext.
    //    dataFrames.reduce((df1, df2) => df1.sample(true, .01).union(df2.sample(true, .01)))
    dataFrames.head
  }

  def readSingleLog(spark: SparkSession, inputPath: String): DataFrame = {
    val name = (PATH_NAME_PATTERN findFirstIn inputPath).get
    println(name)
    name match {
      case "zep_baomoi_tracking_anonymous" => readZep(spark, inputPath, name)
      case "adtima_ads_action_anonymous" => readAdtima(spark, inputPath, name)
      case "znews-log-event_anonymous" => readZnews(spark, inputPath, name)
      case "zmp3_api_stats_anonymous" => readZmp3Mobile(spark, inputPath, name)
      case "zmp3_web_stats_anonymous" => readZmp3Mobile(spark, inputPath, name)
    }
  }

  def readZmp3Mobile(spark: SparkSession, inputPath: String, name: String): DataFrame = {

    def append(input: String): String = IdType.ZMP3 + "_" + input

    val udfAppend = udf(append _)

    spark.read.parquet(inputPath)
      .withColumn("log_name", lit(name))
      .select("id", "song_id", "action_id", "log_time")
      .na.drop()
      .withColumnRenamed("id", "global_id")
      .withColumnRenamed("action_id", "kind")
      .withColumnRenamed("song_id", "itemid")
      .withColumn("itemid", udfAppend(col("itemid")))
      .withColumn("kind", udfAppend(col("kind")))
    //      .dropDuplicates(Seq("global_id", "itemid", "log_time"))
  }

  def readAdtima(spark: SparkSession, inputPath: String, name: String): DataFrame = {

    def append(input: String): String = IdType.ZAD + "_" + input

    val udfAppend = udf(append _)

    spark.read.parquet(inputPath)
      .withColumn("log_name", lit(name))
      .select("global_id", "itemid", "kind", "log_time")
      .withColumn("itemid", udfAppend(col("itemid")))
      .withColumn("kind", udfAppend(col("kind")))
      .dropDuplicates(Seq("global_id", "itemid", "log_time"))
  }

  def readZep(spark: SparkSession, inputPath: String, name: String): DataFrame = {

    def append(input: String): String = IdType.ZEP + "_" + input

    val udfAppend = udf(append _)
    val zepCommands = Array("1", "2", "3", "4", "5")

    spark.read.parquet(inputPath)
      .filter(col("command").isin(zepCommands: _*))
      .withColumn("log_name", lit(name))
      .select("global_id", "article_id", "command", "log_time")
      .withColumnRenamed("command", "kind")
      .withColumnRenamed("article_id", "itemid")
      .withColumn("itemid", udfAppend(col("itemid")))
      .withColumn("kind", udfAppend(col("kind")))
      .dropDuplicates(Seq("global_id", "itemid", "log_time"))
  }

  def readZnews(spark: SparkSession, inputPath: String, name: String): DataFrame = {

    def append(input: String): String = IdType.ZNEWS + "_" + input

    val udfAppend = udf(append _)
    spark.read.parquet(inputPath)
      .withColumn("log_name", lit(name))
      .filter(col("action").equalTo("article"))
      .select("id", "article_id", "action", "log_time")
      .withColumnRenamed("id", "global_id")
      .withColumnRenamed("action", "kind")
      .withColumnRenamed("article_id", "itemid")
      .withColumn("itemid", udfAppend(col("itemid")))
      .withColumn("kind", udfAppend(col("kind")))
      .dropDuplicates(Seq("global_id", "itemid", "log_time"))
  }

  def encodeBase64(rawString: String): String = {
    val authBytes = rawString.getBytes(StandardCharsets.UTF_8)
    Base64.getEncoder.encodeToString(authBytes)
  }
}
