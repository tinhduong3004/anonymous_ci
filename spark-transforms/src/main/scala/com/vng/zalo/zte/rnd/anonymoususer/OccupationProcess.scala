package com.vng.zalo.zte.rnd.anonymoususer

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, collect_list}

object OccupationProcess extends GenericIOTransformation {
  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Merge train data for 1 days")
      //            .master("local[*]")
      .getOrCreate()
    val inputData = spark.read.parquet(inputPaths: _*)
    val ouputData = inputData.selectExpr("cast(uid as int) uid",
                                        "job_id",
                                        "prob"
                                      ).groupBy("uid").agg(
                                        collect_list(col("job_id")).as("job_ids"),
                                        collect_list(col("prob")).as("probs")
                                      )

    ouputData.coalesce(32).write.parquet(outputPath)
  }
}
