package com.vng.zalo.zte.rnd.anonymoususer.adtima

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation

//      ------------+
//|        pause| - pause video
//|       render| - client gọi request lên server để lấy ad
//|firstQuartile| - xem 25% video
//|        rvast| - client gọi request vast lên server để lấy vast
//|          ins| - install
//|         actv| - ad hình (view 50% pixel), ad video (xem 2s video)
//|        close| - close ad
//|   impression| - client show ad
//|     midPoint| - xem 50% video
//|         skip| - skip ad
//|     pageview| -
//|       unmute| - unmute video
//|        click| - click ad
//|         mute| - mute video
//|       resume| - resumse ad
//|         conv| -
//|          prg| - xem 30s video
//|     creative| - client load dc ad
//|   fullscreen| - fullscreen video
//|thirdQuartile| - xem 75% video

abstract class GenericExtract extends GenericIOTransformation {
  val kinds = Array("pause", "firstQuartile", "ins", "actv", "close", "midPoint", "skip", "unmute",
    "click", "mute", "resume", "conv", "prg", "fullscreen", "thirdQuartile")
}