package com.vng.zalo.zte.rnd.anonymoususer.zingmp3.anonymous.daily

import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SaveMode, SparkSession}

import scala.collection.mutable.ListBuffer
import scala.math.max

object Union extends GenericIOTransformation {
  val ITEMS_LIMIT = 300
  val RIGHT_HOURS = 24
  val MAX_LEN = 150

  // Notice: PARTITIONS = 128. Changing this will cause disaster!
  val PARTITIONS = 128


  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    assert(inputPaths.size > RIGHT_HOURS)
    val spark = SparkSession.builder()
      .appName("Merge train data for 1 days")
//                  .master("local[*]")
      .getOrCreate()

    val schema = new StructType()
      .add(StructField("global_id", StringType, true))
      .add(StructField("song_id", StringType, true))
      .add(StructField("action_id", StringType, true))
      .add(StructField("interval", StringType, true))

    val right = readAndClean(spark, inputPaths.slice(inputPaths.size - RIGHT_HOURS, inputPaths.size))
      .rdd
      .filter(r => r.getInt(2) > 0 && r.getInt(3) > 0 )
      .repartition(256)
      .map(r => (r.getString(0), (r.getString(1), r.getInt(2), r.getInt(3))))
      .groupByKey()
      .mapValues(r => r.toList.slice(0, MAX_LEN).sorted)


    val left = readAndClean(spark, inputPaths.slice(0, inputPaths.size - RIGHT_HOURS))
      .rdd
      .repartition(256)
      .filter(r => r.getInt(2) > 0 && r.getInt(3) > 0)
      .map(r => (r.getString(0), (r.getString(1), r.getInt(2), r.getInt(3))))
      .groupByKey()
      .mapValues(r => r.toList.slice(max(0, r.size - MAX_LEN), r.size).sorted)
      .rightOuterJoin(right)
      .mapValues(r => concatRightOuterJoinResult(r._1, r._2))
      .mapValues(r => r.slice(max(0, r.size - MAX_LEN), r.size))
      .mapValues(r => (retrieveByIndex(r, 2), retrieveByIndex(r, 3), retrieveByIndex(r, 1)))
      .map(r => Row(r._1, r._2._1, r._2._2, r._2._3))

    spark.createDataFrame(left, schema)
      .coalesce(PARTITIONS)
      .write
      .mode(SaveMode.Overwrite)
      .csv(outputPath)
  }


  def retrieveByIndex(list: List[(Any, Any, Any)], index: Int) : String = {
    var result = ListBuffer[String]()
    index match {
      case 1 => list.foreach(item => result += item._1.toString)
      case 2 => list.foreach(item => result += item._2.toString)
      case 3 => list.foreach(item => result += item._3.toString)
    }
    if (index==1)
      getTimeInterval(result.toArray.mkString(";"))
    else
      result.toArray.mkString(" ")
  }

  def readAndClean(spark: SparkSession, inputPaths: Seq[String]): DataFrame = {
    spark.read.parquet(inputPaths:_*)
      .na.drop()
      .withColumn("time_at_minute", date_format(col("log_time"), "yyyy-MM-dd HH:mm"))
      .dropDuplicates(Seq("id", "song_id", "action_id", "time_at_minute"))
      .select("id", "song_id", "action_id", "log_time")
      .withColumnRenamed("id", "global_id")
      .selectExpr(
        "cast (global_id as string) global_id",
        "log_time",
        "cast (song_id as int) song_id",
        "cast (action_id as int) action_id"
      )
  }


  def concatRightOuterJoinResult(left: Option[List[(Any, Any, Any)]], right: List[(Any, Any, Any)]) : List[(Any, Any, Any)] = {
    if (left.isEmpty)
      right
    else
      left.head ++ right
  }

  def getTimeInterval(rawTimeList: String): String = {
    val timeFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
    val input = rawTimeList.split(";").map(x => timeFormater.parse(x))
    var resultList = List(timeFormater.format(input.head))
    for (i <- 1 until input.length) resultList = resultList :+ TimeUnit.MILLISECONDS.toMillis(input(i).getTime - input(i - 1).getTime).toString
    resultList.mkString(";")
  }

  def truncateStringSeq(inputList: Seq[String]): Seq[String] = {
    inputList.take(ITEMS_LIMIT)
  }
}