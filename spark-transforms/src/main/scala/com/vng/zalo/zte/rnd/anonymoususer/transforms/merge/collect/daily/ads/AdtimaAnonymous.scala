package com.vng.zalo.zte.rnd.anonymoususer.transforms.merge.collect.daily.ads

import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import com.vng.zalo.zte.rnd.anonymoususer.transforms.merge.collect.daily.ads.AdtimaIdentified.{concatIntArrays, concatStringArrays}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}


object AdtimaAnonymous extends GenericIOTransformation {
  val ITEMS_LIMIT = 300
  val PARTITIONS = 128
  val RIGHT_HOURS = 24

  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    assert(inputPaths.size > RIGHT_HOURS)
    val intervalUdf = udf(getTimeInterval _)
    val SEQ_LIMIT = 300
    val sliceStr = udf((array: Seq[String], from: Int, to: Int) => array.slice(from, to))
    val sliceInt = udf((array: Seq[Int], from: Int, to: Int) => array.slice(from, to))
    val concatIntUDF = udf(concatIntArrays _)
    val concatStrUDF = udf(concatStringArrays _)
    val spark = SparkSession.builder()
      .appName("Merge predict data for 14 days")
//                  .master("local[*]")
      .getOrCreate()

    val paths = inputPaths.sorted
    val dfRight = readAndClean(spark, paths.slice(paths.size - RIGHT_HOURS, paths.size))
      .groupBy("global_id")
      .agg(collect_list(struct("log_time", "itemid", "kind")).as("nested"))
      .withColumn("nested", sort_array(col("nested"), true))
      .select("global_id", "nested.itemid", "nested.kind", "nested.log_time")
      .withColumn("itemid", sliceInt(col("itemid"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("kind", sliceStr(col("kind"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("log_time", sliceStr(col("log_time"), lit(0), lit(SEQ_LIMIT)))

    readAndClean(spark, inputPaths.sorted.slice(0, paths.size - RIGHT_HOURS))
      .groupBy("global_id")
      .agg(collect_list(struct("log_time", "itemid", "kind")).as("nested"))
      .withColumn("nested", sort_array(col("nested"), true))
      .select("global_id", "nested.itemid", "nested.kind", "nested.log_time")
      .withColumn("itemid", sliceInt(col("itemid"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("kind", sliceStr(col("kind"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("log_time", sliceStr(col("log_time"), lit(0), lit(SEQ_LIMIT)))
      .join(dfRight
        .withColumnRenamed("itemid", "itemid_right")
        .withColumnRenamed("kind", "kind_right")
        .withColumnRenamed("log_time", "log_time_right"),
        Seq("global_id"),
        "right_outer"
      )
      .withColumn("itemid", concatIntUDF(col("itemid"), col("itemid_right")))
      .withColumn("kind", concatStrUDF(col("kind"), col("kind_right")))
      .withColumn("log_time", concatStrUDF(col("log_time"), col("log_time_right")))
      .withColumn("itemid", sliceInt(col("itemid"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("kind", sliceStr(col("kind"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("log_time", sliceStr(col("log_time"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("itemid", concat_ws(" ", col("itemid")))
      .withColumn("kind", concat_ws(" ", col("kind")))
      .withColumn("log_time", concat_ws(";", col("log_time")))
      .withColumn("interval", intervalUdf(col("log_time")))
      .drop(col("log_time"))
      .select("global_id", "itemid", "kind", "interval")
      .repartition(PARTITIONS)
      .write
      .mode(SaveMode.Overwrite)
      .csv(outputPath)
  }

  def readAndClean(spark: SparkSession, inputPath: Seq[String]): DataFrame = {
    spark.read.parquet(inputPath: _*)
      .na.drop()
      .withColumn("time_at_minute", date_format(col("log_time"), "yyyy-MM-dd HH:mm"))
      .dropDuplicates(Seq("global_id", "itemid", "kind", "time_at_minute"))
      .select("global_id", "itemid", "kind", "log_time")
  }

  def getTimeInterval(rawTimeList: String): String = {
    val timeFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
    val input = rawTimeList.split(";").map(x => timeFormater.parse(x))
    var resultList = List(timeFormater.format(input.head))
    for (i <- 1 until input.length) resultList = resultList :+ TimeUnit.MILLISECONDS.toMillis(input(i).getTime - input(i - 1).getTime).toString
    resultList.mkString(";")
  }

  def truncateStringSeq(inputList: Seq[String]): Seq[String] = {
    inputList.take(ITEMS_LIMIT)
  }
}