package com.vng.zalo.zte.rnd.anonymoususer.all.anonymous.hourly

import java.util.Calendar
import java.util.concurrent.TimeUnit

import com.vng.zalo.zte.rnd.anonymoususer.all.GenericExtract
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods

import scala.io.Source

object RawExtract extends GenericExtract {

  val currentTime = Calendar.getInstance().getTimeInMillis
  val globalIdConvertAPI = "http://10.30.65.168:18703/globalid_map?globalid="
  def getJson(globalId: String): String = {
    try{
      if ( !globalId.isEmpty && (globalId.startsWith("3000.") || globalId.startsWith("200") )) {
        Source.fromURL(globalIdConvertAPI + globalId)("UTF-8").mkString
      }else{
        "{}"
      }
    }catch{
      case e: Exception => "{}"
    }
  }

  def decodeGlobalId( jsonStr: String): Long = {
    implicit val formats = DefaultFormats
    val json =  JsonMethods.parse(jsonStr)
    (json \ "id").extractOrElse[String]("0").toLong
  }

  def extractCreatedTime( jsonStr: String): Long = {
    implicit val formats = DefaultFormats
    val json =  JsonMethods.parse(jsonStr)
    (json \ "createdTime").extractOrElse[String]("0").toLong
  }

  def differentDays = udf((timestamp: Long) => {
    TimeUnit.MILLISECONDS.toDays(currentTime - timestamp)
  })

  def transformation(spark:SparkSession,input:DataFrame): DataFrame = {
    import spark.implicits._
    val udfDecodeGlobalId = udf((globalId:String) => decodeGlobalId(globalId))
    val udfDecodedJson = udf((globalId:String) => getJson(globalId))
    val udfExtractCreatedTime = udf((globalId:String) => extractCreatedTime(globalId))
    input.rdd
      .map(row => (row.getString(0),(row.getString(1),row.getString(2))))
      .mapValues{ case (ip,src) => (Set(ip),Set(src)) }
      .reduceByKey((a,b)=> (a._1 | b._1,a._2 | b._2) )
      .map{ case (global_id,(ip,src)) => (global_id,fromSetToString(ip),fromSetToString(src))}
      .toDF("global_id","ip","src")
      .withColumn("json", udfDecodedJson(col("global_id")))
      .withColumn("id", udfDecodeGlobalId(col("json")))
      .withColumn("createdTime",udfExtractCreatedTime(col("json")))
      .select("global_id","id","createdTime","ip", "src")
  }
  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Daily Aggregated Data (Extract)")
      .getOrCreate()
    val inputDatas = inputPaths.map { inputPath => readAndTransform(spark,inputPath) }
    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = transformation(spark,
      inputData.filter(!col("global_id").startsWith("3000"))
    )

    outputData
      .withColumn("alive_days",differentDays(col("createdTime")))
      .select("global_id","alive_days","id","ip","src")
      .filter(col("id") > 0)
      .coalesce(8)
      .write
      .mode(SaveMode.Overwrite)
      .parquet(outputPath)
  }
}