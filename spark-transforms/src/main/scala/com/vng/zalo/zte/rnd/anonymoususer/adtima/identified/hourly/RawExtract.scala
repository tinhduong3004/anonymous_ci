package com.vng.zalo.zte.rnd.anonymoususer.adtima.identified.hourly

import java.util.Calendar
import java.util.concurrent.TimeUnit

import com.vng.zalo.zte.rnd.anonymoususer.adtima.GenericExtract
import org.apache.spark.sql.functions.{col, udf, lit}
import org.apache.spark.sql.{DataFrame, SparkSession}

object RawExtract extends GenericExtract {
  val columnNames1 = Array("log_time", "global_id", "itemid", "siteid", "zoneid", "kind", "time", "platformType", "targetinfo", "index_category")
  val columnNames2 = Array("log_time", "zalo_id", "gender", "birthday", "itemid", "item_kind", "siteid", "zoneid", "kind", "time", "platformType", "targetinfo", "index_category")
  val currentTime = Calendar.getInstance().getTimeInMillis

  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Extract Ads")
      .getOrCreate()
    val inputDatas = inputPaths.grouped(3) map {
      inputPath =>
        readAndJoin(
          spark.read.parquet(inputPath(0)),
          spark.read.parquet(inputPath(1)),
          spark.read.parquet(inputPath(2)))
    }
    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = transformation(inputData)

    outputData
      .coalesce(8)
      .write
      .parquet(outputPath)
  }

  def readAndJoin(globalIdMap: DataFrame, itemIdMap: DataFrame, inputData: DataFrame): DataFrame = {
    var input_ = inputData
    if ( !input_.columns.contains("targetinfo") ){
      input_ = input_.withColumn("targetinfo",lit(""))
    }
    if ( !input_.columns.contains("index_category") ){
      input_ = input_.withColumn("index_category",lit(-1))
    }
    if ( !input_.columns.contains("platform_type") ){
      input_ = input_.withColumn("platform_type",lit(-1))
    }
    if ( !input_.columns.contains("time") ){
      input_ = input_.withColumn("time",lit(-1))
    }
    val globalId = input_
      .withColumnRenamed("uid", "global_id")
      .filter(col("global_id").startsWith("3000"))
      .select(columnNames1.head, columnNames1.tail: _*)
      .join(globalIdMap, Seq("global_id"), "inner")
      .join(itemIdMap.withColumnRenamed("kind", "item_kind"), Seq("itemid"), "left")
      // .filter(col("item_kind").notEqual("networkHtml"))
      .select(columnNames2.head, columnNames2.tail: _*)

    globalId
      .sort("log_time")
  }

  def transformation(inputData: DataFrame): DataFrame = {
    inputData.select(columnNames2.head, columnNames2.tail: _*)
      .filter(col("kind").isin(kinds: _*))
      .withColumn("birthday", calculateAgeFromBirthday(col("birthday")))
      .selectExpr(
        "log_time",
        "cast (zalo_id as string) zalo_id",
        "gender",
        "cast( birthday as double) age",
        "cast (kind as string) kind",
        "cast (itemid as int) itemid",
        "item_kind",
        "cast (siteid as int) siteid",
        "cast (zoneid as int) zoneid",
        "cast (time as long) time",
        "cast (platformType as int) platform_type",
        "cast (targetinfo as string) target_info",
        "cast (index_category as int) index_category")
  }

  def calculateAgeFromBirthday = udf((birthDay: Long) => {
    TimeUnit.MILLISECONDS.toDays(currentTime - birthDay * 1000) / 365.25
  })
}