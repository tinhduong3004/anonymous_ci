package com.vng.zalo.zte.rnd.anonymoususer.location.living.daily

import java.util.Calendar

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col

object FilterNewUser extends GenericIOTransformation{
  def twoDayBefore = {
    val cal = Calendar.getInstance
    cal.add(Calendar.DATE, -2)
    cal.getTime
  }
  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Most visited extraction!!!")
      .getOrCreate()
    val timeLowerBound = twoDayBefore.getTime
    spark.read.parquet(inputPaths:_*)
      .filter(col("last_date") > timeLowerBound)
      .write
      .parquet(outputPath)
  }
}