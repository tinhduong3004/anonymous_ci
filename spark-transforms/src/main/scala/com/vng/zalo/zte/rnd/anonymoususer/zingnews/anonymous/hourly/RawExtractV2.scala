package com.vng.zalo.zte.rnd.anonymoususer.zingnews.anonymous.hourly

import com.vng.zalo.zte.rnd.anonymoususer.zingnews.GenericExtract;
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.udf
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods

object RawExtractV2 extends GenericExtract{
  val columnNames = Array("log_time","id", "client_os", "client_browser", "client_ip", "platform","action","article_id")
  def readAndJoin(globalIdMap:DataFrame, inputData: DataFrame): DataFrame = {
    inputData
      .join(globalIdMap, Seq("global_id"), "inner")
      .select(columnNames.head,columnNames.tail:_*)
      .sort("log_time")
  }
  


  def readCSV(spark:SparkSession, inputPath:String): DataFrame = {
    spark.read
      .option("delimiter","\t")
      .option("header",false)
      .csv(inputPath)
//      .select("_c0","_c4","_c5","_c7","_c3","_c9","_c10","_c11")
      .withColumn("client_os", udfClientOs(col("_c10")))
      .withColumn("client_browser", udfClientBrowser(col("_c10")))
      .withColumn("article_id", udfArticleId(col("_c8")))
      .selectExpr(
        "cast (_c0 as string) log_time",
        "cast (_c4 as string) global_id",
        "client_os",
        "client_browser",
        "cast (_c3 as string) client_ip",
        "cast (_c6 as string) platform",
        "cast (_c7 as string) action",
        "article_id"
      )
      .filter(!col("global_id").startsWith("3000"))
  }
  
  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
    .master("local[4]")
      .appName("Extract ZNews")
      .getOrCreate()
    val inputDatas = inputPaths.grouped(2) map {
      inputPath =>
        readAndJoin(
          spark.read.parquet(inputPath(0)),
          readCSV(spark,inputPath(1))
        )
    }

    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = inputData

    outputData.show();
//    outputData
//      .coalesce(8)
//      .write
//      .parquet(outputPath)
  }
}