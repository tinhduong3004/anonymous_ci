package com.vng.zalo.zte.rnd.anonymoususer.zep.identified.daily

import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object Union extends GenericIOTransformation {
  val commands = Array(1,2,3,4,5)
  val MAX_LEN = 300
  val RIGHT_HOURS = 24

  // Notice: PARTITIONS = 128. Changing this will cause disaster!
  val PARTITIONS = 128

  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    assert(inputPaths.size > RIGHT_HOURS)
    val intervalUdf = udf(getTimeInterval _)
    val SEQ_LIMIT = 300
    val sliceStr = udf((array: Seq[String], from: Int, to: Int) => array.slice(from, to))
    val sliceInt = udf((array: Seq[Int], from: Int, to: Int) => array.slice(from, to))
    val oneIntUdf = udf((x: Seq[Integer]) => x.head)
    val oneDoubleUdf = udf((x: Seq[Double]) => x.head)
    val concatIntUDF = udf(concatIntArrays _)
    val concatStrUDF = udf(concatStringArrays _)
    val spark = SparkSession.builder()
      .appName("Merge train data for 14 days")
      //            .master("local[*]")
      .getOrCreate()

    val paths = inputPaths.sorted
    val listPathRight = paths.slice(paths.size - RIGHT_HOURS, paths.size)
    print(listPathRight)
    val dfRight = readAndClean(spark, listPathRight)
      .groupBy("zalo_id")
      .agg(collect_list(struct("log_time", "article_id", "age", "gender")).as("nested"))
      .withColumn("nested", sort_array(col("nested"), true))
      .select("zalo_id", "nested.article_id", "nested.log_time", "nested.age", "nested.gender")
      .withColumn("age", oneDoubleUdf(col("age")))
      .withColumn("gender", oneIntUdf(col("gender")))
      .withColumn("article_id", sliceInt(col("article_id"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("log_time", sliceStr(col("log_time"), lit(0), lit(SEQ_LIMIT)))

    val listPathLeft = inputPaths.sorted.slice(0, paths.size - RIGHT_HOURS)
    print(listPathLeft)
    readAndClean(spark, listPathLeft)
      .groupBy("zalo_id")
      .agg(collect_list(struct("log_time", "article_id", "age", "gender")).as("nested"))
      .withColumn("nested", sort_array(col("nested"), true))
      .select("zalo_id", "nested.article_id", "nested.log_time", "nested.age", "nested.gender")
      .withColumn("age", oneDoubleUdf(col("age")))
      .withColumn("gender", oneIntUdf(col("gender")))
      .withColumn("article_id", sliceInt(col("article_id"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("log_time", sliceStr(col("log_time"), lit(0), lit(SEQ_LIMIT)))
      .drop("age", "gender")
      .join(dfRight
        .withColumnRenamed("article_id", "article_id_right")
        .withColumnRenamed("log_time", "log_time_right"),
        Seq("zalo_id"),
        "right_outer"
      )
      .withColumn("article_id", concatIntUDF(col("article_id"), col("article_id_right")))
      .withColumn("log_time", concatStrUDF(col("log_time"), col("log_time_right")))
      .withColumn("article_id", sliceInt(col("article_id"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("log_time", sliceStr(col("log_time"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("article_id", concat_ws(" ", col("article_id")))
      .withColumn("log_time", concat_ws(";", col("log_time")))
      .withColumn("interval", intervalUdf(col("log_time")))
      .drop(col("log_time"))
      .select("zalo_id", "article_id", "interval", "age", "gender")
      .coalesce(PARTITIONS)
      .write
      .mode(SaveMode.Overwrite)
      .csv(outputPath)
  }


  def retrieveByIndex(list: List[(Any, Any, Any, Any, Any)], index: Int): String = {
    var result = ListBuffer[String]()
    index match {
      case 1 => list.foreach(item => result += item._1.toString)
      case 2 => list.foreach(item => result += item._2.toString)
      case 3 => result += list.head._3.toString
      case 4 => result += list.head._4.toString
      case 5 => result += list.head._5.toString
    }
    if (index == 1)
      getTimeInterval(result.toArray.mkString(";"))
    else
      result.toArray.mkString(" ")
  }

  def concatRightOuterJoinResult(left: Option[List[(Any, Any, Any, Any, Any)]], right: List[(Any, Any, Any, Any, Any)]): List[(Any, Any, Any, Any, Any)] = {
    if (left.isEmpty)
      right
    else
      left.head ++ right
  }

  def readAndClean(spark: SparkSession, inputPath: Seq[String]): DataFrame = {
    spark.read.parquet(inputPath:_*)
      .filter(col("command").isin(commands:_*))
      .withColumn("time_at_minute", date_format(col("log_time"), "yyyy-MM-dd HH:mm"))
      .dropDuplicates(Seq("zalo_id", "article_id", "time_at_minute"))
      .select("zalo_id", "article_id", "log_time", "age", "gender")
  }

  def getTimeInterval(rawTimeList: String): String = {
    val timeFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
    val input = rawTimeList.split(";").map(x => timeFormater.parse(x))
    var resultList = List(timeFormater.format(input.head))
    for (i <- 1 until input.length) resultList = resultList :+ TimeUnit.MILLISECONDS.toMillis(input(i).getTime - input(i - 1).getTime).toString
    resultList.mkString(";")
  }

  def concatIntArrays(firstarray: mutable.WrappedArray[Int],
                      secondarray: mutable.WrappedArray[Int]) : mutable.WrappedArray[Int] =
  {
    if (firstarray != null & secondarray != null)
      firstarray ++ secondarray
    else if (firstarray == null)
      secondarray
    else
      firstarray
  }

  def concatStringArrays(firstarray: mutable.WrappedArray[String],
                         secondarray: mutable.WrappedArray[String]) : mutable.WrappedArray[String] =
  {
    if (firstarray != null & secondarray != null)
      firstarray ++ secondarray
    else if (firstarray == null)
      secondarray
    else
      firstarray
  }
}
