package com.vng.zalo.zte.rnd.anonymoususer.device.anonymous.daily

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{col, rank}

object D01Union extends GenericIOTransformation{
  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit={
    val spark = SparkSession.builder()
      .appName("Daily Unions")
      .getOrCreate()
    val bigDF = spark.read.parquet(inputPaths:_*)

    val w = Window.partitionBy("id")
      .orderBy(col("datetime").desc)
    bigDF
      .withColumn("rank", rank().over(w))
      .filter("rank = 1")
      .drop("rank")
      .orderBy("id")
      .coalesce(16)
      .write
      .parquet(outputPath)
  }
}