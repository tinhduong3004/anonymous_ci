package com.vng.zalo.zte.rnd.anonymoususer.webvisitor.identified.hourly

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOIdentified
import com.vng.zalo.zte.rnd.anonymoususer.webvisitor.Common
import org.apache.spark.sql.{DataFrame, SparkSession}

object RestoreExtract extends GenericIOIdentified {
  val finalColumnNames = Array("log_time", "global_id","zalo_id","birthday","gender", "id_created_time",
                        "client_ip", "url", "status", "device_type", "referer", "browser_type", "platform",
                        "product","gid_src")

  def transformation(inputData: DataFrame): DataFrame = {
    val intermediateData = Common.normalizeRawLog(inputData)
    val globalIdMap = globalIdMapDF(intermediateData.select("global_id"))

    intermediateData.join(globalIdMap, Seq("global_id"), "left")
      .select(finalColumnNames.head, finalColumnNames.tail: _*)
      .sort("log_time")
  }

  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {

    val spark = SparkSession.builder()
      .appName("Extract Get Visitor")
      .getOrCreate()

    val inputData = spark.read.format("csv")
      .option("delimiter","\t")
      .load(inputPaths:_*)
      .toDF(Common.LOG_SCHEMA:_*)

    val outputData = transformation(inputData)

    outputData
      .coalesce(8)
      .write
      .parquet(outputPath)
  }
}

