package com.vng.zalo.zte.rnd.anonymoususer.device.identified.daily

import com.vng.zalo.zte.rnd.anonymoususer.device.identified.{DeviceInfo, GenericUnion}
import org.apache.spark.sql.functions.{col, collect_list, udf}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.json4s.native.Serialization.write
import org.json4s.{DefaultFormats, Extraction, Formats}

object D01Union extends GenericUnion{
  def combineDeviceInfo(brandName:String,deviceName:String,osName:String,osVersion:String,lastUsed:String): String = {
    implicit val formats: Formats = DefaultFormats.withCompanions(classOf[DeviceInfo] -> DeviceInfo)
    val instance = DeviceInfo(brandName,deviceName,osName,osVersion,toEpocsTime(lastUsed))
    val jsonObject = Extraction.decompose(instance)
    write(jsonObject)
  }

  def transform(input:DataFrame): DataFrame = {
    val udfCombineDeviceInfo = udf((brandName:String,deviceName:String,osName:String,osVersion:String,lastUsed:String)
    =>combineDeviceInfo(brandName,deviceName,osName,osVersion,lastUsed))
    val udfReduceJsonList = udf((seq:Seq[String])=>reduceJsonList(seq))

    input.withColumn("device_info",
      udfCombineDeviceInfo(col("brand_name"),col("device_name"),
        col("os_name"),col("os_version"),col("access_time")))
      .groupBy("zalo_id").agg(collect_list(col("device_info")).alias("device_info"))
      .withColumn("reducedList",udfReduceJsonList(col("device_info")))
      .withColumn("devicesId",col("reducedList._2"))
      .withColumn("device_info",col("reducedList._1"))
      .select("zalo_id","devicesId","device_info")
  }
  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Union Device")
      .getOrCreate()
    val inputData = spark.read.parquet(inputPaths:_*)
    val output = transform(inputData)
    output
      .coalesce(128)
      .write
      .mode(SaveMode.Overwrite)
      .parquet(outputPath)
  }
}
