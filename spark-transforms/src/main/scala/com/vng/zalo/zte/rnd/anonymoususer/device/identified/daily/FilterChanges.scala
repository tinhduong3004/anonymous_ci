package com.vng.zalo.zte.rnd.anonymoususer.device.identified.daily

import com.vng.zalo.zte.rnd.anonymoususer.device.identified.GenericUnion
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object FilterChanges extends GenericUnion{
  def except(previousData:DataFrame,inputData:DataFrame): DataFrame = {
    inputData.join(previousData,Seq("zalo_id","devicesId"),"leftanti")
  }
  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Union Device")
      .getOrCreate()
    val inputDatas = inputPaths.grouped(2) map {
      inputPath =>
        except(
          spark.read.parquet(inputPath(0)),
          spark.read.parquet(inputPath(1)))
    }
    val output = inputDatas.reduce((id1, id2) => id1.union(id2))
    output.write
      .mode(SaveMode.Overwrite)
      .parquet(outputPath)
  }
}
