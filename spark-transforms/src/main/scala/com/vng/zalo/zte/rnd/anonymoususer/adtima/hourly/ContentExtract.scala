package com.vng.zalo.zte.rnd.anonymoususer.adtima.hourly

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods.parse

import scala.io.Source

object ContentExtract extends GenericIOTransformation {
  val columnNames = Array("itemid", "kind", "caption", "description", "categoryTargeting", "targetUrl")
  val kinds = Array("pause", "firstQuartile", "ins", "actv", "close", "midPoint", "skip", "unmute",
    "click", "mute", "resume", "conv", "prg", "fullscreen", "thirdQuartile")
  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Extract Ads")
      .getOrCreate()
    val inputDatas = inputPaths map {
      inputPath => spark.read.parquet(inputPath)
    }
    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = transformation(inputData)

    outputData
      .coalesce(8)
      .write.mode(SaveMode.Overwrite)
      .parquet(outputPath)
  }

  def transformation(inputData: DataFrame): DataFrame = {
    inputData
      .filter(col("kind").isin(kinds: _*))
      .select("itemid")
      .distinct()
      .withColumn("json", getRestContentColumn(col("itemid")))
      .withColumn("caption", getCaption(col("json")))
      .withColumn("description", getDescription(col("json")))
      .withColumn("categoryTargeting", getCategoryTargeting(col("json")))
      .withColumn("targetUrl", getTargetUrl(col("json")))
      .withColumn("kind", getKind(col("json")))
      .select(columnNames.head, columnNames.tail: _*)
  }

  val adtimaItemContentApi = "http://10.30.65.168:18703/adtimaitemcontentof?itemid="

  def getJson(itemId: String): String = {
    try {
      if (!itemId.isEmpty) {
        Source.fromURL(adtimaItemContentApi + itemId)("UTF-8").mkString
      } else {
        "{}"
      }
    }catch {
      case e: Exception => {
        println("Error globalId:" + itemId)
        "{}"
      }
    }
  }

  def getRestContentColumn = udf((id: String) => {
    getJson(id)
  })



  def getCaption = udf((jsonString: String) => getField(jsonString, "caption"))

  def getKind = udf((jsonString: String) => getField(jsonString, "kind"))

  def getDescription = udf((jsonString: String) => getField(jsonString, "description"))

  def getField(jsonString: String, field: String): String = {
    try {
      implicit val formats = DefaultFormats
      val json = parse(jsonString)
      (json \ "data" \ field).extractOrElse[String]("").replace("\n", ". ")
    } catch {
      case ex => ""
    }
  }

  def getTargetUrl = udf((jsonString: String) => getField(jsonString, "targetUrl"))

  def getCategoryTargeting = udf((jsonString: String) => {
    try{
      implicit val formats = DefaultFormats
      val json = parse(jsonString)
      (json \ "data" \ "categoryTargeting").extractOrElse[List[Int]](List()).mkString(",")
    }catch {
      case e:Exception => ""
    }
  })
}