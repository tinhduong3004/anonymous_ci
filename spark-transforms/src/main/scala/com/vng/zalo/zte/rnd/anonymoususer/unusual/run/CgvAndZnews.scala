package com.vng.zalo.zte.rnd.anonymoususer.unusual.run

import java.net.URL

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._

object CgvAndZnews {

  def registeredList = Array("anninhthudo.vn", "baocaobang.vn", "baodauthau.vn", "baodientu.chinhphu.vn",
    "giaoducthoidai.vn", "ngaynay.vn", "baonghean.vn", "plo.vn", "tienphong.vn", "viettimes.vn", "bizlive.vn",
    "doanhnhanviet.net.vn", "tinnhanhchungkhoan.vn", "forbesvietnam.com", "giaoduc.net.vn", "infonet.vn",
    "kienthuc.net.vn", "nghenhinvietnam.vn", "baophapluat.vn", "sggp.org.vn", "taichinhdientu.vn",
    "xedoisong.vn", "thanhnien.vn", "antoangiaothong.gov.vn", "vietnamplus.vn", "vov.vn", "vovworld.vn")

  def readAndCountZnews(spark:SparkSession,suffix:String): DataFrame = {
    val inputPaths = pathGenerate("/data/jobs/rnd/intermediate_data/global_apps_hourly/znews-log-event_{}/2020/03".replace("{}",suffix))
    spark.read
      .parquet(inputPaths:_*)
      .withColumnRenamed("zalo_id","id")
      .select("id")
      .groupBy("id")
      .agg(count("*").alias("znews"))
  }

  def joinCount(cgv: DataFrame, znews: DataFrame): DataFrame = {
    val znews_count = znews.agg(count("znews")).first().get(0)
    val znews_sum = znews.agg(sum("znews")).first().get(0)
    cgv.join(znews,col("id") === col("gid"), "leftouter")
      .groupBy("domain")
      .agg(count("cgv").alias("cgv_uniques"),
        sum("cgv").alias("cgv_requests"),
        count("znews").alias("intersect_uniques"),
        sum(when(col("znews").isNotNull,col("cgv")).otherwise(0)).alias("left_requests"),
        sum("znews").alias("right_requests")
      )
      .withColumn("znews_uniques", lit(znews_count))
      .withColumn("znews_requests", lit(znews_sum))
  }

  def pathGenerate(root:String): Seq[String] = {
    val hours = Array("00", "01", "02", "03", "04", "05",
                      "06", "07", "08", "09", "10", "11",
                      "12", "13", "14", "15", "16", "17",
                      "18", "19", "20", "21", "22", "23")
    val days = Array("01", "02", "03", "04", "05", "06", "07")
    days.flatMap(day => hours.map(hour => root+"/"+day+"/"+hour))
  }

  def readAndCountCGV(sparkSession: SparkSession, suffix:String): DataFrame = {
    val inputPaths = pathGenerate("/data/jobs/rnd/intermediate_data/global_apps_hourly/centralized_get_visitor_{}/2020/03/".replace("{}",suffix))
    val domain = udf((url:String) => {
      val inRegisteredList = registeredList.filter(x => url.contains(x))
      if ( inRegisteredList.size > 0 ) {
        try {
          new URL(url).getHost
        } catch {
          case ex: Exception => "NotAUrl"
        }
      }else{
        "NotAUrl"
      }
    })
    sparkSession
      .read
      .parquet(inputPaths:_*)
      .withColumnRenamed("zalo_id","id")
      .select("id","url")
      .withColumn("domain", domain(col("url")))
      .filter(!col("domain").contains("NotAUrl"))
      .groupBy("domain","id")
      .agg(count("url").alias("cgv"))
  }

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("CentralizedGetVisitorCount")
//      .master("local[*]")
      .getOrCreate()


    val joinUserTypes = Array("identified")




    joinUserTypes.foreach(userType => {
      val cgvCountDf = readAndCountCGV(spark, userType)
      val znewsCountDf = readAndCountZnews(spark, userType)

      joinCount(cgvCountDf,znewsCountDf.withColumnRenamed("id","gid"))
        .coalesce(1)
        .write
        .parquet("/data/jobs/rnd/anonymoususer/stats/CgvAndZnews"+userType)
    })
  }
}
