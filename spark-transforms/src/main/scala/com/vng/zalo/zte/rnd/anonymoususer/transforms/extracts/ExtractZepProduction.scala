package com.vng.zalo.zte.rnd.anonymoususer.transforms.extracts

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.{col, collect_list, concat_ws, udf}
import org.apache.spark.sql.{DataFrame, SparkSession}

object ExtractZepProduction extends GenericIOTransformation {
  val commands = Array(1)
  def transformation(inputData: DataFrame): DataFrame = {
    val getConcatenated = udf((first: String, second: String) => { first + " " + second })
    inputData
      .filter(col("global_id").notEqual("0"))
      .filter(col("command").isin(commands: _*))
      .selectExpr(
        "log_time",
        "cast (global_id as string) global_id",
        "cast (article_id as int) article_id")
      .distinct()
      .orderBy("global_id", "log_time")
      .groupBy("global_id")
      .agg(collect_list("article_id"), collect_list("log_time"))
      .withColumnRenamed("collect_list(article_id)", "article_ids")
      .withColumnRenamed("collect_list(log_time)", "log_times")
      .withColumn("article_ids", concat_ws(" ", col("article_ids")))
      .withColumn("log_times", concat_ws(";", col("log_times")))
  }

  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Extract ZEP production")
      //      .master("local[*]")
      .getOrCreate()
    val inputDatas = inputPaths map { inputPath => spark.read.parquet(inputPath) }
    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = transformation(inputData)
    //    print(outputData.show(20, false))
    outputData
      .coalesce(4)
      .write
      .csv(outputPath)
  }
}