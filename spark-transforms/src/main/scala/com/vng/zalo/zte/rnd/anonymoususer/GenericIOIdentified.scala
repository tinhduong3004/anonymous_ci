package com.vng.zalo.zte.rnd.anonymoususer

import com.vng.zing.zudm_userinfocache.thrift.ZUDM_UserInfoCacheThrift
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.{DataFrame, Row}
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.types.{IntegerType, LongType, StringType, StructField, StructType}
import org.apache.thrift.protocol.TBinaryProtocol
import org.apache.thrift.transport.{TFramedTransport, TSocket}

import scala.io.Source

abstract class GenericIOIdentified extends GenericIOTransformation {
  val globalIdConvertAPI = "http://10.30.65.168:18703/globalid_map?globalid="


  def getJson = udf((globalId: String) => {
    try{
      if ( !globalId.isEmpty && (globalId.startsWith("3000.") || globalId.startsWith("200") )) {
        Source.fromURL(globalIdConvertAPI + globalId)("UTF-8").mkString
      }else{
        "{}"
      }
    }catch{
      case e: Exception => "{}"
    }
  })
  def decodeGlobalId = udf((jsonStr: String) => {
    implicit val formats = DefaultFormats
    val json =  JsonMethods.parse(jsonStr)
    (json \ "id").extractOrElse[String]("0").toLong
  })

  def decodeZaloId = udf((jsonStr: String) => {
    implicit val formats = DefaultFormats
    val json =  JsonMethods.parse(jsonStr)
    (json \ "rawId").extractOrElse[String]("0").toLong
  })

  def fillDemography(input:DataFrame): DataFrame = {
    val encoder = RowEncoder(
      StructType(Seq(
        StructField("global_id", StringType),
        StructField("id", LongType),
        StructField("zalo_id", IntegerType),
        StructField("gender", IntegerType),
        StructField("birthday", IntegerType)
      )
      )
    )
    input.mapPartitions(
      iterator => {
        val profileSocket = new TSocket("10.30.65.105", 18782)
        val profileTransport = new TFramedTransport(profileSocket)
        val profileProtocol = new TBinaryProtocol(profileTransport)
        val profileClient = new ZUDM_UserInfoCacheThrift.Client(profileProtocol)
        profileTransport.open()
        val output = iterator.map(
          row => {
            val globalId = row.getString(0)
            val id = row.getLong(1)
            val zaloId = row.getInt(2)
            val userProfile = profileClient.getUserInfo(zaloId)
            var birthday = -1
            var gender = -1
            if (userProfile != null && userProfile.gender != null && userProfile.error == 0) {
              birthday = userProfile.getBirthDate
              gender = userProfile.getGender.getValue
            }
            Row.fromSeq(Seq(globalId, id, zaloId, gender, birthday))
          }
        ).toList
        profileTransport.close()
        output.iterator
      }
    )(encoder)
  }
  def globalIdMapDF(globalIdDF:DataFrame): DataFrame = {
    val intermediateDF = globalIdDF
      .distinct()
      .filter(col("global_id").startsWith("3000"))
      .withColumn("json",getJson(col("global_id")))
      .withColumn("id",decodeGlobalId(col("json")))
      .withColumn("zalo_id",decodeZaloId(col("json")))
      .select("global_id","id","zalo_id")
    fillDemography(intermediateDF)
  }
}
