package com.vng.zalo.zte.rnd.anonymoususer.gidmap.daily

import java.util.Date

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.{col, countDistinct, max, udf}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object StatUnion extends GenericIOTransformation{
  def classifyDuration(last: Long): String = {
    val now = new Date().getTime
    val duration = (now - last) * 1.0 / 86400000 // 24 * 60 * 60 * 1000
    duration match {
      case x if x <= 2 => "D02"
      case x if x <= 7 => "D07"
      case x if x <= 30 => "D30"
      case _ => "Inf"
    }
  }

  def classifyPlatform(globalId: String): String = {
    val prefix = globalId.substring(0,4)
    prefix match {
      case "2000" => "Web"
      case "2001" => "iOs"
      case "2002" => "Android"
      case "2003" => "WP"
      case _ => "Others"
    }
  }

  def transform(input:DataFrame): DataFrame = {
    val udfClassifyDuration = udf((last:Long) => classifyDuration(last))
    val udfClassifyPlatform = udf((globalId:String) => classifyPlatform(globalId))

    input.withColumn("duration",udfClassifyDuration(col("created_time")))
      .withColumn("platform",udfClassifyPlatform(col("anonymous_gid")))
      .filter(col("zalo_gender") === col("dmp_gender"))
      .groupBy("zalo_id","dmp_id","anonymous_id","duration","platform")
      .agg(
        max(col("zalo_gender")).alias("zalo_gender"),
        countDistinct(col("zalo_gender")).alias("zalo_gender_distinct"),

        max(col("dmp_gender")).alias("dmp_gender"),
        countDistinct(col("dmp_gender")).alias("dmp_gender_distinct"),

        max(col("anonymous_gender")).alias("anonymous_gender"),
        countDistinct(col("anonymous_gender")).alias("anonymous_gender_distinct"),

        max(col("zalo_age")).alias("zalo_age"),
        countDistinct(col("zalo_age")).alias("zalo_age_distinct"),

        max(col("dmp_age")).alias("dmp_age"),
        countDistinct(col("dmp_age")).alias("dmp_age_distinct"),

        max(col("anonymous_age")).alias("anonymous_age"),
        countDistinct(col("anonymous_age")).alias("anonymous_age_distinct")
      )
  }

  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("gidmap.StatPreprocess")
      .getOrCreate()
    val inputData = spark.read.parquet(inputPaths:_*)
    val outputData = transform(inputData)
    outputData
      .coalesce(64)
      .write
      .mode(SaveMode.Overwrite)
      .parquet(outputPath)
  }
}
