package com.vng.zalo.zte.rnd.anonymoususer.transforms.merge.collect.daily.all

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{ coalesce, col, mean, udf }

object ResultMerge extends GenericIOTransformation {
  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Merge daily predictions ")
      //      .config("spark.master", "local")
      //      .master("local[*]")
      .getOrCreate()

    val inputData = spark.read.option("delimiter", ",").option("header", "true").format("csv").load(inputPaths: _*)
      .dropDuplicates()

    val outputData = inputData.distinct()

    //    print("Len after groupby: " + outputData.count())
    //    outputData.describe().show()
    //    outputData.show()
    //    outputData.groupBy("gender").sum().show()
    outputData
      .coalesce(1)
      .write
      .option("header", "true")
      .mode("overwrite")
      .csv(outputPath)
  }
}