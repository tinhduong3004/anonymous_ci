package com.vng.zalo.zte.rnd.anonymoususer.zingnews

import java.time.format.DateTimeFormatter
import java.time.{Instant, ZoneId}
import java.util.Calendar
import java.util.concurrent.TimeUnit

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.udf
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods

abstract class GenericExtract extends GenericIOTransformation {



  def extractArray(jsonStr: String,field:String): String = {
    try {
      if (jsonStr != null) {
        implicit val formats = DefaultFormats
        val json = JsonMethods.parse(jsonStr)
        (json \ field).extractOrElse[Seq[String]](Seq()).mkString(",")
      } else {
        ""
      }
    }catch{
      case e:Exception => ""
    }
  }

  def extractField(jsonStr: String, field:String): String = {
    try {
      if (jsonStr != null) {
        implicit val formats = DefaultFormats
        val json = JsonMethods.parse(jsonStr)
        (json \ field).extractOrElse[String]("")
      } else {
        ""
      }
    }catch{
      case e:Exception => ""
    }
  }
  
    def extractFieldBoolean(jsonStr: String, field:String): Boolean = {
    try {
      if (jsonStr != null) {
        implicit val formats = DefaultFormats
        val json = JsonMethods.parse(jsonStr)
        (json \ field).extractOrElse[Boolean](false)
      } else {
        false
      }
    }catch{
      case e:Exception => false
    }
  }

  val udfClientOs = udf((jsonStr:String) => {
    extractField(jsonStr,"os")
  })

  val udfClientBrowser = udf((jsonStr:String) => {
    extractField(jsonStr,"browser")
  })
  
  val udfArticleId = udf((jsonStr:String) => {
    extractField(jsonStr,"objId")
  })

  val udfListenTime = udf((jsonStr:String) => {
    val timestamp = extractField(jsonStr,"timestamp")
    try {
      DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
        .withZone(ZoneId.systemDefault())
        .format(Instant.ofEpochMilli(timestamp.toLong))
    }catch{
      case e:Exception => timestamp
    }
  })
  
  val currentTime = Calendar.getInstance().getTimeInMillis
  val actions = List(5,6,18,19)
  def calculateAgeFromBirthday = udf((birthDay: Long) => {
    TimeUnit.MILLISECONDS.toDays(currentTime - birthDay * 1000) / 365.0
  })
}
