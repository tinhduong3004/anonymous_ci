package com.vng.zalo.zte.rnd.anonymoususer.webvisitor.identified.hourly

import java.util.Calendar
import java.util.concurrent.TimeUnit

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.{DataFrame, SparkSession}

object RawExtract extends GenericIOTransformation {
  val currentTime = Calendar.getInstance().getTimeInMillis
  val columnNames = Array("log_time", "gid", "client_ip", "url", "status", "device_type", "referer", "browser_type", "platform", "product","gid_src")

  val finalColumnNames = Array("log_time","zalo_id","age","gender", "client_ip", "url", "status", "device_model", "referer", "browser_type", "platform", "product","gid_src")

  def calculateAgeFromBirthday = udf((birthDay: Long) => {
    TimeUnit.MILLISECONDS.toDays(currentTime - birthDay * 1000) / 365.0
  })
  def transformation(inputData: DataFrame): DataFrame = {
    inputData
      .sort("log_time")
      .select(columnNames.head, columnNames.tail: _*)
      .filter(!col("gid").===(""))
      .selectExpr(
        "log_time",
        "cast (gid as string) global_id",
        "client_ip",
        "cast (url as string) url",
        "cast (status as string) status",
        "cast (device_type as string) device_model",
        "referer",
        "browser_type",
        "platform",
        "product",
        "gid_src"
      )
  }

  def readAndJoin(globalIdMap: DataFrame, inputData: DataFrame): DataFrame = {
    val globalId = inputData
      .join(globalIdMap, Seq("global_id"), "inner")
      .withColumn("age", calculateAgeFromBirthday(col("birthday")))
      .select(finalColumnNames.head, finalColumnNames.tail: _*)
    globalId
      .filter(col("zalo_id") > 0)
  }

  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Extract Centralized Get Visitor")
      .getOrCreate()
    val inputDatas = inputPaths.grouped(2) map {
      inputPath =>
        readAndJoin(
          spark.read.parquet(inputPath(0)),
          transformation(spark.read.parquet(inputPath(1))))
    }
    val outputData = inputDatas.reduce((id1, id2) => id1.union(id2))

    outputData
      .coalesce(64)
      .write
      .parquet(outputPath)
  }
}