package com.vng.zalo.zte.rnd.anonymoususer.zep.anonymous.daily

import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object Union extends GenericIOTransformation {
  val commands = Array(1,2,3,4,5)
  val ITEMS_LIMIT = 300
  val RIGHT_HOURS = 24
  val MAX_LEN = 150

  // Notice: PARTITIONS = 128. Changing this will cause disaster!
  val PARTITIONS = 128

  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    assert(inputPaths.size > RIGHT_HOURS) //TODO: Remove later
    val spark = SparkSession.builder()
      .appName("Merge train data for 1 days")
      //                        .master("local[*]")
      .getOrCreate()
    assert(inputPaths.size > RIGHT_HOURS)
    val truncateStringSeqUdf = udf(truncateStringSeq _)
    val intervalUdf = udf(getTimeInterval _)
    val SEQ_LIMIT = 300
    val sliceStr = udf((array: Seq[String], from: Int, to: Int) => array.slice(from, to))
    val sliceInt = udf((array: Seq[Int], from: Int, to: Int) => array.slice(from, to))
    val oneIntUdf = udf((x: Seq[Integer]) => x.head)
    val concatIntUDF = udf(concatIntArrays _)
    val concatStrUDF = udf(concatStringArrays _)
    val paths = inputPaths.sorted
    val listPathRight = paths.slice(paths.size - RIGHT_HOURS, paths.size)
    val dfRight = readAndClean(spark, listPathRight)
      .groupBy("global_id")
      .agg(collect_list(struct("log_time", "article_id")).as("nested"))
      .withColumn("nested", sort_array(col("nested"), true))
      .select("global_id", "nested.article_id", "nested.log_time")
      .withColumn("article_id", sliceInt(col("article_id"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("log_time", sliceStr(col("log_time"), lit(0), lit(SEQ_LIMIT)))
    val listPathLeft = inputPaths.sorted.slice(0, paths.size - RIGHT_HOURS)
    val dfLeft = readAndClean(spark, listPathLeft)
    dfLeft
      .groupBy("global_id")
      .agg(collect_list(struct("log_time", "article_id")).as("nested"))
      .withColumn("nested", sort_array(col("nested"), true))
      .select("global_id", "nested.article_id", "nested.log_time")
      .withColumn("article_id", sliceInt(col("article_id"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("log_time", sliceStr(col("log_time"), lit(0), lit(SEQ_LIMIT)))
      .join(dfRight
        .withColumnRenamed("article_id", "article_id_right")
        .withColumnRenamed("log_time", "log_time_right"),
        Seq("global_id"),
        "right_outer")
      .withColumn("article_id", concatIntUDF(col("article_id"), col("article_id_right")))
      .withColumn("log_time", concatStrUDF(col("log_time"), col("log_time_right")))
      .withColumn("article_id", sliceInt(col("article_id"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("log_time", sliceStr(col("log_time"), lit(0), lit(SEQ_LIMIT)))
      .withColumn("article_id", concat_ws(" ", col("article_id")))
      .withColumn("log_time", concat_ws(";", col("log_time")))
      .withColumn("interval", intervalUdf(col("log_time")))
      .drop(col("log_time"))
      .select("global_id", "article_id", "interval")
      .coalesce(PARTITIONS)
      .write
      .mode(SaveMode.Overwrite)
      .csv(outputPath)
  }

  def retrieveByIndex(list: List[(Any, Any, Any)], index: Int): String = {
    var result = ListBuffer[String]()
    index match {
      case 1 => list.foreach(item => result += item._1.toString)
      case 2 => list.foreach(item => result += item._2.toString)
      case 3 => list.foreach(item => result += item._3.toString)
    }
    if (index == 1)
      getTimeInterval(result.toArray.mkString(";"))
    else
      result.toArray.mkString(" ")
  }

  def readAndClean(spark: SparkSession, inputPath: Seq[String]): DataFrame = {
    spark.read.parquet(inputPath: _*)
      .withColumn("time_at_minute", date_format(col("log_time"), "yyyy-MM-dd HH:mm"))
      .dropDuplicates(Seq("global_id", "article_id", "time_at_minute"))
      .select("global_id", "article_id", "log_time")
  }

  def concatRightOuterJoinResult(left: Option[List[(Any, Any, Any)]], right: List[(Any, Any, Any)]): List[(Any, Any, Any)] = {
    if (left.isEmpty)
      right
    else
      left.head ++ right
  }

  def getTimeInterval(rawTimeList: String): String = {
    val timeFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
    val input = rawTimeList.split(";").map(x => timeFormater.parse(x))
    var resultList = List(timeFormater.format(input.head))
    for (i <- 1 until input.length) resultList = resultList :+ TimeUnit.MILLISECONDS.toMillis(input(i).getTime - input(i - 1).getTime).toString
    resultList.mkString(";")
  }

  def concatIntArrays(firstarray: mutable.WrappedArray[Int],
                      secondarray: mutable.WrappedArray[Int]): mutable.WrappedArray[Int] = {
    if (firstarray != null & secondarray != null)
      firstarray ++ secondarray
    else if (firstarray == null)
      secondarray
    else
      firstarray
  }

  def concatStringArrays(firstarray: mutable.WrappedArray[String],
                         secondarray: mutable.WrappedArray[String]): mutable.WrappedArray[String] = {
    if (firstarray != null & secondarray != null)
      firstarray ++ secondarray
    else if (firstarray == null)
      secondarray
    else
      firstarray
  }
  def truncateStringSeq(inputList: Seq[String]): Seq[String] = {
    inputList.take(ITEMS_LIMIT)
  }
}
