package com.vng.zalo.zte.rnd.anonymoususer.device.identified.daily

import com.vng.zalo.zte.rnd.anonymoususer.device.identified.GenericUnion
import org.apache.spark.sql.functions.{col, collect_list, udf}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object D60Union extends GenericUnion{
  def transform(input:DataFrame): DataFrame = {
    val udfReduceJsonList = udf((seq:Seq[String])=>reduceJsonList(seq))
    val udfFlattenSeq = udf((seq:Seq[Seq[String]]) => seq flatten)

    input.groupBy("zalo_id").agg(collect_list(col("device_info")).alias("device_info"))
      .withColumn("device_info",udfFlattenSeq(col("device_info")))
      .withColumn("reducedList",udfReduceJsonList(col("device_info")))
      .withColumn("devicesId",col("reducedList._2"))
      .withColumn("device_info",col("reducedList._1"))
      .select("zalo_id","devicesId","device_info")
  }
  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Union Device")
      .getOrCreate()
    val inputData = spark.read.parquet(inputPaths:_*)
    val output = transform(inputData)
    output.write
      .mode(SaveMode.Overwrite)
      .parquet(outputPath)
  }
}
