package com.vng.zalo.zte.rnd.anonymoususer.zep.anonymous.hourly

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOAnonymous
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{DataFrame, SparkSession}


// This code was used for restoration log progress
object RestoreExtract extends GenericIOAnonymous{
  val rawLogSchema = Array("log_time", "app_name", "app_mode", "timestamp", "cmd_type",
    "obj_type", "obj_id", "publisher_id", "cate_id", "json_extra",
    "user_id", "zalo_id", "login", "gy", "client_type",
    "client_version", "client_ip", "network_operator", "network_type", "source",
    "source_index", "global_id", "user_agent", "referer")

  val columnNames = Array("log_time", "global_id","id","id_created_time" , "command",
                          "client_type", "article_id", "article_cate_id", "client_ip", "source")
  val commands = Array(1, 2, 3, 4, 5, 3001)

  def transform(inputData: DataFrame): DataFrame = {

    val intermediateData = inputData
      .filter(!col("global_id").startsWith("3000"))
      .filter(!col("global_id").===(""))
      .filter(col("cmd_type").isin(commands: _*))
      .withColumnRenamed("cmd_type", "command")
      .withColumnRenamed("obj_id", "article_id")
      .withColumnRenamed("cate_id", "article_cate_id")

    val globalIdMap = globalIdMapDF(intermediateData.select("global_id"))

    intermediateData.join(globalIdMap, Seq("global_id"), "left")
      .select(columnNames.head, columnNames.tail: _*)
      .sort("log_time")
  }

  override def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Extract ZEP")
      .getOrCreate()

    val inputData = spark.read.format("csv")
                        .option("delimiter","\t")
                        .load(inputPaths:_*)
                        .toDF(rawLogSchema:_*)

    val outputData = transform(inputData)

    outputData
      .coalesce(8)
      .write
      .parquet(outputPath)
  }
}
