package com.vng.zalo.zte.rnd.anonymoususer.zep.daily

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.{SaveMode, SparkSession}

object ContentUnion extends GenericIOTransformation {

  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Merge articles for ... days")
      .getOrCreate()
    val inputDatas = spark.read.parquet(inputPaths: _*)
    val data = inputDatas
      .distinct()
    data
      .selectExpr("cast (article_id as long) article_id", "keywords", "article_cate")
      .dropDuplicates()
      .coalesce(1)
      .write
      .mode(SaveMode.Overwrite)
      .parquet(outputPath)
  }
}
