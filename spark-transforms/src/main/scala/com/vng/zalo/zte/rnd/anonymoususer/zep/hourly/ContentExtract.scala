package com.vng.zalo.zte.rnd.anonymoususer.zep.hourly

import java.util.Base64

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.json4s.JsonAST.{JField, JInt, JObject, JString}
import org.json4s.NoTypeHints
import org.json4s.native.JsonMethods.parse
import org.json4s.native.Serialization

import scala.io.Source

object ContentExtract extends GenericIOTransformation{
  val selectedColumns: Array[String] = Array("article_id", "article_cate", "keywords")
  val keywordParserAPI = "http://10.30.65.168:18703/zepcontentof?contentid="

  def getTerms(jsonString: String): String = {
    val json = parse(jsonString)
    var mp = Map[String,Int]()
    for {
      JObject(terms) <- json
      JField("term", JString(term)) <- terms
      JField("type", JInt(termType)) <- terms
      JField("frequency", JInt(frequency)) <- terms
    } yield {
      mp += ((term+"__"+termType.toString,frequency.toInt))
    }
    implicit val formats = Serialization.formats(NoTypeHints)
    Serialization.write(mp)
  }
  def getKeywordJson(url: String): String = {
    val encodedUrl = Base64.getUrlEncoder().encodeToString(url.getBytes())
    if ( encodedUrl.length() <= 2000 ) {
      try {
        Source.fromURL(keywordParserAPI + url + "&type=keywords")("UTF-8").mkString
      } catch {
        case t: Throwable => "{}" // TODO: handle error
      }

    }else{
      "{}"
    }
  }

  def getCategory(url: String): String = {
    val encodedUrl = Base64.getUrlEncoder().encodeToString(url.getBytes())
    if ( encodedUrl.length() <= 2000 ) {
      try {
        Source.fromURL(keywordParserAPI + url + "&type=category")("UTF-8").mkString
      } catch {
        case t: Throwable => "-2" // TODO: handle error
      }
    }else{
      "{}"
    }
  }

  def transformation(inputData: DataFrame): DataFrame = {
    val terms = udf((json: String) => getTerms(json))
    val cateId = udf((article_id:Int) => getCategory(String.valueOf(article_id)))
    val bmUrl = udf((article_id:Int) => getKeywordJson(String.valueOf(article_id)))
    inputData.select("obj_id")
      .withColumnRenamed("obj_id", "article_id")
      .filter(col("article_id") > 0)
      .groupBy("article_id").count()
      .withColumn("keywords",bmUrl(col("article_id")))
      .withColumn("article_cate", cateId(col("article_id")))
      .withColumn("article_id", col("article_id").cast("long"))
      .select(selectedColumns.head, selectedColumns.tail: _*)
  }
  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Daily Aggregated Data (Extract)")
      .getOrCreate()
    val inputDatas = inputPaths.map { inputPath => spark.read.parquet(inputPath) }
    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = transformation(inputData)
    outputData.coalesce(24).write.parquet(outputPath)
  }
}