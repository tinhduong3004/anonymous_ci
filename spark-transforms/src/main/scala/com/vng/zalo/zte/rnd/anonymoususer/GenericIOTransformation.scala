package com.vng.zalo.zte.rnd.anonymoususer

/**
 * Created by quydm on 11/20/17.
 */
case class Config(inputPaths: Seq[String] = Seq(), outputPath: String = "")

abstract class GenericIOTransformation extends Serializable {
  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit

  def main(args: Array[String]) {
    val parser = new scopt.OptionParser[Config]("scopt") {
      head("Extract users")
      opt[String]("input") action { (x, c) =>
        c.copy(inputPaths = x.split(','))
      } text ("The input file (diretory), of ZP_CENTRALIZED_TRACKING format")
      opt[String]("output") action { (x, c) =>
        c.copy(outputPath = x)
      } text ("The output directory, will be nl-seperated uids")
    }

    parser.parse(args, Config()) match {
      case Some(config) =>
        readWriteTransform(inputPaths = config.inputPaths, outputPath = config.outputPath)
      case None =>
        println("Couldn't parse command line parameters.")
    }
  }
}