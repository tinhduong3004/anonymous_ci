package com.vng.zalo.zte.rnd.anonymoususer.unusual.run

import java.net.URL

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._

object CentralizedGetVisitorCount {

  def registeredList = Array("anninhthudo.vn", "baocaobang.vn", "baodauthau.vn", "baodientu.chinhphu.vn",
    "giaoducthoidai.vn", "ngaynay.vn", "baonghean.vn", "plo.vn", "tienphong.vn", "viettimes.vn", "bizlive.vn",
    "doanhnhanviet.net.vn", "tinnhanhchungkhoan.vn", "forbesvietnam.com", "giaoduc.net.vn", "infonet.vn",
    "kienthuc.net.vn", "nghenhinvietnam.vn", "baophapluat.vn", "sggp.org.vn", "taichinhdientu.vn",
    "xedoisong.vn", "thanhnien.vn", "antoangiaothong.gov.vn", "vietnamplus.vn", "vov.vn", "vovworld.vn")

  def readAndCountZnews(spark:SparkSession): DataFrame = {
    val inputPaths = pathGenerate("/zlogcentral_live/scribe/ZNEWS-COUNTER/2020/03")

    spark.read
      .option("delimiter","\t")
      .option("header",false)
      .csv(inputPaths:_*)
      .select("_c4")
      .selectExpr(
        "cast (_c4 as string) global_id"
      ).groupBy("global_id")
      .agg(count("*").alias("znews"))
  }

  def joinCount(cgv: DataFrame, znews: DataFrame): DataFrame = {
    val znews_count = znews.agg(count("znews")).first().get(0)
    val znews_sum = znews.agg(sum("znews")).first().get(0)
    cgv.join(znews,col("global_id") === col("gid"), "leftouter")
      .groupBy("domain")
      .agg(count("cgv").alias("cgv_uniques"),
        sum("cgv").alias("cgv_requests"),
        count("znews").alias("intersect_uniques"),
        sum(when(col("znews").isNotNull,col("cgv")).otherwise(0)).alias("left_requests"),
        sum("znews").alias("right_requests")
      )
      .withColumn("znews_uniques", lit(znews_count))
      .withColumn("znews_requests", lit(znews_sum))
  }
  def pathGenerate(root:String): Seq[String] = {
    val hours = Array("00", "01", "02", "03", "04", "05",
                      "06", "07", "08", "09", "10", "11",
                      "12", "13", "14", "15", "16", "17",
                      "18", "19", "20", "21", "22", "23")
    val days = Array("01", "02", "03", "04", "05", "06", "07")
    days.flatMap(day => hours.map(hour => root+"/"+day+"/"+hour))
  }

  def readAndCountCGV(sparkSession: SparkSession): DataFrame = {
    val inputPaths = pathGenerate("/zlogcentral_live/scribe_parquet/CENTRALIZED_GET_VISITOR/2020/03")
    val domain = udf((url:String) => {
      val inRegisteredList = registeredList.filter(x => url.contains(x))
      if ( inRegisteredList.size > 0 ) {
        try {
          new URL(url).getHost
        } catch {
          case ex: Exception => "NotAUrl"
        }
      }else{
        "NotAUrl"
      }
    })
    sparkSession
      .read
      .parquet(inputPaths:_*)
      .select("gid","url")
      .withColumn("domain", domain(col("url")))
      .filter(!col("domain").contains("NotAUrl"))
      .groupBy("domain","gid")
      .agg(count("url").alias("cgv"))
  }

  def doJoin(cgvCount:DataFrame, znews:DataFrame, filterValue: String ): Unit = {

  }

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("CentralizedGetVisitorCount")
//      .master("local[*]")
      .getOrCreate()

    val cgvCountDf = readAndCountCGV(spark)
    val znewsCountDf = readAndCountZnews(spark)

    val joinFields = Array("200","2000", "2001", "2002", "3000")

    joinFields.foreach(field => {
      joinCount(cgvCountDf.filter(col("gid").startsWith(field)),
        znewsCountDf.filter(col("global_id").startsWith(field))
      ).coalesce(1)
        .write
        .parquet("/data/jobs/rnd/anonymoususer/stats/cgv_count_"+field)
    })
  }
}
