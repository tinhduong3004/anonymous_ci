package com.vng.zalo.zte.rnd.anonymoususer.zingnews.hourly

import com.vng.zalo.zte.rnd.anonymoususer.GenericIOTransformation
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods.parse

import scala.io.Source

object ContentExtract extends GenericIOTransformation{
  val keywordParserAPI = "http://10.30.65.168:18703/zingnewscontentof?contentid="
  def isAllDigits(x: String) = x forall Character.isDigit


  def getJsonContent = udf((id: Int) => {
    Source.fromURL(keywordParserAPI + id)("UTF-8").mkString
  })

  def getField(jsonString: String,field: String): String = {
    implicit val formats = DefaultFormats
    val json = parse(jsonString)
    (json \ field).extractOrElse[String]("")
  }
  def getArticleTitle = udf((jsonString: String) => {
    getField(jsonString,"title")
  })

  def getArticleSummary = udf((jsonString: String) => {
    getField(jsonString,"summary")
  })

  def getArticleCate = udf((jsonString: String) => {
    getField(jsonString,"cate")
  })

  def getArticleSubCate = udf((jsonString: String) => {
    getField(jsonString,"subcate")
  })

  def getArticleZepId = udf((jsonString: String) => {
    getField(jsonString,"zepId")
  })

  def getArticleTopics = udf((jsonString: String) => {
    getField(jsonString,"topics")
  })

  def getArticleTags = udf((jsonString: String) => {
    getField(jsonString,"tags")
  })

  def getTime = udf((jsonString: String) => {
    getField(jsonString,"time")
  })

  def getRelatedArticles = udf((jsonString: String) => {
    getField(jsonString,"relatedArticles")
  })

  def readCSV(spark:SparkSession, inputPath:String): DataFrame = {
    spark.read
      .option("delimiter","\t")
      .option("header",false)
      .csv(inputPath)
      .filter(col("_c10").equalTo("article"))
      .select("_c11")
      .selectExpr(
        "cast (_c11 as int) article_id"
      )
      .distinct()
  }
  def readWriteTransform(inputPaths: Seq[String], outputPath: String): Unit = {
    val spark = SparkSession.builder()
      .appName("Extract ZNews")
      .getOrCreate()
    val inputDatas = inputPaths map { inputPath => readCSV(spark,inputPath) }
    val inputData = inputDatas.reduce((id1, id2) => id1.union(id2))
    val outputData = inputData
      .withColumn("content",getJsonContent(col("article_id")))
      .withColumn("time",getTime(col("content")))
      .withColumn("title",getArticleTitle(col("content")))
      .withColumn("summary",getArticleSummary(col("content")))
      .withColumn("cate",getArticleCate(col("content")))
      .withColumn("sub_cate",getArticleSubCate(col("content")))
      .withColumn("zep_id",getArticleZepId(col("content")))
      .withColumn("topics",getArticleTopics(col("content")))
      .withColumn("tags",getArticleTags(col("content")))
      .withColumn("related_articles",getRelatedArticles(col("content")))
      .drop("content")

    outputData
      .coalesce(8)
      .write
      .parquet(outputPath)
  }
}