lazy val commonSettings = Seq(
  organization := "com.vng.zing.zudm",
  version := "0.3-SNAPSHOT",
  scalaVersion := "2.11.0"
)
libraryDependencies ++= Seq(
  "org.apache.spark" % "spark-core_2.11" % "2.3.1",// % "provided",
  "org.apache.spark" % "spark-sql_2.11" % "2.3.1",// % "provided",
  "org.apache.spark" %% "spark-mllib" % "2.3.1",// % "provided",
  "com.github.scopt" %% "scopt" % "3.3.0",
  "org.json4s" %% "json4s-native" % "3.2.11"

)

assemblyMergeStrategy in assembly := {
 case PathList("META-INF", xs @ _*) => MergeStrategy.discard
 case x => MergeStrategy.first
}

resolvers += Resolver.sonatypeRepo("public")
parallelExecution in Test := false

lazy val root = (project in file("."))
  .settings(commonSettings: _*)
  .settings(
    name:= "spark-transforms",
    moduleName:= "article-interest"
)

