#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys


# In[2]:


# sys.argv = ['train_zep.py',
#             '/data/zmining/.anonymous/data/inputs/zep_activity_train/2000/02/01/data.csv', 
#             '/data/zmining/.anonymous/data/intermediate/2000/02/01/train.done',
#             '_zep_2_weeks_2000_02_01',
#             '3',
#             '/data/zmining/.anonymous/data/intermediate/2000/02/01/intermediate.bundle'            
#            ]


# In[3]:


# sys.argv[5]


# In[4]:


#import fastparquet as fp
import os
gpu_index = sys.argv[4]
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]=str(gpu_index)
#os.environ["CUDA_VISIBLE_DEVICES"]='5,6,7'
os.system("echo -e ZEP GPU:{} | tee -a /data/zmining/.anonymous/logs/train_daily_zep.log".format(gpu_index))
import pandas as pd
import numpy as np
import warnings
from sklearn.model_selection import train_test_split,  KFold
from sklearn.metrics import roc_auc_score
from sklearn.metrics import f1_score, roc_auc_score
warnings.filterwarnings('ignore')
import json
import gc
import math
from tqdm import tqdm_notebook
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_auc_score
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

import pickle
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

from keras.models import Sequential
from keras import  backend as K
from keras.models import Model, load_model
from keras.layers import LSTM, CuDNNGRU, CuDNNLSTM, Dense, Bidirectional, Input, SpatialDropout1D,Embedding,         BatchNormalization, GlobalMaxPooling1D, GlobalAveragePooling1D, concatenate, Conv1D, Flatten, Dropout, Lambda
from keras.engine.topology import Layer
from keras import initializers, regularizers, constraints
from keras.initializers import he_uniform
from keras.models import model_from_json
import tensorflow as tf

from sklearn.model_selection import train_test_split, StratifiedShuffleSplit, GroupShuffleSplit
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, EarlyStopping, ReduceLROnPlateau
from keras.preprocessing import text, sequence

path_infered_age = '/data/zmining/.anonymous/data/inputs/predicted_age_labels/Age_2018_output.csv'

path_input = sys.argv[1]
path_output = sys.argv[2]
model_suffix = sys.argv[3]
path_bundle = sys.argv[5]


# In[5]:


print("training model with \ninput {} \noutput {} \nsuffix {} \ngpu {} \ninfered age {} \nbundle {}"      .format(path_input, path_output, model_suffix, gpu_index, path_infered_age, path_bundle))


# In[6]:


max_len = 150
max_kw = 25


# In[7]:


with open(path_bundle, 'rb') as f:
    bundle = pickle.load(f)


# In[8]:


def filter_activities(df_activity):
    print('Before filter nans: {}'.format(len(df_activity)))
    df_activity = df_activity.dropna()
    df_activity.gender = df_activity.gender.astype(np.int32)
    print('Before filter genders: {}'.format(len(df_activity)))
    df_activity = df_activity[df_activity.gender.apply(lambda x:x in [1,2])]
    print('Before filter ages: {}'.format(len(df_activity)))
    df_activity = df_activity[df_activity.age.apply(lambda x:x > 10 and x < 65)]
    print('Before filter videos: {}'.format(len(df_activity)))
    filter_article =  lambda x: ' '.join([article for article in x.split() if int(article)>9999999])
    df_activity.article_ids=df_activity.article_ids.apply(filter_article)
    print('Before filter short seqs: {}'.format(len(df_activity)))
    df_activity = df_activity[df_activity.article_ids.apply(lambda x:len(x.split())>3)]
    print('Before filter 39: {}'.format(len(df_activity)))    
    df_activity = df_activity[np.abs(df_activity.age_pred - 39.) > 0.00001]
    print('After filtering: {}'.format(len(df_activity)))    
    return df_activity

def preprocess_train(path_train, path_age_infered):
    print(path_train)
    print(path_age_infered)
    df_train = pd.read_csv(path_train, names=['zalo_id', 'article_ids', 'log_time', 'age', 'gender'])
    df_train = df_train.dropna()
    df_train.zalo_id = df_train.zalo_id.astype(np.int64)
#     df_train.age = df_train.age.apply(lambda x:x.split()[0])
#     df_train.gender = df_train.gender.apply(lambda x:x.split()[0])
    df_train.age = df_train.age.astype(np.float32)
    df_train.gender = df_train.gender.astype(np.int16)
    print("Len train before filter age: {}".format(len(df_train)))
    df_age = pd.read_csv(path_age_infered)
    df_train = df_train.merge(df_age, on='zalo_id', how='left')
    df_train.age_pred = df_train['age_pred'].fillna(df_train.age)
    df_train = filter_activities(df_train)
    df_train = df_train.drop(['age'], axis=1)
    df_train = df_train.rename(index=str, columns={'age_pred':'age'})
    print("Len train after filter age: {}".format(len(df_train)))
    return df_train[['zalo_id', 'article_ids', 'gender', 'age']]


train = preprocess_train(path_input, path_infered_age)


# In[9]:


df_train, df_valid = train_test_split(train, test_size = 0.01, random_state = 100)
del train

x_train = bundle['article_tokenizer'].texts_to_sequences(df_train.article_ids.values)
x_val = bundle['article_tokenizer'].texts_to_sequences(df_valid.article_ids.values)

df_train = df_train[['zalo_id', 'age', 'gender']]
print('DEBUG: Using {} to train and {} to valid'.format(len(x_train), len(x_val)))

x_train = sequence.pad_sequences(x_train, maxlen=max_len, padding='post',truncating='post')
x_val = sequence.pad_sequences(x_val, maxlen=max_len, padding='post',truncating='post')
print('DEBUG: Shape train {} val {}'.format(x_train.shape, x_val.shape))

kw_seq = bundle['kw_tokenizer'].texts_to_sequences(bundle['article_union'].xkw.values)
kw_seq = sequence.pad_sequences(kw_seq, maxlen=max_kw, padding='post',truncating='post')

def build_kw_map_model(input_layer):
    import tensorflow as tf
    x = Embedding(len(kw_seq)+1, max_kw, trainable=False, name='emb_article', weights=[np.concatenate([np.zeros((1, 25)), kw_seq])])(input_layer)
    x = Flatten(name='kw_flatten')(x)
    return Lambda(lambda x: tf.cast(x, tf.int32), name='lambda_cast_int32')(x)

input_layer = Input((max_len,))
kw_model = Model([input_layer], build_kw_map_model(input_layer))
kw_model.summary()

def extract_core_model(model, start_layer, core_model_builder):
    core_input = Input((max_len * max_kw, ))
    core_model = Model([core_input], core_model_builder(core_input))
    print("Core model: ")
    print(core_model.summary())
    print("Pretrained model: ")
    print(model.summary())
    for i in range(1, len(core_model.layers)):
        print('OG weights {} {} | new weights {} {}'.format(core_model.layers[i].name, len(core_model.layers[i].weights), model.layers[start_layer+i].name, len(model.layers[start_layer+i].get_weights())))
        core_model.layers[i].set_weights(model.layers[start_layer+i].get_weights())
    return core_model

#x_train = kw_model.predict(x_train, batch_size=32, verbose=1)
#x_val = kw_model.predict(x_val, batch_size=32, verbose=1)

print('DEBUG: Shape kw train {} val {}'.format(x_train.shape, x_val.shape))
num_kws = len(bundle['kw_tokenizer'].word_index) + 1
num_articles = len(bundle['article_tokenizer'].word_index) + 1
num_features = 300

def build_model_gender(input_layer):
    x = Embedding(num_kws, num_features, trainable=False, name='kw_embedding', weights=[bundle['kw_embedding']])(input_layer)
    cnn = Conv1D(16, max_kw, padding='same', activation='relu', strides=max_kw, kernel_initializer=he_uniform(seed=0), name='cnn')(x)
    a = GlobalAveragePooling1D(name='cnn_global_avg_pooling')(cnn)
    b = GlobalMaxPooling1D(name='cnn_global_max_pooling')(cnn)
    cnn_lstm = Conv1D(16, max_kw, padding='same', activation='relu', strides=max_kw, kernel_initializer=he_uniform(seed=0), name='cnn_lstm')(x)
    c = Bidirectional(CuDNNLSTM(16,kernel_initializer=he_uniform(seed=0), return_sequences=True), name='bilstm')(cnn_lstm)
    c_1 = GlobalAveragePooling1D(name='cnn_lstm_global_avg_pooling')(c)
    c_2 = GlobalMaxPooling1D(name='cnn_lstm_global_max_pooling')(c)
    cnn_gru = Conv1D(16, max_kw, padding='same', activation='relu', strides=max_kw, kernel_initializer=he_uniform(seed=0), name='cnn_gru')(x)
    d = Bidirectional(CuDNNGRU(16,kernel_initializer=he_uniform(seed=0), return_sequences=True), name='bigru')(cnn_gru)
    d_1 = GlobalAveragePooling1D(name='cnn_gru_global_avg_pooling')(d)
    d_2 = GlobalMaxPooling1D(name='cnn_gru_global_max_pooling')(d)
    x = concatenate([a, b, c_1, c_2, d_1, d_2], name='final_concat')
    x = Dense(32, activation='relu', kernel_initializer=he_uniform(seed=0), name='final_dense')(x)
    x = Dense(1, activation='sigmoid', kernel_initializer=he_uniform(seed=0), name='output')(x)
    return x
    

def build_model_age(input_layer):
    x = Embedding(num_kws, num_features, trainable=False, name='kw_embedding', weights=[bundle['kw_embedding']])(input_layer)
    cnn = Conv1D(16, max_kw, padding='same', activation='relu', strides=max_kw, kernel_initializer=he_uniform(seed=0), name='cnn')(x)
    a = GlobalAveragePooling1D(name='cnn_global_avg_pooling')(cnn)
    b = GlobalMaxPooling1D(name='cnn_global_max_pooling')(cnn)
    cnn_lstm = Conv1D(16, max_kw, padding='same', activation='relu', strides=max_kw, kernel_initializer=he_uniform(seed=0), name='cnn_lstm')(x)
    c = Bidirectional(CuDNNLSTM(16,kernel_initializer=he_uniform(seed=0), return_sequences=True), name='bilstm')(cnn_lstm)
    c_1 = GlobalAveragePooling1D(name='cnn_lstm_global_avg_pooling')(c)
    c_2 = GlobalMaxPooling1D(name='cnn_lstm_global_max_pooling')(c)
    cnn_gru = Conv1D(16, max_kw, padding='same', activation='relu', strides=max_kw, kernel_initializer=he_uniform(seed=0), name='cnn_gru')(x)
    d = Bidirectional(CuDNNGRU(16,kernel_initializer=he_uniform(seed=0), return_sequences=True), name='bigru')(cnn_gru)
    d_1 = GlobalAveragePooling1D(name='cnn_gru_global_avg_pooling')(d)
    d_2 = GlobalMaxPooling1D(name='cnn_gru_global_max_pooling')(d)
    x = concatenate([a, b, c_1, c_2, d_1, d_2], name='final_concat')
    x = Dense(32, activation='relu', kernel_initializer=he_uniform(seed=0), name='final_dense')(x)
    x = Dense(1, activation=None,kernel_initializer=he_uniform(seed=0))(x)
    return x


input_gender = kw_model.output
#input_gender = Input((max_len*max_kw,), name='input')

model_gender = Model([input_layer], build_model_gender(input_gender))
#model_gender = Model([input_gender], build_model_gender(input_gender))
model_gender.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])


print(model_gender.summary())

input_age = kw_model.output
#input_age = Input((max_len*max_kw,), name='input')
model_age = Model([input_layer], build_model_age(input_age))
#model_age = Model([input_age], build_model_age(input_age))
model_age.compile(loss='mae',
              optimizer='adam',
              metrics=['mse', 'mae'])

print(model_age.summary())


model_gender.fit(x_train, df_train.gender - 1, batch_size=64, verbose=2,shuffle=True, epochs=2, validation_data=(x_val, df_valid.gender-1))



#model_gender.fit(x_train, df_train.gender - 1, batch_size=256,           verbose=1,shuffle=True,          epochs=1, validation_data=(x_val, df_valid.gender-1))

core_gender_model = extract_core_model(model_gender, 3, build_model_gender)

model_json = core_gender_model.to_json()
with open("models/model_gender_zep{}.json".format(model_suffix), "w") as json_file:
    json_file.write(model_json)
core_gender_model.save_weights("models/model_gender_zep{}.h5".format(model_suffix))


model_age.fit(x_train, df_train.age, batch_size=64, verbose=2,shuffle=True, epochs=2, validation_data=(x_val, df_valid.age))


#model_age.fit(x_train, df_train.age, batch_size=256,           verbose=1,shuffle=True,          epochs=1, validation_data=(x_val, df_valid.age))

core_age_model = extract_core_model(model_age, 3, build_model_age)

model_json = core_age_model.to_json()
with open("models/model_age_zep{}.json".format(model_suffix), "w") as json_file:
    json_file.write(model_json)
core_age_model.save_weights("models/model_age_zep{}.h5".format(model_suffix))


os.mknod(path_output)

