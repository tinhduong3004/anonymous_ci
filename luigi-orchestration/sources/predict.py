#import fastparquet as fp
import os

os.system("echo Start predict | tee -a /data/zmining/.anonymous/logs/run_hourly.log")

os.system("echo python version: $(which python) | tee -a /data/zmining/.anonymous/logs/run_hourly.log")

import sys
os.environ["CUDA_VISIBLE_DEVICES"]="7"
import pandas as pd
import numpy as np
pd.options.display.max_rows = 200
import warnings
from sklearn.model_selection import train_test_split,  KFold
from sklearn.metrics import roc_auc_score
from sklearn.metrics import f1_score, roc_auc_score
warnings.filterwarnings('ignore')
import json
import gc
import math
from tqdm import tqdm_notebook
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_auc_score
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import pickle
import tensorflow as tf

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras import  backend as K
from keras.models import Model, load_model
from keras.layers import LSTM, CuDNNGRU, CuDNNLSTM, Dense, Bidirectional, Input, SpatialDropout1D,Embedding, \
        BatchNormalization, GlobalMaxPooling1D, GlobalAveragePooling1D, concatenate, Conv1D, Flatten, Dropout
from keras.engine.topology import Layer
from keras import initializers, regularizers, constraints
from keras.initializers import he_uniform
from keras.models import model_from_json


pd.set_option('display.max_colwidth', -1)

from sklearn.model_selection import train_test_split, StratifiedShuffleSplit, GroupShuffleSplit
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, EarlyStopping, ReduceLROnPlateau
from keras.preprocessing import text, sequence

import sys

os.system("echo import all libs OK| tee -a /data/zmining/.anonymous/logs/run_hourly.log")
path_input = sys.argv[1]
path_output = sys.argv[2]

os.system("echo input_{}_output_{} | tee -a /data/zmining/.anonymous/logs/run_hourly.log".format(path_input, path_output))

X_test = pd.read_csv(path_input,
                  header = None)
print("X_test before filter {}".format(len(X_test)))
X_test = X_test[X_test[1].apply(lambda x: len(x.split())>3)]
print("X_test after filter {}".format(len(X_test)))
# sys.exit()
with open('models/tokenizer_history.pickle', 'rb') as handle:
    tokenizer_history = pickle.load(handle)

x_test = tokenizer_history.texts_to_sequences(X_test[1].values)
x_test = sequence.pad_sequences(x_test, maxlen=300, padding='post',truncating='post')

json_file = open('models/model_age.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
model_age = model_from_json(loaded_model_json)
model_age.load_weights("models/model_age.h5")

X_test['age_preds'] = model_age.predict(x_test, batch_size=1024, verbose=1).squeeze()

X_test.columns = ['id','history','timestamp','age_preds']
X_test[['id', 'age_preds']].to_csv(path_output, index=False)

