#import fastparquet as fp
import os

os.system("echo Start predict | tee -a /data/zmining/.anonymous/logs/run_hourly_zmp3_mobile.log")

os.system("echo python version: $(which python) | tee -a /data/zmining/.anonymous/logs/run_hourly_zmp3_mobile.log")

import sys
gpu=model_suffix = sys.argv[4]
os.environ["CUDA_VISIBLE_DEVICES"]=str(gpu)
import pandas as pd
import numpy as np
pd.options.display.max_rows = 200
import warnings
from sklearn.model_selection import train_test_split,  KFold
from sklearn.metrics import roc_auc_score
from sklearn.metrics import f1_score, roc_auc_score
warnings.filterwarnings('ignore')
import json
import gc
import math
from tqdm import tqdm_notebook
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_auc_score
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import pickle
import tensorflow as tf

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras import  backend as K
from keras.models import Model, load_model
from keras.layers import LSTM, CuDNNGRU, CuDNNLSTM, Dense, Bidirectional, Input, SpatialDropout1D,Embedding, \
        BatchNormalization, GlobalMaxPooling1D, GlobalAveragePooling1D, concatenate, Conv1D, Flatten, Dropout
from keras.engine.topology import Layer
from keras import initializers, regularizers, constraints
from keras.initializers import he_uniform
from keras.models import model_from_json
from scipy.stats import norm

pd.set_option('display.max_colwidth', -1)

from sklearn.model_selection import train_test_split, StratifiedShuffleSplit, GroupShuffleSplit
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, EarlyStopping, ReduceLROnPlateau
from keras.preprocessing import text, sequence
from datetime import datetime
import sys

os.system("echo import all libs OK| tee -a /data/zmining/.anonymous/logs/run_hourly_zmp3_mobile.log")
path_input = sys.argv[1]
path_output = sys.argv[2]

model_suffix = sys.argv[3]
train_path = sys.argv[5]

print("predict input {} output {} suffix {} gpu {}".format(path_input, path_output, model_suffix, gpu))

os.system("echo input_{}_output_{} | tee -a /data/zmining/.anonymous/logs/run_hourly_zmp3_mobile.log".format(path_input, path_output))

X_test = pd.read_csv(path_input, header = None)
X_test.head()
print("X_test before filter {}".format(len(X_test)))
X_test = X_test[X_test[1].apply(lambda x: len(x.split())>3)]
print("X_test after filter {}".format(len(X_test)))
# sys.exit()
with open('models/tokenizer_history_zmp3_mobile{}.pickle'.format(model_suffix), 'rb') as handle:
    tokenizer_history = pickle.load(handle)

x_test = tokenizer_history.texts_to_sequences(X_test[1].values)
x_test = sequence.pad_sequences(x_test, maxlen=300, padding='post',truncating='post')

json_file = open('models/model_age_zmp3_mobile{}.json'.format(model_suffix), 'r')
loaded_model_json = json_file.read()
json_file.close()
model_age = model_from_json(loaded_model_json)
model_age.load_weights("models/model_age_zmp3_mobile{}.h5".format(model_suffix))


json_file = open('models/model_gender_zmp3_mobile{}.json'.format(model_suffix), 'r')
loaded_model_json = json_file.read()
json_file.close()
model_gender = model_from_json(loaded_model_json)
model_gender.load_weights("models/model_gender_zmp3_mobile{}.h5".format(model_suffix))


X_test['age_preds'] = model_age.predict(x_test, batch_size=1024, verbose=1).squeeze()
X_test['gender_preds'] = model_gender.predict(x_test, batch_size=1024, verbose=1).squeeze()

max_len=300
short_seq_limit=3

def zero_count(x):
    return max_len - sum(x==0)

def unique_count(x):
    return len(np.unique(x))

def unique_count_str(x):
    return len(np.unique(x.split()))

def clean_age(df, low=10, high=65):
    days_passed = datetime.today() - datetime(year=datetime.today().year, month=1, day=1)
    year_progress = min(365, days_passed.days) / 365
    abnormal_low = year_progress - .04
    abnormal_high = year_progress + .04
    for age in range(low, high):
        df = df[(df.age < age+abnormal_low) | (df.age > age+abnormal_high)]
    return df

def preprocess_train(path_train):
    print(path_train)
    df_train = pd.read_csv(path_train, names=['zalo_id', 'article_ids', 'kind', 'log_time', 'age', 'gender'])
    print("Len of df_train {}".format(len(df_train)))
    df_train = df_train.dropna()
    df_train.zalo_id = df_train.zalo_id.astype(np.int64)
#     df_train.age = df_train.age.apply(lambda x:x.split()[0])
#     df_train.gender = df_train.gender.apply(lambda x:x.split()[0])
    df_train.age = df_train.age.astype(np.float32)
    df_train.gender = df_train.gender.astype(np.int16)
    print("Len train before filter age: {}".format(len(df_train)))
#    df_age = pd.read_csv(path_age_infered)
#    df_train = df_train.merge(df_age, on='zalo_id', how='left')
#    df_train.age_pred = df_train['age_pred'].fillna(df_train.age)
#    df_train = df_train[np.abs(df_train.age_pred - 39.) > 0.00001]
    df_train = df_train[df_train.age>10]
    df_train = df_train[df_train.age<65]
    df_train = df_train[df_train.gender.apply(lambda x: x in [1,2])]
#    df_train = df_train.drop(['age'], axis=1)
    df_train = clean_age(df_train)
#    df_train = df_train.rename(index=str, columns={'age_pred':'age'})
    print("Len train after filter age: {}".format(len(df_train)))
    return df_train[['zalo_id', 'article_ids', 'gender', 'age']]

#X_train = preprocess_train(train_path)

#norm_test = norm.fit(X_test.age_preds[(X_test.age_preds>18)&(X_test.age_preds<45)])
#norm_train = norm.fit(X_train[(X_train.age>18)&(X_train.age<45)].age)
#std_scale = norm_train[1] / norm_test[1]

#pred_median_shift = (X_test.age_preds+(X_train.age.median()-X_test.age_preds.median()))
#X_test['age_preds'] = pred_median_shift + std_scale*(pred_median_shift - pred_median_shift.mean())
#X_test['age_preds'] = X_test['age_preds'].clip(10,65)

X_test.columns = ['id','history', 'kind', 'interval', 'age_preds', 'gender_preds']
X_test[['id', 'age_preds', 'gender_preds']].to_csv(path_output, index=False)

