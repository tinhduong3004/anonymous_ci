import os
import sys
import pandas as pd
import numpy as np
import warnings
import pickle
from keras import backend as K
from keras.models import Model
from keras.layers import CuDNNLSTM, Dense, Bidirectional, Input, Embedding, GlobalMaxPooling1D, GlobalAveragePooling1D, concatenate
from keras.initializers import he_uniform
from multiprocessing import Pool
from datetime import datetime
from sklearn.model_selection import train_test_split
from keras.preprocessing import text, sequence

warnings.filterwarnings('ignore')
path_infered_age = '/data/zmining/.anonymous/data/inputs/predicted_age_labels/Age_2018_output.csv'
path_input = sys.argv[1]
path_output = sys.argv[2]
model_suffix = sys.argv[3]
gpu_index = sys.argv[4]
os.environ["CUDA_VISIBLE_DEVICES"]=str(gpu_index)
os.system("echo -e importing | tee -a /data/zmining/.anonymous/logs/train_daily_znews.log")
os.system("echo -e GPU:{} | tee -a /data/zmining/.anonymous/logs/train_daily_znews.log".format(gpu_index))

print("training model with input {} output {} suffix {} gpu {} infered age {}".format(path_input, path_output, model_suffix, gpu_index, path_infered_age))
os.system("echo -e TRAINING | tee -a /data/zmining/.anonymous/logs/train_daily_znews.log")
print("model_suffix {}".format(model_suffix))

max_len=300
short_seq_limit=3

def zero_count(x):
    return max_len - sum(x==0)

def unique_count(x):
    return len(np.unique(x))

def unique_count_str(x):
    return len(np.unique(x.split()))

def clean_age(df, low=10, high=65):
    days_passed = datetime.today() - datetime(year=datetime.today().year, month=1, day=1)
    year_progress = min(365, days_passed.days) / 365
    abnormal_low = year_progress - .04
    abnormal_high = year_progress + .04
    for age in range(low, high):
        df = df[(df.age < age+abnormal_low) | (df.age > age+abnormal_high)]
    return df

def preprocess_train(path_train, path_age_infered):
    print(path_train)
    print(path_age_infered)
    df_train = pd.read_csv(path_train, names=['zalo_id', 'article_ids', 'client_os', 'client_browser', 'platform', 'age', 'gender', 'log_time'])
    df_train = df_train.dropna()
    df_train.zalo_id = df_train.zalo_id.astype(np.int64)
    def filter_olds(inp): return ' '.join(article for article in str(inp).split() if int(article)>500000)
    df_train.article_ids = df_train.article_ids.apply(filter_olds)
    df_train.age = df_train.age.astype(np.float32)
    df_train.gender = df_train.gender.astype(np.int16)
    print("Len train before filter age: {}".format(len(df_train)))
    df_train = df_train[df_train.age>10]
    df_train = df_train[df_train.age<65]
    df_train = df_train[df_train.gender.apply(lambda x: x in [1,2])]
    print("Len train before clean age: {}".format(len(df_train)))
    df_train = clean_age(df_train)
    print("Len train before short uniques: {}".format(len(df_train)))
    with Pool(8) as pool:
        unique_id_counts = pool.map(unique_count_str, df_train.article_ids)
    df_train = df_train[np.array(unique_id_counts) > short_seq_limit]
    print("Len train after filter age: {}".format(len(df_train)))
    return df_train[['zalo_id', 'article_ids', 'gender', 'age']]

train = preprocess_train(path_input, path_infered_age)

train['history_count'] = train.article_ids.apply(lambda x: len(x.split()))
print("before remove short sentence {}".format(len(train)))
train = train[train.history_count > 9]
train = train.drop(['history_count'], axis=1)
print("after remove short sentence {}".format(len(train)))

X_train, X_valid = train_test_split(train, test_size = 0.01, random_state = 100)

X_train.reset_index(drop=True, inplace = True)
X_valid.reset_index(drop=True, inplace = True)

tokenizer_history = text.Tokenizer(filters='\t\n', lower=False)
tokenizer_history.fit_on_texts(list(X_train.article_ids.values))

x_train = tokenizer_history.texts_to_sequences(X_train.article_ids.values)
x_train = sequence.pad_sequences(x_train, maxlen=300, padding='post',truncating='post')
x_valid = tokenizer_history.texts_to_sequences(X_valid.article_ids.values)
x_valid = sequence.pad_sequences(x_valid, maxlen=300, padding='post',truncating='post')

with open('models/tokenizer_history_znews{}.pickle'.format(model_suffix), 'wb') as handle:
    pickle.dump(tokenizer_history, handle)


embedding_matrix = np.zeros((np.max(x_train) + 1, 300))

def mean_absolute_error(y_true, y_pred):
    return K.sqrt(K.mean(K.square(y_pred - y_true), axis=-1))


def build_model_age(input_layer):
    x = Embedding(embedding_matrix.shape[0], 300, trainable= True)(input_layer)
    y = Bidirectional(CuDNNLSTM(64, kernel_initializer=he_uniform(seed=0), return_sequences=True))(x)
    a = GlobalAveragePooling1D()(y)
    b = GlobalMaxPooling1D()(y)
    x = concatenate([a, b])
    x = Dense(32, activation='relu',kernel_initializer=he_uniform(seed=0))(x)
    x = Dense(1, activation=None,kernel_initializer=he_uniform(seed=0))(x)
    return x


def build_model_gender(input_layer):
    x = Embedding(embedding_matrix.shape[0], 300, trainable= True)(input_layer)
    y = Bidirectional(CuDNNLSTM(64, kernel_initializer=he_uniform(seed=0), return_sequences=True))(x)
    a = GlobalAveragePooling1D()(y)
    b = GlobalMaxPooling1D()(y)
    x = concatenate([a, b])
    x = Dense(32, activation='relu',kernel_initializer=he_uniform(seed=0))(x)
    x = Dense(1, activation='sigmoid',kernel_initializer=he_uniform(seed=0))(x)
    return x


input_layer = Input((300,))
output_layer_age = build_model_age(input_layer)
output_layer_gender = build_model_gender(input_layer)

model_gender = Model([input_layer], output_layer_gender)

model_gender.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

model_age = Model([input_layer], output_layer_age)

model_age.compile(loss='mae',
              optimizer='adam',
              metrics=['mae'])

model_gender.fit(x_train, X_train.gender - 1, batch_size=128, \
          verbose=1,shuffle=True,\
          epochs=1, validation_data=(x_valid,X_valid.gender-1))
model_gender.fit(x_train, X_train.gender - 1, batch_size=256, \
          verbose=1,shuffle=True,\
          epochs=1, validation_data=(x_valid, X_valid.gender-1))

model_json = model_gender.to_json()
with open("models/model_gender_znews{}_v2.json".format(model_suffix), "w") as json_file:
    json_file.write(model_json)
model_gender.save_weights("models/model_gender_znews{}_v2.h5".format(model_suffix))

model_age.fit(x_train, X_train.age, batch_size=128, \
          verbose=1,shuffle=True,\
          epochs=1, validation_data=(x_valid,X_valid.age.clip(10,65)))
model_age.fit(x_train, X_train.age, batch_size=256, \
          verbose=1,shuffle=True,\
          epochs=1, validation_data=(x_valid, X_valid.age.clip(10,65)))


model_json = model_age.to_json()
with open("models/model_age_znews{}_v2.json".format(model_suffix), "w") as json_file:
    json_file.write(model_json)
model_age.save_weights("models/model_age_znews{}_v2.h5".format(model_suffix))

os.mknod(path_output)
