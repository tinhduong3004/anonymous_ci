#import fastparquet as fp
import os

os.system("echo Start predict | tee -a /data/zmining/.anonymous/logs/run_hourly_ads.log")

os.system("echo python version: $(which python) | tee -a /data/zmining/.anonymous/logs/run_hourly_ads.log")

import sys
gpu=model_suffix = sys.argv[4]
os.environ["CUDA_VISIBLE_DEVICES"]=str(gpu)
import pandas as pd
import numpy as np
pd.options.display.max_rows = 200
import warnings
from sklearn.model_selection import train_test_split,  KFold
from sklearn.metrics import roc_auc_score
from sklearn.metrics import f1_score, roc_auc_score
warnings.filterwarnings('ignore')
import json
import gc
import math
from tqdm import tqdm_notebook
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_auc_score
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import pickle
import tensorflow as tf

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras import  backend as K
from keras.models import Model, load_model
from keras.layers import LSTM, CuDNNGRU, CuDNNLSTM, Dense, Bidirectional, Input, SpatialDropout1D,Embedding, \
        BatchNormalization, GlobalMaxPooling1D, GlobalAveragePooling1D, concatenate, Conv1D, Flatten, Dropout
from keras.engine.topology import Layer
from keras import initializers, regularizers, constraints
from keras.initializers import he_uniform
from keras.models import model_from_json


pd.set_option('display.max_colwidth', -1)

from sklearn.model_selection import train_test_split, StratifiedShuffleSplit, GroupShuffleSplit
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, EarlyStopping, ReduceLROnPlateau
from keras.preprocessing import text, sequence

import sys

os.system("echo import all libs OK| tee -a /data/zmining/.anonymous/logs/run_hourly_ads.log")
path_input = sys.argv[1]
path_output = sys.argv[2]

model_suffix = sys.argv[3]

print("predict input {} output {} suffix {} gpu {}".format(path_input, path_output, model_suffix, gpu))

os.system("echo input_{}_output_{} | tee -a /data/zmining/.anonymous/logs/run_hourly_ads.log".format(path_input, path_output))

X_test = pd.read_csv(path_input,
                  header = None)
X_test.head()
print("X_test before filter {}".format(len(X_test)))
X_test = X_test[X_test[1].apply(lambda x: len(x.split())>3)]
print("X_test after filter {}".format(len(X_test)))
# sys.exit()
with open('models/tokenizer_history_ads{}.pickle'.format(model_suffix), 'rb') as handle:
    tokenizer_history = pickle.load(handle)

x_test = tokenizer_history.texts_to_sequences(X_test[1].values)
x_test = sequence.pad_sequences(x_test, maxlen=300, padding='post',truncating='post')

json_file = open('models/model_age_ads{}.json'.format(model_suffix), 'r')
loaded_model_json = json_file.read()
json_file.close()
model_age = model_from_json(loaded_model_json)
model_age.load_weights("models/model_age_ads{}.h5".format(model_suffix))


json_file = open('models/model_gender_ads{}.json'.format(model_suffix), 'r')
loaded_model_json = json_file.read()
json_file.close()
model_gender = model_from_json(loaded_model_json)
model_gender.load_weights("models/model_gender_ads{}.h5".format(model_suffix))


X_test['age_preds'] = model_age.predict(x_test, batch_size=1024, verbose=1).squeeze()
X_test['gender_preds'] = model_gender.predict(x_test, batch_size=1024, verbose=1).squeeze()


X_test.columns = ['id','history', 'kind','timestamp', 'age_preds', 'gender_preds']
X_test[['id', 'age_preds', 'gender_preds']].to_csv(path_output, index=False)

