#import fastparquet as fp
import os
import pandas as pd
import numpy as np
pd.options.display.max_rows = 200
import warnings
warnings.filterwarnings('ignore')
from tqdm import tqdm
import pickle
from keras.models import model_from_json
pd.set_option('display.max_colwidth', -1)

from keras.preprocessing import text, sequence

from multiprocessing import Pool
from datetime import datetime
from scipy.stats import norm

import sys

os.system("echo import all libs OK| tee -a /data/zmining/.anonymous/logs/run_hourly_znews.log")
path_input = sys.argv[1]
path_output = sys.argv[2]
model_suffix = sys.argv[3]
gpu=model_suffix = sys.argv[4]
os.environ["CUDA_VISIBLE_DEVICES"]=str(gpu)
path_train = sys.argv[5]
os.system("echo Start predict | tee -a /data/zmining/.anonymous/logs/run_hourly_znews.log")
os.system("echo python version: $(which python) | tee -a /data/zmining/.anonymous/logs/run_hourly_znews.log")

print("predict input {} output {} suffix {} gpu {}".format(path_input, path_output, model_suffix, gpu))

os.system("echo input_{}_output_{} | tee -a /data/zmining/.anonymous/logs/run_hourly_znews.log".format(path_input, path_output))

os.system("echo train path {} | tee -a /data/zmining/.anonymous/logs/run_hourly_znews.log".format(path_train))

max_len=300
short_seq_limit=4

def zero_count(x):
    return max_len - sum(x==0)

def unique_count(x):
    return len(np.unique(x))

def unique_count_str(x):
    return len(np.unique(x.split()))

def clean_age(df, low=10, high=65):
    print("\n\n\n")
    print(df.columns)
    days_passed = datetime.today() - datetime(year=datetime.today().year, month=1, day=1)
    year_progress = min(365, days_passed.days) / 365
    abnormal_low = year_progress - .04
    abnormal_high = year_progress + .04
    for age in range(low, high):
        df = df[(df.age < age+abnormal_low) | (df.age > age+abnormal_high)]
    return df


X_test = pd.read_csv(path_input,
                  header = None)
X_test.head()
print("X_test before filter {}".format(len(X_test)))
def filter_olds(inp): return ' '.join(article for article in str(inp).split() if int(article)>500000)
X_test[1] = X_test[1].apply(filter_olds)
X_test = X_test[X_test[1].apply(lambda x: len(x.split())>short_seq_limit)]
print("X_test after filter {}".format(len(X_test)))
# sys.exit()
with open('models/tokenizer_history_znews{}.pickle'.format(model_suffix), 'rb') as handle:
    tokenizer_history = pickle.load(handle)

x_test = tokenizer_history.texts_to_sequences(X_test[1].values)
x_test = sequence.pad_sequences(x_test, maxlen=300, padding='post',truncating='post')

json_file = open('models/model_age_znews{}_v2.json'.format(model_suffix), 'r')
loaded_model_json = json_file.read()
json_file.close()
model_age = model_from_json(loaded_model_json)
model_age.load_weights("models/model_age_znews{}_v2.h5".format(model_suffix))


json_file = open('models/model_gender_znews{}_v2.json'.format(model_suffix), 'r')
loaded_model_json = json_file.read()
json_file.close()
model_gender = model_from_json(loaded_model_json)
model_gender.load_weights("models/model_gender_znews{}_v2.h5".format(model_suffix))

x_test = tokenizer_history.texts_to_sequences(X_test[1].values)
x_test = sequence.pad_sequences(x_test, maxlen=max_len, padding='post',truncating='post')

with Pool(8) as pool:
    test_unique_counts = pool.map(unique_count, tqdm(x_test))

print("X_test before filter short unique {}".format(len(x_test)))
x_test = x_test[np.array(test_unique_counts) > short_seq_limit+1]
X_test = X_test[np.array(test_unique_counts) > short_seq_limit+1]
print("X_test after filter {}".format(len(x_test)))


X_test['age_preds'] = model_age.predict(x_test, batch_size=1024, verbose=1).squeeze()
X_test['gender_preds'] = model_gender.predict(x_test, batch_size=1024, verbose=1).squeeze()

def preprocess_train(path_train):
    print(path_train)
    df_train = pd.read_csv(path_train, names=['zalo_id', 'article_ids', 'client_os', 'client_browser', 'platform', 'age', 'gender', 'log_time'])
    df_train = df_train.dropna()
    df_train.zalo_id = df_train.zalo_id.astype(np.int64)
    def filter_olds(inp): return ' '.join(article for article in str(inp).split() if int(article)>500000)
    df_train.article_ids = df_train.article_ids.apply(filter_olds)
    df_train.age = df_train.age.astype(np.float32)
    df_train.gender = df_train.gender.astype(np.int16)
    print("Len train before filter age: {}".format(len(df_train)))
    df_train = df_train[df_train.age>10]
    df_train = df_train[df_train.age<65]
    df_train = df_train[df_train.gender.apply(lambda x: x in [1,2])]
    print("Len train before clean age: {}".format(len(df_train)))
    df_train = clean_age(df_train)
    print("Len train before short uniques: {}".format(len(df_train)))
    with Pool(8) as pool:
        unique_id_counts = pool.map(unique_count_str, df_train.article_ids)
    df_train = df_train[np.array(unique_id_counts) > short_seq_limit]
    print("Len train after filter age: {}".format(len(df_train)))
    return df_train[['zalo_id', 'article_ids', 'gender', 'age']]

X_train = preprocess_train(path_train)

norm_test = norm.fit(X_test.age_preds[(X_test.age_preds>18)&(X_test.age_preds<45)])
norm_train = norm.fit(X_train[(X_train.age>18)&(X_train.age<45)].age)
std_scale = norm_train[1] / norm_test[1]

pred_median_shift = (X_test.age_preds+(X_train.age.median()-X_test.age_preds.median()))
X_test['age_preds'] = pred_median_shift + std_scale*(pred_median_shift - pred_median_shift.mean())
X_test['age_preds'] = X_test['age_preds'].clip(10,65)

X_test.columns = ['id','history', 'client_os', 'client_browser', 'platform', 'timestamp', 'age_preds', 'gender_preds']
X_test[['id', 'age_preds', 'gender_preds']].to_csv(path_output, index=False)

