#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
# sys.argv = ['predict_zep.py',
#            '/data/zmining/.anonymous/data/inputs/zep_activity_pred/2000/02/01/data.csv',
#            '/data/zmining/.anonymous/data/outputs/zep/outputs/zep/2000/02/01/results.csv',
#            '_zep_2_weeks_2000_02_01',
#            '4', 
#             '/data/zmining/.anonymous/data/intermediate/2000/02/01/intermediate.bundle']


# In[6]:


#import fastparquet as fp
import os

os.system("echo Start predict | tee -a /data/zmining/.anonymous/logs/run_hourly_zep.log")

os.system("echo python version: $(which python) | tee -a /data/zmining/.anonymous/logs/run_hourly_zep.log")

gpu = sys.argv[4]
os.environ["CUDA_VISIBLE_DEVICES"]=str(gpu)
import pandas as pd
import numpy as np
pd.options.display.max_rows = 200
import warnings
from sklearn.model_selection import train_test_split,  KFold
from sklearn.metrics import roc_auc_score
from sklearn.metrics import f1_score, roc_auc_score
warnings.filterwarnings('ignore')
import json
import gc
import math
from tqdm import tqdm_notebook
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_auc_score
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import pickle
import tensorflow as tf

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras import  backend as K
from keras.models import Model, load_model
from keras.layers import LSTM, CuDNNGRU, CuDNNLSTM, Dense, Bidirectional, Input, SpatialDropout1D,Embedding,         BatchNormalization, GlobalMaxPooling1D, GlobalAveragePooling1D, concatenate, Conv1D, Flatten, Dropout, Lambda
from keras.engine.topology import Layer
from keras import initializers, regularizers, constraints
from keras.initializers import he_uniform
from keras.models import model_from_json


pd.set_option('display.max_colwidth', -1)

from sklearn.model_selection import train_test_split, StratifiedShuffleSplit, GroupShuffleSplit
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, EarlyStopping, ReduceLROnPlateau
from keras.preprocessing import text, sequence

import sys


# In[7]:


os.system("echo import all libs OK| tee -a /data/zmining/.anonymous/logs/run_hourly_zep.log")
path_input = sys.argv[1]
path_output = sys.argv[2]

model_suffix = sys.argv[3]
path_bundle = sys.argv[5]

print("predict input {} output {} suffix {} gpu {}".format(path_input, path_output, model_suffix, gpu))

os.system("echo input_{}_output_{} | tee -a /data/zmining/.anonymous/logs/run_hourly_zep.log".format(path_input, path_output))


# In[8]:


max_kw = 25
max_len = 150


# In[9]:


X_test = pd.read_csv(path_input, names=['id', 'article_ids', 'log_time'])


# In[10]:


# X_test.head()


# In[11]:



print("X_test before filter {}".format(len(X_test)))
X_test = X_test[X_test.article_ids.apply(lambda x: len(x.split())>3)]
filter_article =  lambda x: ' '.join([article for article in x.split() if int(article)>9999999])
X_test.article_ids=X_test.article_ids.apply(filter_article)
print("X_test after filter {}".format(len(X_test)))
# sys.exit()


# In[12]:


with open(path_bundle, 'rb') as f:
    bundle = pickle.load(f)

kw_seq = bundle['kw_tokenizer'].texts_to_sequences(bundle['article_union'].xkw.values)
kw_seq = sequence.pad_sequences(kw_seq, maxlen=max_kw, padding='post',truncating='post')
num_kws, num_features = bundle['kw_embedding'].shape
    
def build_kw_map_model(input_layer):
    import tensorflow as tf
    x = Embedding(len(kw_seq)+1, max_kw, trainable=False, name='emb_article', weights=[np.concatenate([np.zeros((1, 25)), kw_seq])])(input_layer)
    x = Flatten(name='kw_flatten')(x)
    return Lambda(lambda x: tf.cast(x, tf.int32), name='lambda_cast_int32')(x)


# In[13]:


# bundle.keys()


# In[14]:

print("DEBUG: Preprocess x_test")
x_test = bundle['article_tokenizer'].texts_to_sequences(X_test.article_ids.values)
x_test = sequence.pad_sequences(x_test, maxlen=max_len, padding='post',truncating='post')


# In[15]:


# x_test.shape


# In[16]:

print("DEBUG: Preprocess kw_seq")
kw_seq = bundle['kw_tokenizer'].texts_to_sequences(bundle['article_union'].xkw.values)
kw_seq = sequence.pad_sequences(kw_seq, maxlen=max_kw, padding='post',truncating='post')


# In[17]:


#def build_kw_map_model(input_layer):
#    x = Embedding(len(kw_seq)+1, max_kw, trainable=False, name='emb_article', weights=[np.concatenate([np.zeros((1, 25)), kw_seq])])(input_layer)
#    x = Flatten(name='kw_flatten')(x)
#    return Lambda(lambda x: tf.cast(x, tf.int32), name='lambda_cast_int32')(x)


# In[18]:


# input_layer = Input((max_len,))
# kw_model = Model([input_layer], build_kw_map_model(input_layer))
# kw_model.summary()


# In[19]:
def reconstruct_model(front_model_builder, back_model_builder, back_model_pretrained, start_layer=3):
    input_base = Input((max_len, ))
    base_model = Model([input_base], back_model_builder(front_model_builder(input_base)))
    print('base {} {}'.format(base_model.layers.__len__(), base_model.layers[-1].name))
    for i in range(1, len(base_model.layers)-start_layer):
        print(i)
        print("{} {} - {} {}".format(start_layer+i, base_model.layers[start_layer+i].name, i, back_model_pretrained.layers[i].name))
        base_model.layers[start_layer+i].set_weights(back_model_pretrained.layers[i].get_weights()) 
    return base_model
    

def build_model_gender(input_layer):
#     input_layer = build_kw_map_model(input_layer)
    x = Embedding(num_kws, num_features, trainable=False, name='kw_embedding', weights=[bundle['kw_embedding']])(input_layer)
    cnn = Conv1D(16, max_kw, padding='same', activation='relu', strides=max_kw, kernel_initializer=he_uniform(seed=0), name='cnn')(x)
    a = GlobalAveragePooling1D(name='cnn_global_avg_pooling')(cnn)
    b = GlobalMaxPooling1D(name='cnn_global_max_pooling')(cnn)
    cnn_lstm = Conv1D(16, max_kw, padding='same', activation='relu', strides=max_kw, kernel_initializer=he_uniform(seed=0), name='cnn_lstm')(x)
    c = Bidirectional(CuDNNLSTM(16,kernel_initializer=he_uniform(seed=0), return_sequences=True), name='bilstm')(cnn_lstm)
    c_1 = GlobalAveragePooling1D(name='cnn_lstm_global_avg_pooling')(c)
    c_2 = GlobalMaxPooling1D(name='cnn_lstm_global_max_pooling')(c)
    cnn_gru = Conv1D(16, max_kw, padding='same', activation='relu', strides=max_kw, kernel_initializer=he_uniform(seed=0), name='cnn_gru')(x)
    d = Bidirectional(CuDNNGRU(16,kernel_initializer=he_uniform(seed=0), return_sequences=True), name='bigru')(cnn_gru)
    d_1 = GlobalAveragePooling1D(name='cnn_gru_global_avg_pooling')(d)
    d_2 = GlobalMaxPooling1D(name='cnn_gru_global_max_pooling')(d)
    x = concatenate([a, b, c_1, c_2, d_1, d_2], name='final_concat')
    x = Dense(32, activation='relu', kernel_initializer=he_uniform(seed=0), name='final_dense')(x)
    x = Dense(1, activation='sigmoid', kernel_initializer=he_uniform(seed=0), name='output')(x)
    return x


def build_model_age(input_layer):
#    input_layer = build_kw_map_model(input_layer)
    x = Embedding(num_kws, num_features, trainable=False, name='kw_embedding', weights=[bundle['kw_embedding']])(input_layer)
    cnn = Conv1D(16, max_kw, padding='same', activation='relu', strides=max_kw, kernel_initializer=he_uniform(seed=0), name='cnn')(x)
    a = GlobalAveragePooling1D(name='cnn_global_avg_pooling')(cnn)
    b = GlobalMaxPooling1D(name='cnn_global_max_pooling')(cnn)
    cnn_lstm = Conv1D(16, max_kw, padding='same', activation='relu', strides=max_kw, kernel_initializer=he_uniform(seed=0), name='cnn_lstm')(x)
    c = Bidirectional(CuDNNLSTM(16,kernel_initializer=he_uniform(seed=0), return_sequences=True), name='bilstm')(cnn_lstm)
    c_1 = GlobalAveragePooling1D(name='cnn_lstm_global_avg_pooling')(c)
    c_2 = GlobalMaxPooling1D(name='cnn_lstm_global_max_pooling')(c)
    cnn_gru = Conv1D(16, max_kw, padding='same', activation='relu', strides=max_kw, kernel_initializer=he_uniform(seed=0), name='cnn_gru')(x)
    d = Bidirectional(CuDNNGRU(16,kernel_initializer=he_uniform(seed=0), return_sequences=True), name='bigru')(cnn_gru)
    d_1 = GlobalAveragePooling1D(name='cnn_gru_global_avg_pooling')(d)
    d_2 = GlobalMaxPooling1D(name='cnn_gru_global_max_pooling')(d)
    x = concatenate([a, b, c_1, c_2, d_1, d_2], name='final_concat')
    x = Dense(32, activation='relu', kernel_initializer=he_uniform(seed=0), name='final_dense')(x)
    x = Dense(1, activation=None,kernel_initializer=he_uniform(seed=0))(x)
    return x

# x_test = kw_model.predict(x_test, batch_size=3584, verbose=1)


# In[21]:

print("DEBUG: Loading age model")
json_file = open("models/model_age_zep{}.json".format(model_suffix), 'r')
loaded_model_json = json_file.read()
json_file.close()
model_age = model_from_json(loaded_model_json, custom_objects={"tf": tf})
model_age.load_weights("models/model_age_zep{}.h5".format(model_suffix))
model_age = reconstruct_model(build_kw_map_model, build_model_age, model_age)
#reconstruct_model(front_model_builder, back_model_builder, back_model_pretrained, start_layer=3)

print("DEBUG: Loading gender model")
json_file = open("models/model_gender_zep{}.json".format(model_suffix), 'r')
loaded_model_json = json_file.read()
json_file.close()
model_gender = model_from_json(loaded_model_json, custom_objects={"tf": tf})
model_gender.load_weights("models/model_gender_zep{}.h5".format(model_suffix))
model_gender = reconstruct_model(build_kw_map_model, build_model_gender, model_gender)

del bundle
# In[23]:

print("DEBUG: Predicting age")
X_test['age_preds'] = model_age.predict(x_test, batch_size=256, verbose=1).squeeze()


# In[24]:

print("DEBUG: Predicting gender")
X_test['gender_preds'] = model_gender.predict(x_test, batch_size=256, verbose=1).squeeze()


# In[26]:


# X_test.head()


# In[28]:

print("DEBUG: Saving predictions!")
X_test.columns = ['id','history', 'log_time', 'age_preds', 'gender_preds']
X_test[['id', 'age_preds', 'gender_preds']].to_csv(path_output, index=False)
print("DEBUG: DONE!")

# In[29]:


# X_test.head()
# In[ ]:



