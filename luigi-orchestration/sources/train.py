#import fastparquet as fp
import os
os.system("echo -e importing | tee -a /data/zmining/.anonymous/logs/train_daily.log")
import sys
os.environ["CUDA_VISIBLE_DEVICES"]="4"
import pandas as pd
import numpy as np
pd.options.display.max_rows = 200
import warnings
from sklearn.model_selection import train_test_split,  KFold
from sklearn.metrics import roc_auc_score
from sklearn.metrics import f1_score, roc_auc_score
warnings.filterwarnings('ignore')
import json
import gc
import math
from tqdm import tqdm_notebook
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_auc_score
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

import pickle
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

from keras.models import Sequential
from keras import  backend as K
from keras.models import Model, load_model
from keras.layers import LSTM, CuDNNGRU, CuDNNLSTM, Dense, Bidirectional, Input, SpatialDropout1D,Embedding, \
        BatchNormalization, GlobalMaxPooling1D, GlobalAveragePooling1D, concatenate, Conv1D, Flatten, Dropout
from keras.engine.topology import Layer
from keras import initializers, regularizers, constraints
from keras.initializers import he_uniform
from keras.models import model_from_json


pd.set_option('display.max_colwidth', -1)

from sklearn.model_selection import train_test_split, StratifiedShuffleSplit, GroupShuffleSplit
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, EarlyStopping, ReduceLROnPlateau
from keras.preprocessing import text, sequence


path_input = sys.argv[1]
path_output = sys.argv[2]


os.system("echo -e TRAINING | tee -a /data/zmining/.anonymous/logs/train_daily.log")

train = pd.read_csv(path_input)

train['history_count'] = train.article_ids.apply(lambda x: len(x.split()))
print("before remove short sentence {}".format(len(train)))
train = train[train.history_count > 3]
train = train.drop(['history_count'], axis=1)
print("after remove short sentence {}".format(len(train)))

p1 = train[train.age != 39].reset_index(drop=True)
p2 = train[train.age == 39].reset_index(drop=True)
p2 = p2.sample(frac=1/4)
train = pd.concat([p1,p2], ignore_index=True)

X_train, X_valid = train_test_split(train, test_size = 0.01, random_state = 100)
#y_train, y_test = train_test_split(target, test_size = 0.01, random_state = 100)

X_train.reset_index(drop=True, inplace = True)
X_valid.reset_index(drop=True, inplace = True)

tokenizer_history = text.Tokenizer(filters='\t\n', lower=False)
tokenizer_history.fit_on_texts(list(X_train.article_ids.values))

x_train = tokenizer_history.texts_to_sequences(X_train.article_ids.values)
x_train = sequence.pad_sequences(x_train, maxlen=300, padding='post',truncating='post')
x_valid = tokenizer_history.texts_to_sequences(X_valid.article_ids.values)
x_valid = sequence.pad_sequences(x_valid, maxlen=300, padding='post',truncating='post')

with open('models/tokenizer_history.pickle', 'wb') as handle:
    pickle.dump(tokenizer_history, handle)


embedding_matrix = np.zeros((np.max(x_train) + 1, 300))

def mean_absolute_error(y_true, y_pred):
    return K.sqrt(K.mean(K.square(y_pred - y_true), axis=-1)) 
    
def build_model_age(input_layer):
    x = Embedding(embedding_matrix.shape[0], 300, trainable= True)(input_layer)
    #x = SpatialDropout1D(0.2)(x)
    #x = Conv1D(128, (5,), activation = 'relu', padding="same")(x)
    #y = Bidirectional(CuDNNLSTM(64, kernel_initializer=he_uniform(seed=0), return_sequences=True))(x)
    #y = Conv1D(256, (5,), activation = 'relu', padding="same")(x)
    y = Bidirectional(CuDNNLSTM(64, kernel_initializer=he_uniform(seed=0), return_sequences=True))(x)
    #x = BatchNormalization()(x)
    #x = SpatialDropout1D(0.2)(x)
    #x = Conv1D(128, (5,), activation = 'relu', padding="same")(x)
    #y = Bidirectional(CuDNNLSTM(64,kernel_initializer=he_uniform(seed=0), return_sequences=True))(x)
    #y = Bidirectional(CuDNNLSTM(64,kernel_initializer=he_uniform(seed=0), return_sequences=True))(y)
    #y = BatchNormalization()(y)
    a = GlobalAveragePooling1D()(y)
    b = GlobalMaxPooling1D()(y)
    #c = Bidirectional(CuDNNLSTM(128, kernel_initializer=he_uniform(seed=0), return_sequences=False))(x)
    #t = GlobalMaxPooling1D()(x)
    #d = Attention(30)(x)
    #e = Attention(30)(y)
    x = concatenate([a, b])
    #y = Flatten()(y)
    #x = Dropout(0.1)(x)
    x = Dense(32, activation='relu',kernel_initializer=he_uniform(seed=0))(x)
    x = Dense(1, activation=None,kernel_initializer=he_uniform(seed=0))(x)
    return x


def build_model_gender(input_layer):
    x = Embedding(embedding_matrix.shape[0], 300, trainable= True)(input_layer)
    #x = SpatialDropout1D(0.2)(x)
    #x = Conv1D(128, (5,), activation = 'relu', padding="same")(x)
    #y = Bidirectional(CuDNNLSTM(64, kernel_initializer=he_uniform(seed=0), return_sequences=True))(x)
    #y = Conv1D(256, (5,), activation = 'relu', padding="same")(x)
    y = Bidirectional(CuDNNLSTM(64, kernel_initializer=he_uniform(seed=0), return_sequences=True))(x)
    #x = BatchNormalization()(x)
    #x = SpatialDropout1D(0.2)(x)
    #x = Conv1D(128, (5,), activation = 'relu', padding="same")(x)
    #y = Bidirectional(CuDNNLSTM(64,kernel_initializer=he_uniform(seed=0), return_sequences=True))(x)
    #y = Bidirectional(CuDNNLSTM(64,kernel_initializer=he_uniform(seed=0), return_sequences=True))(y)
    #y = BatchNormalization()(y)
    a = GlobalAveragePooling1D()(y)
    b = GlobalMaxPooling1D()(y)
    #c = Bidirectional(CuDNNLSTM(128, kernel_initializer=he_uniform(seed=0), return_sequences=False))(x)
    #t = GlobalMaxPooling1D()(x)
    #d = Attention(30)(x)
    #e = Attention(30)(y)
    x = concatenate([a, b])
    #y = Flatten()(y)
    #x = Dropout(0.1)(x)
    x = Dense(32, activation='relu',kernel_initializer=he_uniform(seed=0))(x)
    x = Dense(1, activation='sigmoid',kernel_initializer=he_uniform(seed=0))(x)
    return x


input_layer = Input((300,))
output_layer_age = build_model_age(input_layer)
output_layer_gender = build_model_gender(input_layer)

model_gender = Model([input_layer], output_layer_gender)

model_gender.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

model_age = Model([input_layer], output_layer_age)

model_age.compile(loss='mse',
              optimizer='adam',
              metrics=['mse'])

model_age.fit(x_train, X_train.age.fillna(39).clip(10,60), batch_size=128, \
          verbose=1,shuffle=True,\
          epochs=1, validation_data=(x_valid,X_valid.age.clip(10,60)))
model_age.fit(x_train, X_train.age.fillna(39).clip(10,60), batch_size=256, \
          verbose=1,shuffle=True,\
          epochs=1, validation_data=(x_valid, X_valid.age.clip(10,60)))


model_json = model_age.to_json()
with open("models/model_age.json", "w") as json_file:
    json_file.write(model_json)
model_age.save_weights("models/model_age.h5")

model_gender.fit(x_train, X_train.gender - 1, batch_size=128, \
          verbose=1,shuffle=True,\
          epochs=1, validation_data=(x_valid,X_valid.gender-1))
model_gender.fit(x_train, X_train.gender - 1, batch_size=256, \
          verbose=1,shuffle=True,\
          epochs=1, validation_data=(x_valid, X_valid.gender-1))

model_json = model_gender.to_json()
with open("models/model_gender.json", "w") as json_file:
    json_file.write(model_json)
model_gender.save_weights("models/model_gender.h5")

os.mknod(path_output)
