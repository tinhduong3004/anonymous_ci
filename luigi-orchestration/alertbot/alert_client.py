from alertbot import ZUDM_AlertBotService
from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol


class AlertClient():
    def __init__(self, host="10.30.65.105", port="18791"):
        self.host = host
        self.port = port

    def _get_transport(self):
        transport = TSocket.TSocket(self.host, self.port)
        # Buffering is critical. Raw sockets are very slow
        transport = TTransport.TFramedTransport(transport)
        return transport

    def _get_client(self, transport):
        # Wrap in a protocol
        protocol = TBinaryProtocol.TBinaryProtocol(transport)
        # Create a client to use the protocol encoder
        client = ZUDM_AlertBotService.Client(protocol)
        return client

    def alert_to_group(self, group_name, message):
        """
        send message to all member in project
        :param group_name: text as define in java package com.vng.zing.alert_bot.common.ProjectManager
        :param message: text message
        :return: ZUDM_AlertResult object, error code = 0 => all good
        """
        transport = self._get_transport()
        client = self._get_client(transport)

        # Connect!
        transport.open()
        result = client.sendAllMemberInProject(group_name, message)
        # Close!
        transport.close()
        return result