### Description
This is python code for luigi module. There are 4 python package here:
* **alertbot**: a package was copied and moidifid from ThienT@vng.com.vn. This module is an util sending alert message to user through a OA Chatbot name `Rnd Alert OA`
* **anonymoususer**: a main package that was deploy on server `rnd@10.30.22.176`. This module has reponsibilies to extract raw log, process date, comunicate with the module `anonymoususer_gpu` and upload result to the database.
* **anonymoususer_gpu**: a package that was deployed on server `zdeploy@10.50.9.12`. This module contains the flow training data with gpu on the server and send back to the previous module.
* **luigi**: a package copied from luigi official github