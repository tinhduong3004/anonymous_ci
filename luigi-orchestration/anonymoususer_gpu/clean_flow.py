import luigi
import datetime as dt
from common import config
from glob import glob
import os


luigi.task.namespace(__name__)


class CleanTask(luigi.task.Task):
    date_hour = luigi.DateHourParameter(default=dt.datetime.now().replace(hour=config.TRAIN_HOUR), significant=False)
    input_path_pattern = "/data/zmining/.anonymous/data/inputs/*/%Y/%m/%d/data.csv"
    delay_days = 7

    def requires(self):
        return None

    def run(self):
        self.date_hour = self.date_hour - dt.timedelta(days=self.delay_days)
        self.input_path_pattern = self.date_hour.strftime(self.input_path_pattern)
        paths = glob(self.input_path_pattern)
        for path in paths:
            if path.startswith("/data/zmining/.anonymous/data/inputs/"):
                if os.path.exists(path):
                    os.remove(path)
                    print("removed {}".format(path))
        with self.output().open('w') as f:
            f.write('\n'.join(paths))

    def output(self):
        return luigi.LocalTarget(self.date_hour.strftime("/data/zmining/.anonymous/data/inputs/.markers/cleaned_%Y_%m_%d.txt"))
