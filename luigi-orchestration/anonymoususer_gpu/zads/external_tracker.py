import luigi
from anonymoususer_gpu.common.dir import local

luigi.task.namespace(__name__)


class ADTIMA_TRAIN_INPUT_TRACKER(luigi.ExternalTask):
    date = luigi.DateParameter(positional=False)
    templated_path = local.DAILY_INPUT_PATTERN + local.INPUT_FILE_NAME
    folder = 'adtima_activity_train'

    def output(self):
        path = self.date.strftime(self.templated_path.format(self.folder))
        print("DEBUG: ADTIMA_TRAIN_INPUT_TRACKER output: {}".format(path))
        return luigi.LocalTarget(path)


class ADTIMA_PREDICT_INPUT_TRACKER(luigi.ExternalTask):
    date = luigi.DateParameter(positional=False)
    templated_path = local.DAILY_INPUT_PATTERN + local.INPUT_FILE_NAME
    folder = 'adtima_activity_pred'

    def output(self):
        path = self.date.strftime(self.templated_path.format(self.folder))
        print("DEBUG: ADTIMA_PREDICT_INPUT_TRACKER output: {}".format(path))
        return luigi.LocalTarget(path)
