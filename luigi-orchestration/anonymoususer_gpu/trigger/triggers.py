import luigi
import datetime as dt
from anonymoususer_gpu.zads import daily_tasks as ads_daily
from anonymoususer_gpu.zep import daily_tasks as zep_daily
from anonymoususer_gpu.znews import daily_tasks as znews_daily
from anonymoususer_gpu.zmp3.mobile import daily_tasks as zmp3_mobile_daily

luigi.task.namespace(__name__)


class DailyFlow(luigi.task.WrapperTask):
    date = luigi.DateParameter(default=dt.datetime.now(), significant=False)

    def requires(self):
        for d in range(2):
            previous_day = self.date - dt.timedelta(days=d)
            print("DEBUG: DailyTrigger calling {}".format("RsyncTask {}".format(previous_day)))
            yield ads_daily.RsyncTask(date=previous_day)
            yield zep_daily.RsyncTask(date=previous_day)
            yield znews_daily.RsyncTask(date=previous_day)
            yield zmp3_mobile_daily.RsyncTask(date=previous_day)
