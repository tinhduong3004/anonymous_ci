import luigi
import os
from anonymoususer_gpu.common.dir import local
import abc
luigi.task.namespace(__name__)


class DailyTask(luigi.Task, metaclass=abc.ABCMeta):
    date = luigi.DateParameter()

    @property
    @abc.abstractmethod
    def folder(self):
        pass

    @property
    @abc.abstractmethod
    def next_task(self):
        pass

    @property
    @abc.abstractmethod
    def cmd(self):
        pass

    @property
    def dir(self):
        return self.date.strftime(local.INTERMEDIATE_TEMPLATE_DIR.format(self.folder))

    def requires(self):
        yield self.next_task(date=self.date)

    def run(self):
        os.system("mkdir -p {}".format(os.path.dirname(self.dir)))
        os.system(self.cmd())
        os.rename(self.output().path+"_TRAINING", self.output().path)

    def output(self):
        path = self.dir + local.MARKER_FILE
        return luigi.LocalTarget(path)
