import luigi
from ..dir import local
import abc
luigi.task.namespace(__name__)


class DailyTask(luigi.ExternalTask,metaclass=abc.ABCMeta):
    date = luigi.DateParameter(positional=False)
    template_path = local.INPUT_DATA_PATTERN + local.INPUT_DATA_FILE

    @property
    @abc.abstractmethod
    def folder(self):
        pass

    def output(self):
        path = self.date.strftime(self.template_path.format(self.folder))
        return luigi.LocalTarget(path)