CUDA_VISIBLE_DEVICES = "5, 6"

DATA_LOCAL_DIR = '/data/zmining/.anonymous/data/'
INPUT_DATA_PATTERN = DATA_LOCAL_DIR + 'inputs/{}/%Y/%m/%d/'
OUTPUT_DATA_PATTERN = DATA_LOCAL_DIR + 'outputs/{}/%Y/%m/%d/'
INPUT_DATA_FILE = 'data.csv'

MARKER_FILE = "TRAINING_DONE.marker_file"
INTERMEDIATE_DIR = DATA_LOCAL_DIR+'intermediate/'
INTERMEDIATE_TEMPLATE_DIR = INTERMEDIATE_DIR+'{}/%Y/%m/%d/'






PREDICT_RESULT_NAME = 'result.csv'
TEMPLATE_OUTPUT_DIR = DATA_LOCAL_DIR + 'outputs/%Y/%m/%d/'
OUTPUT_DIR = DATA_LOCAL_DIR + 'outputs/'
DAILY_RSYNC_INPUT_LOCAL = DATA_LOCAL_DIR + 'inputs/{}/%Y/%m/%d/'
RSYNC_MARKER = "LUIGI_SUCCESS.txt"
INPUT_PATTERN = DATA_LOCAL_DIR + 'inputs/{}/'

# DAILY_INTERMEDIATE_PATTERN = DATA_LOCAL_DIR + 'inputs/{}/%Y/%m/%d/'

ARTICLE_FILE_NAME = 'articles.csv'
PRETRAINED_FASTTEXT_CC_FILE_NAME = 'cc.vi.300.bin'
PRETRAINED_FASTTEXT_BAOMOI_FILE_NAME = 'fasttext.baomoi.bin'





SEQUENCE_THRESHOLD = 3

PATH_PREDICTED_AGE = '/data/zmining/.anonymous/data/inputs/predicted_age_labels/Age_2019/part-00000-b0f9741e-0b2e-4b1a-b6cb-72ddfbf39a74-c000.snappy.parquet'
