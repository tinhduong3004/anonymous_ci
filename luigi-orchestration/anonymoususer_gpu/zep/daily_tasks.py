import luigi
import os
import datetime as dt
import pandas as pd
import numpy as np
from anonymoususer_gpu.zep import external_tracker
from anonymoususer_gpu.common.dir import local

luigi.task.namespace(__name__)


class DailyPreprocessTask(luigi.Task):
    date = luigi.DateParameter()
    resources = {'train_model_limit': 1}

    def requires(self):
        yield external_tracker.ZEP_TRAIN_INPUT_TRACKER(date=self.date)
        yield external_tracker.ZEP_PREDICT_INPUT_TRACKER(date=self.date)
        yield external_tracker.ZEP_ARTICLE_INPUT_TRACKER(date=self.date)
        yield external_tracker.ZEP_FASTTEXT_INPUT_TRACKER()
        yield DailyPreprocessTask(date=self.date - dt.timedelta(days=1))

    def run(self):

        print("bash /data/zmining/.anonymous/scripts/preprocess_zep.sh {} {} {} {} {} > {}".format(
            self.input()[0].path, self.input()[1].path, self.input()[2].path, self.input()[3].path,
            self.output().path + "_PREPROCESS",
                                  os.path.dirname(self.output().path) + "/zep_preprocess.log"
        ))
        os.system("bash /data/zmining/.anonymous/scripts/preprocess_zep.sh {} {} {} {} {} > {}".format(
            self.input()[0].path, self.input()[1].path, self.input()[2].path, self.input()[3].path,
            self.output().path + "_PREPROCESS",
                                  os.path.dirname(self.output().path) + "/zep_preprocess.log"
        ))
        try:
            os.rename(self.output().path + "_PREPROCESS", self.output().path)
        except:
            raise Exception("daily preprocess failed!")

    def output(self):
        path = self.date.strftime(local.INTERMEDIATE_TEMPLATE_DIR + 'zep_intermediate.bundle')
        os.system('mkdir -p {}'.format(os.path.dirname(path)))
        print("DEBUG: DailyPreprocessTask output {}".format(path))
        return luigi.LocalTarget(path)


class DailyTrainTask(luigi.Task):
    date = luigi.DateParameter()
    max_len = 150

    def requires(self):
        yield external_tracker.ZEP_TRAIN_INPUT_TRACKER(date=self.date)
        yield DailyTrainTask(date=self.date - dt.timedelta(days=1))
        yield DailyPreprocessTask(date=self.date)

    def run(self):
            print("bash /data/zmining/.anonymous/scripts/train_zep.sh {} {} {} {} {} > {}".format(
                self.input()[0].path, self.output().path+"_TRAINING", self.date.strftime("zep_%Y_%m_%d_2_weeks"), 5,
                self.input()[2].path,
                os.path.dirname(self.output().path)+"/zep_train.log"
            ))
            os.system("bash /data/zmining/.anonymous/scripts/train_zep.sh {} {} {} {} {}> {}".format(
                self.input()[0].path, self.output().path+"_TRAINING", self.date.strftime("zep_%Y_%m_%d_2_weeks"), 5,
                self.input()[2].path,
                os.path.dirname(self.output().path)+"/zep_train.log"
            ))
            try:
                os.rename(self.output().path+"_TRAINING", self.output().path)
            except:
                raise Exception("daily train failed!")

    def output(self):
        path = self.date.strftime(local.INTERMEDIATE_TEMPLATE_DIR + 'ZEP_TRAIN_SUCCESS.txt')
        os.system("mkdir -p {}".format(os.path.dirname(path)))
        print("DEBUG: DailyTrainTask output {}".format(path))
        return luigi.LocalTarget(path)


class DailyPredictTask(luigi.Task):
    date = luigi.DateParameter()
    folder = 'zep'

    def requires(self):
        yield external_tracker.ZEP_PREDICT_INPUT_TRACKER(date=self.date)
        yield DailyPreprocessTask(date=self.date)
        yield DailyTrainTask(date=self.date)
        yield DailyPredictTask(date=self.date - dt.timedelta(days=1))

    def run(self):
        log_dir = self.date.strftime(local.INTERMEDIATE_TEMPLATE_DIR + 'zep_predict.log')
        os.system("mkdir -p {}".format(os.path.dirname(log_dir)))
        print("bash /data/zmining/.anonymous/scripts/predict_zep.sh {} {} {} {} {} > {}".format(
            self.input()[0].path, self.output().path+"_PREDICTING", self.date.strftime("zep_%Y_%m_%d_2_weeks"), 4,
            self.input()[1].path,
            log_dir
        ))
        os.system("bash /data/zmining/.anonymous/scripts/predict_zep.sh {} {} {} {} {}> {}".format(
            self.input()[0].path, self.output().path+"_PREDICTING", self.date.strftime("zep_%Y_%m_%d_2_weeks"), 4,
            self.input()[1].path,
            log_dir
        ))
        try:
            os.rename(self.output().path+"_PREDICTING", self.output().path)
        except:
            raise Exception("daily predict failed!")

    def output(self):
        path = self.date.strftime(local.DAILY_OUTPUT_PATTERN.format(self.folder) + 'results.csv')
        os.system("mkdir -p {}".format(os.path.dirname(path)))
        print("DEBUG: DailyPredictTask output {}".format(path))
        return luigi.LocalTarget(path)


class RefineTask(luigi.Task):
    date = luigi.DateParameter()
    folder = 'zep'

    def requires(self):
        yield DailyPredictTask(date=self.date)

    def run(self):
        df_predicted = pd.read_csv(self.input()[0].path)
        print('Before refine: {} '.format(len(df_predicted)))
        df_predicted = df_predicted[df_predicted.gender_preds > 0.006]
        df_predicted.gender_preds = df_predicted.gender_preds > .45
        df_predicted.gender_preds = df_predicted.gender_preds.astype(np.int16)
        df_predicted.to_csv(self.output().path, index=False)
        print('After refine: {}'.format(len(df_predicted)))

    def output(self):
        path = self.date.strftime(local.DAILY_OUTPUT_PATTERN.format(self.folder) + 'predicted_age_gender.csv')
        os.system("mkdir -p {}".format(os.path.dirname(path)))
        print("DEBUG: RefineTask output {}".format(path))
        return luigi.LocalTarget(path)


class RsyncTask(luigi.Task):
    date = luigi.DateParameter()
    resources = {'rsync_limit': 1}

    def requires(self):
        yield RefineTask(date=self.date)

    def run(self):
        cmd = "rsync -aurv /data/zmining/.anonymous/data/outputs 10.30.22.176::rnd/.anonymous/data/ --bwlimit=15000>{}"
        os.system(cmd.format(os.path.dirname(self.output().path)+"/rsync.log"))
        os.mknod(self.output().path)

    def output(self):
        path = self.date.strftime(local.INTERMEDIATE_TEMPLATE_DIR + 'RSYNC_ZEP_SUCCESS.txt')
        os.system("mkdir -p {}".format(os.path.dirname(path)))
        print("DEBUG: RsyncTask output {}".format(path))
        return luigi.LocalTarget(path)
