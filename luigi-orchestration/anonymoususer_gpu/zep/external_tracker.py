import luigi
from anonymoususer_gpu.common.dir import local

luigi.task.namespace(__name__)


class ZEP_TRAIN_INPUT_TRACKER(luigi.ExternalTask):
    date = luigi.DateParameter(positional=False)
    templated_path = local.DAILY_INPUT_PATTERN + local.INPUT_FILE_NAME
    folder = 'zep_activity_train'

    def output(self):
        path = self.date.strftime(self.templated_path.format(self.folder))
        print("DEBUG: ZEP_TRAIN_INPUT_TRACKER output: {}".format(path))
        return luigi.LocalTarget(path)


class ZEP_PREDICT_INPUT_TRACKER(luigi.ExternalTask):
    date = luigi.DateParameter(positional=False)
    templated_path = local.DAILY_INPUT_PATTERN + local.INPUT_FILE_NAME
    folder = 'zep_activity_pred'

    def output(self):
        path = self.date.strftime(self.templated_path.format(self.folder))
        print("DEBUG: ZEP_PREDICT_INPUT_TRACKER output: {}".format(path))
        return luigi.LocalTarget(path)


class ZEP_ARTICLE_INPUT_TRACKER(luigi.ExternalTask):
    date = luigi.DateParameter(positional=False)
    templated_path = local.DAILY_INPUT_PATTERN + local.INPUT_FILE_NAME
    folder = 'zep_article_content'

    def output(self):
        path = self.date.strftime(self.templated_path.format(self.folder))
        print("DEBUG: ZEP_ARTICLE_INPUT_TRACKER output: {}".format(path))
        return luigi.LocalTarget(path)


class ZEP_FASTTEXT_INPUT_TRACKER(luigi.ExternalTask):
    templated_path = local.INPUT_PATTERN + local.PRETRAINED_FASTTEXT_BAOMOI_FILE_NAME
    folder = 'model_fasttext_pretrained'

    def output(self):
        path = self.templated_path.format(self.folder)
        print("DEBUG: ZEP_FASTTEXT_INPUT_TRACKER output: {}".format(path))
        return luigi.LocalTarget(path)
