import luigi
import os
import abc
import glob
import datetime as dt
from anonymoususer_gpu.zep import predict
from anonymoususer_gpu.common.dir import local as config


class RsyncToLocalTask(luigi.Task, metaclass=abc.ABCMeta):
    date = luigi.DateParameter(positional=False, default=dt.datetime.now())
    templated_source = config.DAILY_RSYNC_INPUT_22_176
    templated_target = config.DAILY_RSYNC_INPUT_LOCAL

    def csv_sanity_check(self):
        file_list = glob.glob(self.date.strftime(self.templated_target.format(self.folder))+"/*.csv")
        print("File list: {}".format(file_list))
        file_list = [f for f in file_list if f[-4:] == '.csv']
        print("File list: {}".format(file_list))
        if len(file_list) == 0:
            return False
        return True

    def run(self):
        command = ' '.join(['mkdir -p {}'.format(os.path.dirname(self.output().path)),
                            "; rsync -aurv 10.30.22.176::rnd/{} {} --bwlimit=20000".format(
                                self.date.strftime(self.templated_source.format(self.folder)),
                                os.path.dirname(self.output().path)
                            )])
        print("Debug: Rsync command {} ".format(command))
        os.system(command)
        if self.csv_sanity_check():
            with self.output().open('w') as outfile:
                print("Done {} ".format(dt.datetime.now()), file=outfile)
                file_list = glob.glob(self.date.strftime(self.templated_target.format(self.folder))+"*.csv")
                file_list = [f for f in file_list if f[-3:] == 'csv']
                [print("File #{}: {} ".format(index, file), file=outfile) for index, file in enumerate(file_list)]
        else:
            raise Exception("File is not fully rsync!")

    @property
    @abc.abstractmethod
    def folder(self):
        pass

    def output(self):
        path = self.date.strftime(self.templated_target.format(self.folder))+"LUIGI_SUCCESS_RSYNC.txt"
        return luigi.LocalTarget(path)

    def requires(self):
        return None


class RsyncTo22176Task(luigi.Task, metaclass=abc.ABCMeta):
    date = luigi.DateParameter(positional=False)
    templated_source = config.OUTPUT_DIR
    templated_target = config.OUTPUT_22_176_DIR

    def run(self):
        command = "rsync -aurv {} 10.30.22.176::rnd/{} --bwlimit=20000".format(
            self.date.strftime(self.templated_source), self.date.strftime(self.templated_target)
        )
        print("Rsync command: {}".format(command))
        os.system(command)
        with self.output().open('w') as outfile:
            print("Done {} ".format(dt.datetime.now()), file=outfile)
        os.system(command)

    def output(self):
        path = self.date.strftime(config.TEMPLATE_OUTPUT_DIR) + "LUIGI_SUCCESS.txt"
        print("Rsync tracker path {} ".format(path))
        return luigi.LocalTarget(path)

    def requires(self):
        yield predict.DailyGenderPredictTask(date=self.date)
