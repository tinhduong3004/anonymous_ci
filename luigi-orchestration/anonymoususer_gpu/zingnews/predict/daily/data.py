import luigi
from anonymoususer_gpu.common.external import DailyTask
luigi.task.namespace(__name__)

BASE_FOLDER = 'znews_activity_pred'


class Data(DailyTask):
    date = luigi.DateParameter(positional=False)
    folder = BASE_FOLDER
