from anonymoususer_gpu.common.train import DailyTask
import luigi
from .data import Data

luigi.task.namespace(__name__)

class Train(DailyTask):

    folder = "zingnews"
    next_task = Data
    def cmd(self):
        return "bash /data/zmining/.anonymous/scripts/train_znews.sh {} {} {} {} > {}".format(
            self.input()[0].path,
            self.output().path + "_TRAINING",
            self.date.strftime("_%Y_%m_%d_2_weeks"),
            1,
            self.dir + "/training.log"
        )