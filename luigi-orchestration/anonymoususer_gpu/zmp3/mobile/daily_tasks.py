import luigi
import os
import datetime as dt
import pandas as pd
import numpy as np
from anonymoususer_gpu.zmp3.mobile import external_tracker
from anonymoususer_gpu.common.dir import local

luigi.task.namespace(__name__)


class DailyTrainTask(luigi.Task):
    date = luigi.DateParameter()

    def requires(self):
        yield external_tracker.ZMP3_MOBILE_TRAIN_INPUT_TRACKER(date=self.date)
        yield DailyTrainTask(date=self.date - dt.timedelta(days=1))

    def run(self):
            print("bash /data/zmining/.anonymous/scripts/train_zmp3_mobile.sh {} {} {} {} > {}".format(
                self.input()[0].path, self.output().path+"_TRAINING", self.date.strftime("_%Y_%m_%d_2_weeks"), 2,
                os.path.dirname(self.output().path)+"/train_zmp3_mobile.log"
            ))
            os.system("bash /data/zmining/.anonymous/scripts/train_zmp3_mobile.sh {} {} {} {} > {}".format(
                self.input()[0].path, self.output().path+"_TRAINING", self.date.strftime("_%Y_%m_%d_2_weeks"), 2,
                os.path.dirname(self.output().path)+"/train_zmp3_mobile.log"
            ))
            try:
                os.rename(self.output().path+"_TRAINING", self.output().path)
            except:
                raise Exception("daily train failed!")

    def output(self):
        path = self.date.strftime(local.INTERMEDIATE_TEMPLATE_DIR + 'ZMP3_MOBILE_TRAIN_SUCCESS.txt')
        os.system("mkdir -p {}".format(os.path.dirname(path)))
        print("DEBUG: DailyTrainTask output {}".format(path))
        return luigi.LocalTarget(path)


class DailyPredictTask(luigi.Task):
    date = luigi.DateParameter()
    folder = 'zmp3_mobile'

    def requires(self):
        yield external_tracker.ZMP3_MOBILE_PREDICT_INPUT_TRACKER(date=self.date)
        yield DailyTrainTask(date=self.date)
        yield DailyPredictTask(date=self.date - dt.timedelta(days=1))
        yield external_tracker.ZMP3_MOBILE_TRAIN_INPUT_TRACKER(date=self.date)

    def run(self):
        log_dir = self.date.strftime(local.INTERMEDIATE_TEMPLATE_DIR + 'predict_zmp3_mobile.log')
        os.system("mkdir -p {}".format(os.path.dirname(log_dir)))
        print("bash /data/zmining/.anonymous/scripts/predict_zmp3_mobile.sh {} {} {} {} {} > {}".format(
            self.input()[0].path, self.output().path+"_PREDICTING", self.date.strftime("_%Y_%m_%d_2_weeks"), 3,
            self.input()[3].path,
            log_dir
        ))
        os.system("bash /data/zmining/.anonymous/scripts/predict_zmp3_mobile.sh {} {} {} {} {} > {}".format(
            self.input()[0].path, self.output().path+"_PREDICTING", self.date.strftime("_%Y_%m_%d_2_weeks"), 3,
            self.input()[3].path,
            log_dir
        ))
        try:
            os.rename(self.output().path+"_PREDICTING", self.output().path)
        except:
            raise Exception("daily predict failed!")

    def output(self):
        path = self.date.strftime(local.DAILY_OUTPUT_PATTERN.format(self.folder) + 'results.csv')
        os.system("mkdir -p {}".format(os.path.dirname(path)))
        print("DEBUG: DailyPredictTask output {}".format(path))
        return luigi.LocalTarget(path)


class RefineTask(luigi.Task):
    date = luigi.DateParameter()
    folder = 'zmp3_mobile'

    def requires(self):
        yield DailyPredictTask(date=self.date)

    def run(self):
        df_predicted = pd.read_csv(self.input()[0].path)
        print('Before refine: {} '.format(len(df_predicted)))
        df_predicted = df_predicted[df_predicted.gender_preds > 0.006]
        df_predicted.gender_preds = df_predicted.gender_preds > .45
        df_predicted.gender_preds = df_predicted.gender_preds.astype(np.int16)
        df_predicted.to_csv(self.output().path, index=False)
        print('After refine: {}'.format(len(df_predicted)))

    def output(self):
        path = self.date.strftime(local.DAILY_OUTPUT_PATTERN.format(self.folder) + 'predicted_age_gender.csv')
        os.system("mkdir -p {}".format(os.path.dirname(path)))
        print("DEBUG: RefineTask output {}".format(path))
        return luigi.LocalTarget(path)


class RsyncTask(luigi.Task):
    date = luigi.DateParameter()

    def requires(self):
        yield RefineTask(date=self.date)

    def run(self):
        cmd = "rsync -aurv /data/zmining/.anonymous/data/outputs 10.30.22.176::rnd/.anonymous/data/ --bwlimit=15000>{}"
        os.system(cmd.format(os.path.dirname(self.output().path)+"/zmp3_mobile_rsync.log"))
        os.mknod(self.output().path)

    def output(self):
        path = self.date.strftime(local.INTERMEDIATE_TEMPLATE_DIR + 'ZMP3_MOBILE_RSYNC_SUCCESS.txt')
        os.system("mkdir -p {}".format(os.path.dirname(path)))
        print("DEBUG: RsyncTask output {}".format(path))
        return luigi.LocalTarget(path)
