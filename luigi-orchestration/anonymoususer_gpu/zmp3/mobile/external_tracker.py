import luigi
from anonymoususer_gpu.common.dir import local

luigi.task.namespace(__name__)


class ZMP3_MOBILE_TRAIN_INPUT_TRACKER(luigi.ExternalTask):
    date = luigi.DateParameter(positional=False)
    templated_path = local.DAILY_INPUT_PATTERN + local.INPUT_FILE_NAME
    folder = 'zmp3_mobile_activity_train'

    def output(self):
        path = self.date.strftime(self.templated_path.format(self.folder))
        print("DEBUG: ZMP3_MOBILE_TRAIN_INPUT_TRACKER output: {}".format(path))
        return luigi.LocalTarget(path)


class ZMP3_MOBILE_PREDICT_INPUT_TRACKER(luigi.ExternalTask):
    date = luigi.DateParameter(positional=False)
    templated_path = local.DAILY_INPUT_PATTERN + local.INPUT_FILE_NAME
    folder = 'zmp3_mobile_activity_pred'

    def output(self):
        path = self.date.strftime(self.templated_path.format(self.folder))
        print("DEBUG: ZMP3_MOBILE_PREDICT_INPUT_TRACKER output: {}".format(path))
        return luigi.LocalTarget(path)
