from common import external
from common.dir.hdfs import FALL_OVER_FOLDER
import luigi
luigi.task.namespace(__name__)


class RawLog(external.HourlyTask):
    folder = 'ZINGTV_LOG_V2'
    _fall_over_path = FALL_OVER_FOLDER + "zingtv_log_v2_empty"