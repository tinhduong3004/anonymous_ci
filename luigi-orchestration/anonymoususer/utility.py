import luigi

def evoke_task(task, next_task):
    new_family = task.get_task_namespace() + "." + next_task
    return luigi.task_register.Register.get_task_cls(new_family)