import luigi
from common.target import HDFSTarget
import datetime as dt
luigi.task.namespace(__name__)


class Ip2Location(luigi.ExternalTask):
    date = luigi.DateParameter(positional=False)
    template_path = '/data/jobs/rnd/intermediate_data_parquet/d01/{}/%Y/%m/%d/'
    folder = 'IP2Location'

    def output(self):
        path = self.date.strftime(self.template_path.format(self.folder))
        today_target = HDFSTarget(path)
        if today_target.exists():
            return today_target
        else:
            yesterday = self.date - dt.timedelta(days=1)
            yesterday_path = yesterday.strftime(self.template_path.format(self.folder))
            return HDFSTarget(yesterday_path)
