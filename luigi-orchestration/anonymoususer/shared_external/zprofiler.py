import luigi
from common.target import HDFSTarget
luigi.task.namespace(__name__)


class ZProfiler(luigi.ExternalTask):
    # path = "/data/jobs/rnd/intermediate_data/Zprofile/2019/07/24"
    path = "/data/jobs/rnd/intermediate_data/global_apps_hourly/global_id_identified_demography/2019/11/11/*"

    def output(self):
        return HDFSTarget(self.path)