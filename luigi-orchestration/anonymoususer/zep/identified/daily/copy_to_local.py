from common.transfer import HDFSToLocal
from .d14_union import D14Union
import luigi
from .config import BASE_FOLDER
luigi.task.namespace(__name__)


class CopyToLocal(HDFSToLocal):
    folder = BASE_FOLDER
    next_task = D14Union

