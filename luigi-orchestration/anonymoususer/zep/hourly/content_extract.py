from .. import RawLog
from common import extract
import luigi
luigi.task.namespace(__name__)


class ContentExtract(extract.HourlyTask):
    entry_class = "com.vng.zalo.zte.rnd.anonymoususer.zep.hourly.ContentExtract"
    template_path = '/data/jobs/rnd/articleinterest/hourly/{}/%Y/%m/%d/%H/'
    folder = "zep_urls"

    def requires(self):
        yield RawLog(date_hour=self.date_hour)