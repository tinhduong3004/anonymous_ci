from common.union import DailyD14Task
from ..hourly import RawExtract
import luigi
from .config import BASE_FOLDER
luigi.task.namespace(__name__)


class D14Union(DailyD14Task):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.zep.anonymous.daily.Union'
    folder = BASE_FOLDER
    next_task = RawExtract
