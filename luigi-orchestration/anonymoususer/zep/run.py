from .anonymous import hourly as anonymous_hourly
from .identified import hourly as identified_hourly
from .anonymous import daily as anonymous_daily
from .identified import daily as identified_daily
from . import daily
from .hourly import ContentExtract
import datetime as dt
import luigi
luigi.task.namespace(__name__)


class HourlyExtract(luigi.task.WrapperTask):
    date_hour = luigi.DateHourParameter(default=dt.datetime.now(), significant=False)
    
    def requires(self):
        self_date = self.date_hour.date() - dt.timedelta(days=1)
        for i in range(24):
            # previous_hour = self.date_hour - dt.timedelta(hours=i)
            previous_hour = dt.datetime.combine(self_date, dt.time(hour=i))
            yield ContentExtract(date_hour=previous_hour)
            yield anonymous_hourly.RawExtract(date_hour=previous_hour)
            yield identified_hourly.RawExtract(date_hour=previous_hour)


class DailyCopyToLocal(luigi.task.WrapperTask):
    date = luigi.DateParameter(default=dt.datetime.now(), significant=False)

    def requires(self):
        yesterday = self.date - dt.timedelta(days=1)
        yield anonymous_daily.Reduce(date=yesterday)
        yield anonymous_daily.Reduce(date=self.date)
        yield identified_daily.Reduce(date=yesterday)
        yield identified_daily.Reduce(date=self.date)
        yield daily.ContentReduce(date=self.date)
        yield daily.ContentReduce(date=yesterday)


class DailyPutToHDFS(luigi.task.WrapperTask):
    date = luigi.DateParameter(default=dt.datetime.now(), significant=False)

    def requires(self):
        yesterday = self.date - dt.timedelta(days=1)
        yield daily.PutToHDFS(date=yesterday)
        yield daily.PutToHDFS(date=self.date)