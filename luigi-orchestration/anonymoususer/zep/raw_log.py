from common import external
from common.dir.hdfs import FALL_OVER_FOLDER
import luigi
luigi.task.namespace(__name__)


class RawLog(external.HourlyTask):
    folder = 'BAOMOI'
    _fall_over_path = FALL_OVER_FOLDER + "zep_baomoi_tracking_empty"
