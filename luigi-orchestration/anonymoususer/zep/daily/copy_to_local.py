from common.transfer import HDFSToLocal
from .d14_content_union import D14ContentUnion
import luigi
luigi.task.namespace(__name__)


class CopyToLocal(HDFSToLocal):
    folder = "zep_article_content"
    next_task = D14ContentUnion
