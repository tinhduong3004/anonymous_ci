from common.reduce import DailyTask
from .copy_to_local import CopyToLocal
import luigi
import glob
import os
luigi.task.namespace(__name__)


class ContentReduce(DailyTask):
    next_task = CopyToLocal

    def run(self):
        filenames = glob.glob(os.path.dirname(self.input()[0].path) + '/*.parquet')
        assert len(filenames) == 1
        filenames = filenames[0]
        try:
            os.rename(filenames, self.output().path)
        except:
            raise Exception

    def output(self):
        return luigi.LocalTarget(os.path.dirname(self.input()[0].path)+'/'+"data.csv")
