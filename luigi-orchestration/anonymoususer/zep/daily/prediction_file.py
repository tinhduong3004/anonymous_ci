import luigi
from common.external import DailyLocalTask
luigi.task.namespace(__name__)


class PredictionFile(DailyLocalTask):
    folder = 'outputs/zep'
