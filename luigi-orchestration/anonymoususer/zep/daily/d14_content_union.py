from common.union import DailyD14Task
from ..hourly import ContentExtract
import luigi
luigi.task.namespace(__name__)


class D14ContentUnion(DailyD14Task):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.zep.daily.ContentUnion'
    folder = "zep_article_content"
    next_task = ContentExtract


