from ... import FOLDER
from ... import RawLog
from ... import RawLogV2
from common import extract
from all.identified import hourly as identified
import luigi
luigi.task.namespace(__name__)


class RawExtract(extract.HourlyTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.zingnews.identified.hourly.RawExtract'
    folder = FOLDER+"_identified"

    def requires(self):
        yield identified.DemographicJoin(date_hour=self.date_hour)
        yield RawLog(date_hour=self.date_hour)

class RawExtractV2(extract.HourlyTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.zingnews.identified.hourly.RawExtractV2'
    folder = FOLDER+"_identified_v2"

    def requires(self):
        yield identified.DemographicJoin(date_hour=self.date_hour)
        yield RawLogV2(date_hour=self.date_hour)