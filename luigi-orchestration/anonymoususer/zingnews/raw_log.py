from common import external
import luigi
luigi.task.namespace(__name__)


class RawLog(external.HourlyTask):
    folder = 'ZNEWS-COUNTER'
    _fall_over_path = ""

    def output(self):
        template_path = ('/zlogcentral_live/scribe/'
                          '{}/'
                          '%Y/%m/%d/%H/')
        path = self.date_hour.strftime(template_path.format(self.folder))
        return luigi.contrib.hdfs.HdfsTarget(path)

class RawLogV2(external.HourlyTask):
    folder = 'ZNEWS-COUNTER-V2'
    _fall_over_path = ""

    def output(self):
        template_path = ('/zlogcentral_live/scribe/'
                          '{}/'
                          '%Y/%m/%d/%H/')
        path = self.date_hour.strftime(template_path.format(self.folder))
        return luigi.contrib.hdfs.HdfsTarget(path)