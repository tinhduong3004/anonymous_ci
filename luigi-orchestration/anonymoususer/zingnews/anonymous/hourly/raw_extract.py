from ... import FOLDER
from ... import RawLog
from ... import RawLogV2
from common import extract
from all.anonymous import hourly as anonymous
import luigi
luigi.task.namespace(__name__)


class RawExtract(extract.HourlyTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.zingnews.anonymous.hourly.RawExtract'
    folder = FOLDER+"_anonymous"

    def requires(self):
        yield anonymous.RawExtract(date_hour=self.date_hour)
        yield RawLog(date_hour=self.date_hour)


class RawExtractV2(extract.HourlyTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.zingnews.anonymous.hourly.RawExtractV2'
    folder = FOLDER+"_anonymous_v2"

    def requires(self):
        yield anonymous.RawExtract(date_hour=self.date_hour)
        yield RawLogV2(date_hour=self.date_hour)