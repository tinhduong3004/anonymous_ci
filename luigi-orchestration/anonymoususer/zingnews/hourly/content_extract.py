from .. import RawLog
from .. import FOLDER
from common import extract
import luigi
luigi.task.namespace(__name__)


class ContentExtract(extract.HourlyTask):
    entry_class = "com.vng.zalo.zte.rnd.anonymoususer.zingnews.hourly.ContentExtract"
    folder = FOLDER+"_content"

    def requires(self):
        yield RawLog(date_hour=self.date_hour)