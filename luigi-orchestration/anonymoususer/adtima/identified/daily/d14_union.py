from common.union import DailyD14Task
from ..hourly import RawExtract
from .config import BASE_FOLDER
import luigi
luigi.task.namespace(__name__)


# This code is a copy version, It need to be refactored
class D14Union(DailyD14Task):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.adtima.identified.daily.Union'
    folder = BASE_FOLDER
    next_task = RawExtract


