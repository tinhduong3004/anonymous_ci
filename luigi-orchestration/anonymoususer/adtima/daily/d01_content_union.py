from common.union import DailyD01Task
from .. import FOLDER
from common.dir import hdfs
from ..hourly import ContentExtract
import luigi
luigi.task.namespace(__name__)


class D01ContentUnion(DailyD01Task):
    template_path = hdfs.ANONYMOUS_HOME + 'daily/{}/%Y/%m/%d/'
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.adtima.daily.ContentUnion'
    folder = FOLDER+"_content"
    next_task = ContentExtract
