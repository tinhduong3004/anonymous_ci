import luigi
from common.transfer import LocalToHDFS
from .prediction_file import PredictionFile
luigi.task.namespace(__name__)


class PutToHDFS(LocalToHDFS):
    folder = 'adtima_predicted_age_gender'

    def requires(self):
        yield PredictionFile(date=self.date)