from ... import FOLDER
from ... import RawLog
from ...hourly import ContentExtract
from common import extract
from all.anonymous import hourly as anonymous
import luigi

luigi.task.namespace(__name__)


class RawExtract(extract.HourlyTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.adtima.anonymous.hourly.RawExtract'
    folder = FOLDER+"_anonymous"

    def requires(self):
        yield anonymous.RawExtract(date_hour=self.date_hour)
        yield ContentExtract(date_hour=self.date_hour)
        yield RawLog(date_hour=self.date_hour)