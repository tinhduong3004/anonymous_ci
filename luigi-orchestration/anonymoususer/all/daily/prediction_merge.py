from adtima.daily.put_to_hdfs import PutToHDFS as AdtimaPrediction
from zep.daily.put_to_hdfs import PutToHDFS as ZepPrediction
from zingnews.daily.put_to_hdfs import PutToHDFS as ZingNewsPrediction
from zingmp3.mobile.daily.put_to_hdfs import PutToHDFS as MobileZingMp3Prediction
from .external_sample import ExternalSample
from common.union.daily_task import DailyTask
from common.dir import hdfs
import luigi
luigi.task.namespace(__name__)


class PredictionMerge(DailyTask):
    priority = 5
    next_task = None
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.transforms.merge.DailyResultMerge'
    folder = 'predicted_age_gender'
    template_path = hdfs.ANONYMOUS_HOME + 'daily/{}/%Y/%m/%d/'

    def requires(self):
        yield ExternalSample(date=self.date, platform='web')
        yield ExternalSample(date=self.date, platform='wap')
        yield ZepPrediction(date=self.date)
        yield AdtimaPrediction(date=self.date)
        yield ZingNewsPrediction(date=self.date)
        yield MobileZingMp3Prediction(date=self.date)