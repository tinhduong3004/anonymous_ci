import luigi
import datetime as dt
from glob import glob
import os
luigi.task.namespace(__name__)


class CleanUp(luigi.task.Task):
    date = luigi.DateParameter(default=dt.datetime.now(), significant=False)
    template_path = "/data/rnd/.anonymous/data/inputs/*/%Y/%m/%d/data.csv"
    delay_days = 7

    def requires(self):
        return None

    def run(self):
        prev_date = self.date - dt.timedelta(days=self.delay_days)
        star_path = prev_date.strftime(self.template_path)
        paths = glob(star_path)
        for path in paths:
            if path.startswith("/data/rnd/.anonymous/data/inputs/"):
                if os.path.exists(path):
                    os.remove(path)
                    print("removed {}".format(path))
        with self.output().open('w') as f:
            f.write('\n'.join(paths))

    def output(self):
        return luigi.LocalTarget(self.date.strftime("/data/rnd/.anonymous/data/inputs/.markers/cleaned_%Y_%m_%d.txt"))
