import luigi
from common.dir import hdfs
from common.target import HDFSTarget
luigi.task.namespace(__name__)


class ExternalSample(luigi.ExternalTask):
    date = luigi.DateParameter(positional=False)
    platform = luigi.Parameter(positional=False)
    template_path = hdfs.SAMPLE_DIR + '{platform}_demographic/%Y/%m/{day:02d}/23/{platform}_anon_results.tsv'

    def last_day(self):
        if int(self.date.day) < 16:
            return 1
        else:
            return 16

    def output(self):
        path = self.template_path.format(platform = self.platform, day=self.last_day())
        path_with_date = self.date.strftime(path)
        return HDFSTarget(path_with_date)
