import all.anonymous.hourly as anonymous
import all.identified.hourly as identified
from adtima.anonymous.daily import D14Union as AdtimaHistoryMerge
from zingmp3.mobile.anonymous.daily import D14Union as MobileZingMp3HistoryMerge
from zingnews.anonymous.daily import D14Union as ZingNewsHistoryMerge
from location.travelpath.daily import IP2LocationJoin as TravelPathHistorymerge
from .daily import PredictionMerge
from .daily import CleanUp
import datetime as dt
import luigi
luigi.task.namespace(__name__)


class HourlyExtract(luigi.task.WrapperTask):
    date_hour = luigi.DateHourParameter(default=dt.datetime.now(), significant=False)

    def requires(self):
        for i in range(24):
            previous_hour = self.date_hour - dt.timedelta(hours=i)
            yield anonymous.RawExtract(date_hour=previous_hour)
            yield anonymous.TimelineExtract(date_hour=previous_hour)
            yield identified.RawExtract(date_hour=previous_hour)
            yield identified.DemographicJoin(date_hour=previous_hour)


class DailyHistoryMerge(luigi.task.WrapperTask):
    date = luigi.DateParameter(default=dt.date.today(), significant=False)

    def requires(self):
        for d in range(2):
            _date = self.date - dt.timedelta(days=d)
            yield AdtimaHistoryMerge(date=_date)
            yield ZingNewsHistoryMerge(date=_date)
            yield MobileZingMp3HistoryMerge(date=_date)
            yield TravelPathHistorymerge(date=_date)


class DailyPredictionMerge(luigi.task.WrapperTask):
    date = luigi.DateParameter(default=dt.datetime.now(), significant=False)

    def requires(self):
        for d in range(2):
            _date = self.date - dt.timedelta(days=d)
            yield PredictionMerge(date=_date)


class DailyAutoCleanUp(luigi.task.WrapperTask):
    date = luigi.DateParameter(default=dt.datetime.now(), significant=False)

    def requires(self):
        yield CleanUp(date=self.date)