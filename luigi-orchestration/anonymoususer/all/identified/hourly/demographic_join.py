from ... import FOLDER
from common import extract
from shared_external import ZProfiler
from .raw_extract import RawExtract
import datetime as dt
import luigi

luigi.task.namespace(__name__)


class DemographicJoin(extract.HourlyTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.all.identified.hourly.DemographicJoin'
    folder = FOLDER+"_identified_demography"
    executor_memory = '8G'

    def requires(self):
        yield RawExtract(date_hour=self.date_hour)
        self_date = self.date_hour.date() - dt.timedelta(days=1)
        for hour in range(24):
            # prev_hour = self.date_hour - dt.timedelta(hours=hour)
            # yield DemographicJoin(date_hour=prev_hour)
            chosen_hour = dt.datetime.combine(self_date, dt.time(hour=hour))
            yield DemographicJoin(date_hour=chosen_hour)
        
        
