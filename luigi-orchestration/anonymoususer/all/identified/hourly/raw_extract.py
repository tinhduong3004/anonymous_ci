from device import RawLog as DeviceRawLog
from adtima import RawLog as AdtimaRawLog
from zing_tv.mobile import RawLog as ZingTVMobileRawLog
from zing_tv.web import RawLog as ZingTVWebRawLog
from zingmp3.mobile import RawLog as ZingMp3MobileRawLog
from zingmp3.web import RawLog as ZingMp3WebRawLog
from zingnews import RawLog as ZingNewsRawLog
from zep import RawLog as BaoMoiRawLog
from webvisitor import RawLog as WebVisitorRawLog
from ... import FOLDER
from common import extract
import luigi

luigi.task.namespace(__name__)


class RawExtract(extract.HourlyTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.all.identified.hourly.RawExtract'
    folder = FOLDER+"_identified"
    
    def requires(self):
        yield WebVisitorRawLog(date_hour=self.date_hour)
        yield BaoMoiRawLog(date_hour=self.date_hour)
        yield ZingNewsRawLog(date_hour=self.date_hour)
        yield ZingMp3MobileRawLog(date_hour=self.date_hour)
        yield ZingMp3WebRawLog(date_hour=self.date_hour)
        yield ZingTVMobileRawLog(date_hour=self.date_hour)
        yield ZingTVWebRawLog(date_hour=self.date_hour)
        yield AdtimaRawLog(date_hour=self.date_hour)
        yield DeviceRawLog(date_hour=self.date_hour)