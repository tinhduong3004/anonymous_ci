import luigi
import datetime as dt
from .daily import Upload
from .daily import FilterNewUser
luigi.task.namespace(__name__)


class DailyUpload(luigi.task.WrapperTask):
    date = luigi.DateParameter(default=dt.datetime.now(), significant=False)

    def requires(self):
        yield Upload(date=self.date)
        yesterday = self.date - dt.timedelta(days=1)
        yield Upload(date=yesterday)

class DailyMerge(luigi.task.WrapperTask):
    date = luigi.DateParameter(default=dt.datetime.now(), significant=False)

    def requires(self):
        yield FilterNewUser(date=self.date)
        yesterday = self.date - dt.timedelta(days=1)
        yield FilterNewUser(date=yesterday)