"""
This is one of those "bad" modules that hopefully goes away one day. It
basically contains code that should be somewhere else at the moment.
"""
import abc
import datetime as dt
from enum import Enum
import functools
import logging
import luigi
import luigi.contrib.hdfs
import luigi.contrib.spark
from all.anonymous.hourly import RawExtract
import luigi.task
import luigi.task_register
from plumbum import local
import random

################### DEFINITION #################################

CLUSTER = "hdfs://namenode162:9000"

#################################################################

logger = logging.getLogger(__name__)
luigi.task.namespace(__name__)

def parseInputlist(inputlist, rules, MAIN_CLUSTER):
    output = []
    for rule in rules:
        filelist = []
        for path in inputlist:
            if rule[1] in path:
                if (len(rule) != 3 or rule[2] not in path):
                    filelist = filelist + [path]

        if len(filelist) != 0:
            output = output + [rule[0], ','.join([MAIN_CLUSTER + path for path in filelist])]
    return output

def _hadoopcli_client():
    """
    After a Hadoop havoc in December 2016 at VNG, for some reason we
    couldn't do writes using the http hadoop client. So we replace
    touchz-calls with this hellper function.
    """
    return luigi.contrib.hdfs.HdfsClient()


def _next_task(task, cls_name):
    new_family = task.get_task_namespace() + "." + cls_name
    return luigi.task_register.Register.get_task_cls(new_family)

def _next_task_withnamespace(namespace, cls_name):
    new_family = namespace + "." + cls_name
    return luigi.task_register.Register.get_task_cls(new_family)


def _params(task):
    return {k: getattr(task, k) for (k, _) in task.get_params()}


class Duration(Enum):
    non = -1
    d00 = 0
    d01 = 1
    d07 = 2
    d30 = 3
    d90 = 4
    inf = 5

    def calc_priority(self):
        # I call this calc_priority as it's not a @property
        return 6 - self.value

    def dependencies(self, date, epoch):
        """
        Return tuples like  either (range, date) or ('basecase', date_hour)
        """
        if self is Duration.d01:
            for i in range(24):
                new_date_hour = dt.datetime.combine(date, dt.time(hour=i))
                yield 'basecase', new_date_hour
        elif self is Duration.d07:
            for i in range(7):
                new_date = date - dt.timedelta(days=i)
                yield Duration.d01, new_date
        elif self is Duration.d30:
            for i in range(4):
                new_date = date - dt.timedelta(weeks=i)
                yield Duration.d07, new_date
            for td in (dt.timedelta(days=28), dt.timedelta(days=29)):
                yield Duration.d01, date - td
        elif self is Duration.d90:
            for i in range(3):
                new_date = date - dt.timedelta(days=i * 30)
                yield Duration.d30, new_date
        elif self is Duration.inf:
            if date < epoch:
                raise ValueError('Date before epoch requested')
            elif date == epoch:
                yield Duration.d90, date
            else:
                yesterday = date - dt.timedelta(days=1)
                yield Duration.inf, yesterday
                yield Duration.d01, date


class DDParamsMixin(object):
    date = luigi.DateParameter(positional=False)
    duration = luigi.EnumParameter(enum=Duration)

    def dd_format(self, templated_string, *args, **kwargs):
        return self.date.strftime(
            templated_string.format(*args,
                                    duration_name=self.duration.name,
                                    **kwargs))


class VngHdfsTarget(luigi.contrib.hdfs.HdfsTarget):
    def temporary_path(target):
        class Manager(object):
            def __enter__(self):
                path = target.path.rstrip('/')
                num = random.randrange(0, 1e10)
                self.temp_path = path + '-luigi-tmp-{:010}'.format(num)
                return self.temp_path

            def __exit__(self, exc_type, exc_value, traceback):
                if exc_type is None:
                    # There were no exceptions
                    target.fs.rename_dont_move(self.temp_path, target.path)
                return False

        return Manager()

class HadoopMasterTarget(VngHdfsTarget):
    _zuser = "zte"

    def __init__(self, path=None, zuser="zreader"):
        super(HadoopMasterTarget, self).__init__(path)
        self._zuser = zuser

class ZudmSparkSubmitTask(luigi.contrib.spark.SparkSubmitTask):
    zapp_name = None
    zapp_prof = None
    zconf_dir = None
    zconf_files = 'config.ini'
    zjzcommonx_version = 'LATEST'
    zicachex_version = 'LATEST'
    zuser = 'zte'

    def _flag_str(self, val):
        if val:
            return [val]
        return ''

    def _text_str(self, key, val):
        if val:
            return [key + '=' + val]
        return ''

    def _get_configs(self, configs):
        configs = ' '.join(configs)
        return configs

    def vm_options(self):
        configs = []
        # configs += self._text_str('-Dzappname', self.zapp_name)
        # configs += self._text_str('-Dzappprof', self.zapp_prof)
        # configs += self._text_str('-Dzconfdir', self.zconf_dir)
        # configs += self._text_str('-Dzconffiles', self.zconf_files)
        # configs += self._text_str('-Djzcommonx.version', self.zjzcommonx_version)
        # configs += self._text_str('-Dzicachex.version', self.zicachex_version)
        return self._get_configs(configs)

    @property
    def driver_java_options(self):
        return self.vm_options()

    @functools.lru_cache()
    def _inputs(self):
        return list(set([input.path for input in self.input() if input and input.exists()]))

    def run(self):
        with self.output().temporary_path() as self.temp_output_path:
            # super().run()
            # inputs = ','.join(self._inputs())
            # print("Inputs: " + inputs)

            if self._inputs():  # If there is input, use spark
                super().run()
            else:  # Otherwise, just write empty input
                print("GENERATED_WITH_NO_DATA")
            #     fs = self.output().fs
            #     fs.mkdir(self.temp_output_path)
            #     _hadoopcli_client().touchz(os.path.join(self.temp_output_path, "GENERATED_WITH_NO_DATA"))

    def app_options(self):
        inputs = ','.join(self._inputs())
        return [self.zclass_name, inputs, self.temp_output_path]

    def output(self):
        path = self.dd_format(templated_string=self.format_path, folder=self.folder)
        return HadoopMasterTarget(path, self.zuser)




class ExternalLogsTask(luigi.ExternalTask, DDParamsMixin, metaclass=abc.ABCMeta):
    date = luigi.DateHourParameter(positional=False)
    duration = Duration.non
    zuser = 'zte'

    @property
    @abc.abstractmethod
    def folder(self):
        pass

    @property
    @abc.abstractmethod
    def format_path(self):
        pass

    @property
    @abc.abstractmethod
    def sometime(self):
        pass

    def output(self):
        path = self.dd_format(templated_string=self.format_path, folder=self.folder)
        return HadoopMasterTarget(path, self.zuser)

    def complete(self):
        if (self.date + self.sometime) < dt.datetime.today():
            return self.output().exists()
        return False



MAIN_CLUSTER = "hdfs://namenode162:9000"
OUT_DIR = '/data/jobs/rnd'
# OUT_DIR = '/data/zte/rd'

APP_JAR = '/zserver/rnd/apps/location_mining/spark-transforms/spark-transforms.jar'
ENTRY_CLASS = 'com.vng.zing.zudm_locationmining.app.MainApp'

ZAPP_NAME = 'ZUDM_LocationMining'
ZAPP_PROF = 'production'
ZCONF_DIR = '/zserver/rnd/apps/location_mining/spark-transforms/conf'

IN_DIR = '/zlogcentral_live/scribe'
IN_DIR_PARQUET = '/zlogcentral_live/scribe_parquet'

LM_DIR = OUT_DIR + '/location_mining'
TM_DIR = OUT_DIR + '/intermediate_data'
TMP_DIR = OUT_DIR + '/intermediate_data_parquet'
TMFT_DIR = TMP_DIR + '/ft'
IP_RANGE_DIR = OUT_DIR + '/IP2Location'

FORMAT_YMDH_DIR = '/%Y/%m/%d/%H'
FORMAT_YMD_DIR = '/%Y/%m/%d'

FORMAT_FOLDER_DIR = '/{folder}'
FORMAT_HOURLY_DIR = '/hourly' + FORMAT_FOLDER_DIR
FORMAT_D00_DIR = '/d00' + FORMAT_FOLDER_DIR
FORMAT_D01_DIR = '/d01' + FORMAT_FOLDER_DIR
FORMAT_D07_DIR = '/d07' + FORMAT_FOLDER_DIR
FORMAT_D30_DIR = '/d30' + FORMAT_FOLDER_DIR
FORMAT_W00_DIR = '/w00' + FORMAT_FOLDER_DIR
FORMAT_W01_DIR = '/w01' + FORMAT_FOLDER_DIR
FORMAT_W04_DIR = '/w04' + FORMAT_FOLDER_DIR
FORMAT_W09_DIR = '/w09' + FORMAT_FOLDER_DIR
FORMAT_UPLOAD_MARKER_DIR = '/UploadDB' + FORMAT_FOLDER_DIR
ZREADER_INT_DIR = '/zreader_intermediate'

UPLOAD_DONE_FILE = '/_UPLOAD_DONE.marker_file'

SPARK_SUBMIT = '/zserver/spark-2.2.0-bin-hadoop2.4/bin/spark-submit'
JAVA = local.get('/zserver/java/jdk1.8.0_11-x64/bin/java', 'java')




logger = logging.getLogger(__name__)
luigi.task.namespace(__name__)


class MetaTask:
    pass
    # def get_task_namespace(self):
    #     return self.task_namespace


class ZudmSparkSubmit(ZudmSparkSubmitTask, DDParamsMixin, MetaTask):
    spark_submit = SPARK_SUBMIT
    master = 'yarn'
    deploy_mode = 'cluster'
    queue = 'rnd'
    executor_memory = '4G'
    num_executors = 4
    executor_cores = 4
    worker_timeout = 86400

    _ZAPP_NAME = 'DeviceInfoMining'
    app = APP_JAR
    entry_class = ENTRY_CLASS
    duration = Duration.non

    zapp_name = ZAPP_NAME
    zapp_prof = ZAPP_PROF
    zconf_dir = ZCONF_DIR

    def app_options(self):
        inputs = ','.join([CLUSTER + path for path in self._inputs()])
        return [self.zclass_name, inputs, CLUSTER + self.temp_output_path]



class IP2LocationINTLog(ExternalLogsTask, MetaTask):
    sometime = dt.timedelta(hours=1)
    format_path = (TMP_DIR + FORMAT_D01_DIR + FORMAT_YMD_DIR)
    folder = 'IP2Location'
    zuser = 'zreader'


# class AnonymousIPLoc(ExternalLogsTask, MetaTask):
#     sometime = dt.timedelta(hours=1)
#     format_path = '/data/jobs/rnd/intermediate_data/global_apps_hourly/global_id_anonymous' + FORMAT_YMDH_DIR
#     folder = 'global_id_anonymous'
#     zuser = 'zte'


class ExtractAnonymousUserPath(ZudmSparkSubmit):
    ONE_DATE_AGO = (dt.datetime.today() - dt.timedelta(days=1))
    date = luigi.DateParameter(default=ONE_DATE_AGO)
    duration = Duration.d01
    format_path = (LM_DIR + FORMAT_D01_DIR + FORMAT_YMD_DIR)
    folder = 'external/anonymous_path'
    zclass_name = 'com.vng.zing.zudm_locationmining.externaltask.AnonymousUserD01Location'
    entry_class = 'com.vng.zing.zudm_locationmining.externaltask.AnonymousUserD01Location'

    def requires(self):
        dep_tuples = self.duration.dependencies(self.date, self.ONE_DATE_AGO)
        for duration, dateish in dep_tuples:
            if duration == 'basecase':
                yield RawExtract(date_hour=dateish)
                # _next_task(self, 'AnonymousIPLoc')(date=dateish)

        process_date = dt.datetime.combine(self.date, dt.time(hour=0))

        # check if Ip2Location is available

        yielded = False

        for i in range(25):
            checkingtask = _next_task(self, 'IP2LocationINTLog')(date=process_date - dt.timedelta(days=i))

            if checkingtask.output().exists():
                yielded = True
                yield _next_task(self, 'IP2LocationINTLog')(date=process_date - dt.timedelta(days=i))
                break

        if not yielded:
            yield _next_task(self, 'IP2LocationINTLog')(date=process_date)


    def app_options(self):
        rules = [('-anonymousdata', 'global_id_anonymous'), ('-ip2loc', 'IP2Location')]

        inputparam = parseInputlist(self._inputs(), rules, CLUSTER)
        return ['-appname', self.zclass_name] + inputparam + ['pdate', str(self.date), '-output', CLUSTER + self.temp_output_path]




class ExtractAnonymousUserPathV2(ZudmSparkSubmit):
    ONE_DATE_AGO = (dt.datetime.today() - dt.timedelta(days=1))
    date = luigi.DateParameter(default=ONE_DATE_AGO)
    duration = Duration.d01
    format_path = (LM_DIR + FORMAT_D01_DIR + FORMAT_YMD_DIR)
    folder = 'external/anonymous_path'
    zclass_name = 'com.vng.zing.zudm_locationmining.externaltask.AnonymousUserD01LocationV2'
    entry_class = 'com.vng.zing.zudm_locationmining.externaltask.AnonymousUserD01LocationV2'

    def requires(self):
        dep_tuples = self.duration.dependencies(self.date, self.ONE_DATE_AGO)
        for duration, dateish in dep_tuples:
            if duration == 'basecase':
                yield RawExtract(date_hour=dateish)
                # yield _next_task(self, 'AnonymousIPLoc')(date=dateish)

        process_date = dt.datetime.combine(self.date, dt.time(hour=0))

        # check if Ip2Location is available

        yielded = False

        for i in range(25):
            checkingtask = _next_task(self, 'IP2LocationINTLog')(date=process_date - dt.timedelta(days=i))

            if checkingtask.output().exists():
                yielded = True
                yield _next_task(self, 'IP2LocationINTLog')(date=process_date - dt.timedelta(days=i))
                break

        if not yielded:
            yield _next_task(self, 'IP2LocationINTLog')(date=process_date)


    def app_options(self):
        rules = [('-anonymousdata', 'global_id_anonymous'), ('-ip2loc', 'IP2Location')]

        inputparam = parseInputlist(self._inputs(), rules, CLUSTER)
        return ['-appname', self.zclass_name] + inputparam + ['pdate', str(self.date), '-output', CLUSTER + self.temp_output_path]



class ExtractAnonymousUserPathWrapper(luigi.WrapperTask, MetaTask):
    ONE_DATE_AGO = (dt.datetime.today() - dt.timedelta(days=1))
    date = luigi.DateParameter(default=ONE_DATE_AGO)
    def requires(self):

        for i in range(63):
            yield _next_task(self, 'ExtractAnonymousUserPathV2')(date=self.date - dt.timedelta(days=i))


class ExtractAnonymousUserPathD00(ZudmSparkSubmit):
    ONE_DATE_AGO = (dt.datetime.today() - dt.timedelta(days=1))
    date = luigi.DateParameter(default=ONE_DATE_AGO)
    duration = Duration.d01
    format_path = (LM_DIR + FORMAT_D00_DIR + FORMAT_YMD_DIR)
    folder = 'external/anonymous_path'
    zclass_name = 'com.vng.zing.zudm_locationmining.externaltask.AnonymousUserD00Location'
    entry_class = 'com.vng.zing.zudm_locationmining.externaltask.AnonymousUserD00Location'

    def requires(self):
        yield _next_task(self, 'ExtractAnonymousUserPath')(date=self.date)
        yield _next_task(self, 'ExtractAnonymousUserPathD00')(date=self.date - dt.timedelta(days=1))


    def app_options(self):
        rules = [('-anonymousdata', 'anonymous_path')]

        inputparam = parseInputlist(self._inputs(), rules, CLUSTER)
        return ['-appname', self.zclass_name] + inputparam + ['-output', CLUSTER + self.temp_output_path]



class ExtractAnonymousUserPathW09(ZudmSparkSubmit):
    ONE_DATE_AGO = (dt.datetime.today() - dt.timedelta(days=1))
    date = luigi.DateParameter(default=ONE_DATE_AGO)
    duration = Duration.d01
    format_path = (LM_DIR + FORMAT_W09_DIR + FORMAT_YMD_DIR)
    folder = 'external/anonymous_path'
    zclass_name = 'com.vng.zing.zudm_locationmining.externaltask.AnonymousUserD00Location'
    entry_class = 'com.vng.zing.zudm_locationmining.externaltask.AnonymousUserD00Location'

    def requires(self):

        for i in range(63):
            yield _next_task(self, 'ExtractAnonymousUserPath')(date=self.date - dt.timedelta(days=i))


    def app_options(self):
        rules = [('-anonymousdata', 'anonymous_path')]

        inputparam = parseInputlist(self._inputs(), rules, CLUSTER)
        return ['-appname', self.zclass_name] + inputparam + ['-output', CLUSTER + self.temp_output_path]



class AnonymousMostStayAreaW09(ZudmSparkSubmit):
    ONE_DATE_AGO = (dt.datetime.today() - dt.timedelta(days=1))
    date = luigi.DateParameter(default=ONE_DATE_AGO)
    duration = Duration.d01
    format_path = (LM_DIR + FORMAT_W09_DIR + FORMAT_YMD_DIR)
    folder = 'external/most_stay_area'
    zclass_name = 'com.vng.zing.zudm_locationmining.externaltask.GetMostStayArea'
    entry_class = 'com.vng.zing.zudm_locationmining.externaltask.GetMostStayArea'

    def requires(self):

        yield _next_task(self, 'ExtractAnonymousUserPathW09')(date=self.date)


    def app_options(self):
        rules = [('-anonymousdata', 'anonymous_path')]

        inputparam = parseInputlist(self._inputs(), rules, CLUSTER)
        return ['-appname', self.zclass_name] + inputparam + ['-output', CLUSTER + self.temp_output_path]


class AnonymousMostStayAreaW09V2(ZudmSparkSubmit):
    ONE_DATE_AGO = (dt.datetime.today() - dt.timedelta(days=1))
    date = luigi.DateParameter(default=ONE_DATE_AGO)
    duration = Duration.d01
    format_path = (LM_DIR + FORMAT_W09_DIR + FORMAT_YMD_DIR)
    folder = 'external/anonymous_mostvisited'
    zclass_name = 'com.vng.zing.zudm_locationmining.externaltask.GetMostStayAreaV2'
    entry_class = 'com.vng.zing.zudm_locationmining.externaltask.GetMostStayAreaV2'

    def requires(self):

        for i in range(63):
            yield _next_task(self, 'ExtractAnonymousUserPathV2')(date=self.date - dt.timedelta(days=i))


    def app_options(self):
        rules = [('-anonymousdata', 'anonymous_path')]

        inputparam = parseInputlist(self._inputs(), rules, CLUSTER)
        return ['-appname', self.zclass_name] + inputparam + ['-output', CLUSTER + self.temp_output_path]

class ExtractAnonymousUserPathV2id(ExternalLogsTask, MetaTask):
    sometime = dt.timedelta(hours=1)
    format_path = ('/data/jobs/rnd/location_mining/d01/external/identified_path' + FORMAT_YMD_DIR)
    folder = 'anonymous_path'
    zuser = 'zreader'


class ZaloMostStayConfidentCount(ZudmSparkSubmit):
    ONE_DATE_AGO = (dt.datetime.today() - dt.timedelta(days=1))
    date = luigi.DateParameter(default=ONE_DATE_AGO)
    duration = Duration.d01
    format_path = (LM_DIR + FORMAT_D01_DIR + FORMAT_YMD_DIR)
    folder = 'external/zalo_mostvisited_confident_count'
    zclass_name = 'com.vng.zing.zudm_locationmining.externaltask.GetConfidenceFromMostStayAreaCount'
    entry_class = 'com.vng.zing.zudm_locationmining.externaltask.GetConfidenceFromMostStayAreaCount'


    def requires(self):
        yield _next_task(self, 'ExtractAnonymousUserPathV2id')(date=self.date)


    def app_options(self):
        rules = [('-anonymousdata', 'identified_path')]

        inputparam = parseInputlist(self._inputs(), rules, CLUSTER)
        return ['-appname', self.zclass_name] + inputparam + ['-output', CLUSTER + self.temp_output_path]



class AnonymousMostStayConfidentCount(ZudmSparkSubmit):
    ONE_DATE_AGO = (dt.datetime.today() - dt.timedelta(days=1))
    date = luigi.DateParameter(default=ONE_DATE_AGO)
    duration = Duration.d01
    format_path = (LM_DIR + FORMAT_D01_DIR + FORMAT_YMD_DIR)
    folder = 'external/anonymous_mostvisited_confident_count'
    zclass_name = 'com.vng.zing.zudm_locationmining.externaltask.GetConfidenceFromMostStayAreaCount'
    entry_class = 'com.vng.zing.zudm_locationmining.externaltask.GetConfidenceFromMostStayAreaCount'


    def requires(self):
        yield _next_task(self, 'ExtractAnonymousUserPath')(date=self.date)


    def app_options(self):
        rules = [('-anonymousdata', 'anonymous_path')]

        inputparam = parseInputlist(self._inputs(), rules, CLUSTER)
        return ['-appname', self.zclass_name] + inputparam + ['-output', CLUSTER + self.temp_output_path]


class ZaloMostStayConfident(ZudmSparkSubmit):
    ONE_DATE_AGO = (dt.datetime.today() - dt.timedelta(days=1))
    date = luigi.DateParameter(default=ONE_DATE_AGO)
    duration = Duration.d01
    format_path = (LM_DIR + FORMAT_W09_DIR + FORMAT_YMD_DIR)
    folder = 'external/zalo_mostvisited_confident'
    zclass_name = 'com.vng.zing.zudm_locationmining.externaltask.GetConfidenceFromMostStayArea'
    entry_class = 'com.vng.zing.zudm_locationmining.externaltask.GetConfidenceFromMostStayArea'
    executor_cores = 8


    def requires(self):

        for i in range(63):
            yield _next_task(self, 'ZaloMostStayConfidentCount')(date=self.date - dt.timedelta(days=i))
            # yield _next_task(self, 'ExtractAnonymousUserPathV2id')(date=self.date - dt.timedelta(days=i))


    def app_options(self):
        rules = [('-anonymousdata', 'zalo_mostvisited_confident_count')]

        inputparam = parseInputlist(self._inputs(), rules, CLUSTER)
        return ['-appname', self.zclass_name] + inputparam + ['-output', CLUSTER + self.temp_output_path]


class AnonymousMostStayConfident(ZudmSparkSubmit):
    ONE_DATE_AGO = (dt.datetime.today() - dt.timedelta(days=1))
    date = luigi.DateParameter(default=ONE_DATE_AGO)
    duration = Duration.d01
    format_path = (LM_DIR + FORMAT_W09_DIR + FORMAT_YMD_DIR)
    folder = 'external/anonymous_mostvisited_confident'
    zclass_name = 'com.vng.zing.zudm_locationmining.externaltask.GetConfidenceFromMostStayArea'
    entry_class = 'com.vng.zing.zudm_locationmining.externaltask.GetConfidenceFromMostStayArea'
    executor_cores = 8


    def requires(self):

        for i in range(63):
            yield _next_task(self, 'AnonymousMostStayConfidentCount')(date=self.date - dt.timedelta(days=i))
            # yield _next_task(self, 'ExtractAnonymousUserPathV2id')(date=self.date - dt.timedelta(days=i))


    def app_options(self):
        rules = [('-anonymousdata', 'anonymous_mostvisited_confident_count')]

        inputparam = parseInputlist(self._inputs(), rules, CLUSTER)
        return ['-appname', self.zclass_name] + inputparam + ['-output', CLUSTER + self.temp_output_path]



class ExtractZaloUserPathWrapperCount(luigi.WrapperTask, MetaTask):
    ONE_DATE_AGO = (dt.datetime.today() - dt.timedelta(days=1))
    date = luigi.DateParameter(default=ONE_DATE_AGO)
    def requires(self):

        for i in range(63):
            yield _next_task(self, 'ZaloMostStayConfidentCount')(date=self.date - dt.timedelta(days=i))


class ExtractAnonymousUserPathWrapperCount(luigi.WrapperTask, MetaTask):
    ONE_DATE_AGO = (dt.datetime.today() - dt.timedelta(days=1))
    date = luigi.DateParameter(default=ONE_DATE_AGO)
    def requires(self):

        for i in range(63):
            yield _next_task(self, 'AnonymousMostStayConfidentCount')(date=self.date - dt.timedelta(days=i))


class ExternalLogsTaskDaily(luigi.ExternalTask, DDParamsMixin, metaclass=abc.ABCMeta):
    date = luigi.DateParameter(positional=False)
    duration = Duration.non
    zuser = 'zte'

    @property
    @abc.abstractmethod
    def folder(self):
        pass

    @property
    @abc.abstractmethod
    def format_path(self):
        pass

    @property
    @abc.abstractmethod
    def sometime(self):
        pass

    def output(self):
        path = self.dd_format(templated_string=self.format_path, folder=self.folder)
        print(path)
        return HadoopMasterTarget(path, self.zuser)

    def complete(self):
        return self.output().exists()


class LogActiveAnonymous(ExternalLogsTaskDaily, MetaTask):
    sometime = dt.timedelta(hours=1)
    format_path = ('/data/jobs/rnd/user_coverage/d01/active_AnonymousID' + FORMAT_YMD_DIR)
    folder = 'active_AnonymousID'
    zuser = 'zreader'


class SampleAnonymousMostStayConfident(ZudmSparkSubmit):
    ONE_DATE_AGO = (dt.datetime.today() - dt.timedelta(days=1))
    date = luigi.DateParameter(default=ONE_DATE_AGO)
    duration = Duration.d01
    format_path = (LM_DIR + FORMAT_W09_DIR + FORMAT_YMD_DIR)
    folder = 'external/anonymous_mostvisited_confident_sampled_'
    zclass_name = 'com.vng.zing.zudm_locationmining.externaltask.SampleAnonymous'
    entry_class = 'com.vng.zing.zudm_locationmining.externaltask.SampleAnonymous'
    executor_cores = 4


    def requires(self):

        yield _next_task(self, 'LogActiveAnonymous')(date=self.date)
        yield _next_task(self, 'AnonymousMostStayConfident')(date=self.date)


    def app_options(self):
        rules = [('-data', 'anonymous_mostvisited_confident'), ('-active', 'active_AnonymousID')]

        inputparam = parseInputlist(self._inputs(), rules, CLUSTER)
        return ['-appname', self.zclass_name] + inputparam + ['-output', CLUSTER + self.temp_output_path]

#External_Anonymous