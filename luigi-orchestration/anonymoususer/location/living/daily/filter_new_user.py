from location.living import External_Anonymous
from common.extract import DailyTask
import luigi
luigi.task.namespace(__name__)


class FilterNewUser(DailyTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.location.living.daily.FilterNewUser'
    template_path = '/data/jobs/rnd/location_mining/w09/external/{}/%Y/%m/%d/'
    folder = 'anonymous_mostvisited_daily'

    def requires(self):
        yield External_Anonymous.AnonymousMostStayAreaW09V2(date=self.date)