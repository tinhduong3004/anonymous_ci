from utility import evoke_task
from .filter_new_user import FilterNewUser
from common.upload import DailyTask
import luigi
import abc
import functools
import random
import tempfile
luigi.task.namespace(__name__)


class Upload(DailyTask):
    entry_class = 'com.vng.zing.anonymoususer.uploads.DailyLocationUploads'
    template_path = '/data/jobs/rnd/location_mining/w09/external/{}/marker_files/UPLOAD_%Y_%m_%d_DONE'
    folder = 'anonymous_mostvisited_daily'
    next_task = None

    def process_resources(self):
        resources = {"anonymoususer_db_put": 1}
        resources.update(self.resources)
        return resources

    def requires(self):
        yield evoke_task(self, 'ExternalPredictionMergeLogs')(date=self.date)


class ExternalPredictionMergeLogs(luigi.ExternalTask, metaclass=abc.ABCMeta):
    date = luigi.DateParameter(positional=False)

    def output(self):
        templated_path = ('data/jobs/rnd/location_mining/w09/external/'
                          'anonymous_mostvisited_daily/'
                          '%Y/%m/%d/')
        path = self.date.strftime(templated_path.format("anonymous_mostvisited_daily"))
        print("DEBUG: "+path)
        return luigi.contrib.hdfs.HdfsTarget(path)
      
    def complete(self):
        return self.output().exists()