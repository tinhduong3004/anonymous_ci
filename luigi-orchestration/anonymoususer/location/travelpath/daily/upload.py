from .. import FOLDER
from .ip2location_join import IP2LocationJoin
from anonymoususer.utility import evoke_task
from anonymoususer.common.upload import DailyTask
from anonymoususer.common.dir import hdfs
import luigi
import abc
luigi.task.namespace(__name__)


class Upload(DailyTask):
    entry_class = 'com.vng.zing.anonymoususer.uploads.DailyTravelPathUploads'
    template_path = hdfs.ANONYMOUS_HOME + 'inf/{}/marker_files/UPLOAD_%Y_%m_%d_%H_DONE'
    folder = FOLDER+"_with_location"
    next_task = None

    def process_resources(self):
        resources = {"upload_to_db_eventbus": 1}
        resources.update(self.resources)
        return resources

    def requires(self):
        yield evoke_task(self, 'ExternalHistoryLogs')(date=self.date)

class ExternalHistoryLogs(luigi.ExternalTask, metaclass=abc.ABCMeta):
    date = luigi.DateParameter(positional=False)

    def output(self):
        templated_path = ('/data/jobs/rnd/anonymoususer/inf/anonymous_travelpath_with_location/%Y/%m/%d/')
        path = self.date.strftime(templated_path.format("anonymous_travelpath_with_location"))
        print("DEBUG: "+path)
        return luigi.contrib.hdfs.HdfsTarget(path)
      
    def complete(self):
        return self.output().exists()
