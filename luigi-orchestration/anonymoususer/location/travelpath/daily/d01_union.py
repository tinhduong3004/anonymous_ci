from common.union import DailyD01Task
from .. import FOLDER
from ..hourly import IntermediateExtract
import luigi
luigi.task.namespace(__name__)

class D01Union(DailyD01Task):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.location.travelpath.daily.D01Union'
    folder = FOLDER
    next_task = IntermediateExtract