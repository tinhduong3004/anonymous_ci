from common.union import DailyInfTask
from .. import FOLDER
from .d01_union import D01Union
import datetime as dt
import luigi
luigi.task.namespace(__name__)


class InfUnion(DailyInfTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.location.travelpath.daily.D01Union'
    folder = FOLDER
    next_task = D01Union

    def requires(self):
        yesterday = self.date - dt.timedelta(days=1)
        yield InfUnion(date=yesterday)
        yield self.next_task(date=self.date)