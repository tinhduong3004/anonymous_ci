from common.union import DailyInfTask
from shared_external import Ip2Location
from .. import FOLDER
from .inf_union import InfUnion
import luigi
luigi.task.namespace(__name__)


class IP2LocationJoin(DailyInfTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.location.travelpath.daily.IP2LocationJoin'
    folder = FOLDER+"_with_location"
    next_task = InfUnion
    
    def requires(self):
        yield self.next_task(date=self.date)
        yield Ip2Location(date=self.date)