from .. import FOLDER
from common.extract import HourlyTask
from all.anonymous.hourly import TimelineExtract
import luigi
luigi.task.namespace(__name__)


class IntermediateExtract(HourlyTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.location.travelpath.hourly.IntermediateExtract'
    folder = FOLDER

    def requires(self):
        yield TimelineExtract(date_hour=self.date_hour)