from .anonymous import daily as anonymous
from .identified import daily as identified
import datetime as dt
import luigi
luigi.task.namespace(__name__)


class DailyMerge(luigi.task.WrapperTask):
    date = luigi.DateParameter(default=dt.datetime.now(), significant=False)

    def requires(self):
        yesterday = self.date - dt.timedelta(days=1)
        yield anonymous.D01Union(date=yesterday)
        yield anonymous.D01Union(date=self.date)
        yield identified.FilterChanges(date=yesterday)
        yield identified.FilterChanges(date=self.date)

class DailyUpload(luigi.task.WrapperTask):
    date = luigi.DateParameter(default=dt.datetime.now(), significant=False)

    def requires(self):
        yesterday = self.date - dt.timedelta(days=1)
        yield anonymous.Upload(date=yesterday)
        yield anonymous.Upload(date=self.date)
        yield identified.Upload(date=yesterday)
        yield identified.Upload(date=self.date)
