from common.extract import HourlyTask
from all.identified import hourly as identified
from ... import RawLog
from ... import FOLDER
import luigi
luigi.task.namespace(__name__)


class RawExtract(HourlyTask):
    folder = FOLDER+"_identified"
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.device.identified.hourly.RawExtract'

    def requires(self):
        yield identified.RawExtract(date_hour=self.date_hour)
        yield RawLog(date_hour=self.date_hour)