from ... import FOLDER
from utility import evoke_task
from .filter_changes import FilterChanges
from common.upload import DailyTask
from common.dir import hdfs
import luigi
import abc
luigi.task.namespace(__name__)


class Upload(DailyTask):
    priority = 1
    entry_class = 'com.vng.zing.anonymoususer.uploads.DailyZaloDeviceUploads'
    template_path = hdfs.ANONYMOUS_HOME + 'd01/{}/marker_files/UPLOAD_%Y_%m_%d_DONE'
    folder = FOLDER+"_identified_new"
    next_task = None

    def process_resources(self):
        resources = {"upload_to_db_eventbus": 1}
        resources.update(self.resources)
        return resources
    
    def requires(self):
        yield evoke_task(self, 'ExternalMergeLogs')(date=self.date)

class ExternalMergeLogs(luigi.ExternalTask, metaclass=abc.ABCMeta):
    date = luigi.DateParameter(positional=False)

    def output(self):
        templated_path = ('/data/jobs/rnd/anonymoususer/d01/'
                          'global_id_for_device_identified_new/'
                          '%Y/%m/%d/')
        path = self.date.strftime(templated_path.format("global_id_for_device_identified_new"))
        print("DEBUG: "+path)
        return luigi.contrib.hdfs.HdfsTarget(path)
     
    def complete(self):
        return self.output().exists()