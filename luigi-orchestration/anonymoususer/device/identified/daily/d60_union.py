from ... import FOLDER
from .d01_union import D01Union
from common.union import DailyD01Task
from common.dir import hdfs
import datetime as dt
import luigi
luigi.task.namespace(__name__)


class D60Union(DailyD01Task):
    folder = FOLDER+"_identified"
    num_executors = 8
    executor_cores = 8
    executor_memory = '8G'
    template_path = hdfs.ANONYMOUS_HOME + 'd60/{}/%Y/%m/%d/'
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.device.identified.daily.D60Union'
    next_task = D01Union
    def requires(self):
        for i in range(60):
            new_date = self.date - dt.timedelta(days=i)
            yield self.next_task(date=new_date)