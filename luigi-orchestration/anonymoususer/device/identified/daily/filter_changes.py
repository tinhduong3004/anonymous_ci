from ... import FOLDER
from .d60_union import D60Union
from common.union import DailyD01Task
from common.dir import hdfs
import datetime as dt
import luigi
luigi.task.namespace(__name__)


class FilterChanges(DailyD01Task):
    folder = FOLDER+"_identified_new"
    template_path = hdfs.ANONYMOUS_HOME + 'd01/{}/%Y/%m/%d/'
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.device.identified.daily.FilterChanges'
    next_task = D60Union

    def requires(self):
        yesterday = self.date - dt.timedelta(days=1)
        yield self.next_task(date=yesterday)
        yield self.next_task(date=self.date)