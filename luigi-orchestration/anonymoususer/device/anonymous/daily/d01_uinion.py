from ... import FOLDER
from ..hourly import RawExtract
from common.union import DailyD01Task
from common.dir import hdfs
import luigi
luigi.task.namespace(__name__)


class D01Union(DailyD01Task):
    folder = FOLDER+"_anonymous"
    template_path = hdfs.ANONYMOUS_HOME + 'd01/{}/%Y/%m/%d/'
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.device.anonymous.daily.D01Union'
    next_task = RawExtract
