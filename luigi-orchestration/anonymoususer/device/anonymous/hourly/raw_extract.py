from common.extract import HourlyTask
from all.anonymous import hourly as anonymous
from ... import RawLog
from ... import FOLDER
import luigi
luigi.task.namespace(__name__)


class RawExtract(HourlyTask):
    folder = FOLDER+"_anonymous"
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.device.anonymous.hourly.RawExtract'

    def requires(self):
        yield anonymous.RawExtract(date_hour=self.date_hour)
        yield RawLog(date_hour=self.date_hour)