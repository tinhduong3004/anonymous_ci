from common import external
from common.dir.hdfs import FALL_OVER_FOLDER
import luigi
luigi.task.namespace(__name__)


class RawLog(external.HourlyTask):
    folder = 'GLOBAL_ID_FOR_DEVICE'
    _fall_over_path = FALL_OVER_FOLDER + "global_id_for_device_empty"
