import random
import luigi.contrib.hdfs
import luigi.contrib.spark
luigi.task.namespace(__name__)


class HDFSTarget(luigi.contrib.hdfs.HdfsTarget):
    def temporary_path(target):
        class Manager(object):
            def __enter__(self):
                path = target.path.rstrip('/')
                num = random.randrange(0, 1e10)
                self.temp_path = path + '-luigi-tmp-{:010}'.format(num)
                return self.temp_path

            def __exit__(self, exc_type, exc_value, traceback):
                if exc_type is None:
                    # There were no exceptions
                    target.fs.rename_dont_move(self.temp_path, target.path)
                return False

        return Manager()