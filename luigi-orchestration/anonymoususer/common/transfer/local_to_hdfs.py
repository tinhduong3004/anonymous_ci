import abc
import os
from ..target import HDFSTarget
from ..dir import local
import luigi
luigi.task.namespace(__name__)

HADOOP_RESULT_DIR = '/data/jobs/rnd/anonymoususer/daily/'


class LocalToHDFS(luigi.Task, metaclass=abc.ABCMeta):
    date = luigi.DateParameter(positional=False)
    template_path = HADOOP_RESULT_DIR + '{}/%Y/%m/%d/' + local.OUPUT_DATA_FILE

    @property
    @abc.abstractmethod
    def folder(self):
        pass

    def run(self):
        os.system("hadoop fs -mkdir -p {}".format(os.path.dirname(self.output().path)))
        os.system("hadoop fs -put {} {}".format(self.input()[0].path, self.output().path))

    def output(self):
        path = self.date.strftime(self.template_path.format(self.folder))
        return HDFSTarget(path)
