import abc
from common.dir import local

import os
from datetime import datetime
import luigi
luigi.task.namespace(__name__)


class HDFSToLocal(luigi.Task, metaclass=abc.ABCMeta):
    date = luigi.DateParameter(positional=False)
    template_path = local.LOCAL_INPUT_DIR + '{}/%Y/%m/%d/'

    @property
    @abc.abstractmethod
    def next_task(self):
        pass

    @property
    @abc.abstractmethod
    def folder(self):
        pass

    @property
    def _hdfs_client(self):
        return luigi.contrib.hdfs.HdfsClient()

    def sanity_check(self, hadoop_path, local_path):
        hadoop_ls = set([os.path.basename(path) for path in self._hdfs_client.listdir(hadoop_path)])
        local_ls = set([os.path.basename(path) for path in os.listdir(local_path)])
        if len(hadoop_ls & local_ls) == len(hadoop_ls):
            return True
        return False

    def cmd_mkdir(self):
        command = " ".join(['mkdir -p ',
                            os.path.dirname(os.path.dirname(self.output().path))])
        os.system(command)

    def cmd_copy_to_local(self):
        command = " ".join(['hadoop fs -copyToLocal',
                            self.input()[0].path,
                            os.path.dirname(self.output().path)])
        os.system(command)

    def run(self):
        if not self.complete():
            self.cmd_mkdir()
            self.cmd_copy_to_local()
            if self.sanity_check(self.input()[0].path, os.path.dirname(self.output().path)):
                with self.output().open('w') as outfile:
                    print("Done {} ".format(datetime.now()), file=outfile)
            else:
                raise Exception("Hadoop files from {} not fully copied to {}".
                                format(str(self.input()[0].path),
                                       str(os.path.dirname(self.output().path))))

    def output(self):
        path = self.date.strftime(self.template_path.format(self.folder))+"LUIGI_SUCCESS.txt"
        return luigi.LocalTarget(path)

    def complete(self):
        return self.output().exists()

    def requires(self):
        yield self.next_task(date=self.date)



