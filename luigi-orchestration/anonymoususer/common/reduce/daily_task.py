import abc
import os
import glob
import luigi
from ..dir import local
luigi.task.namespace(__name__)
DATA_PARTITIONS = 128


class DailyTask(luigi.Task, metaclass=abc.ABCMeta):
    date = luigi.DateParameter(positional=False)

    @property
    @abc.abstractmethod
    def next_task(self):
        pass

    def requires(self):
        yield self.next_task(date=self.date)

    def run(self):
        local_input_path = os.path.dirname(self.input()[0].path)
        filenames = glob.glob(os.path.dirname(self.input()[0].path) + '/part*.csv')
        assert len(filenames) == DATA_PARTITIONS
        try:
            os.system("cat {}/part*csv > {}/data_temp.csv".format(local_input_path, local_input_path))
            os.rename(local_input_path + "/data_temp.csv", self.output().path)
            for part in filenames:
                if os.path.isfile(part):
                    os.remove(part)
                else:
                    raise FileNotFoundError("Part {} not found!".format(part))
        except:
            raise Exception("Hadoop to local failed!")

    def output(self):
        return luigi.LocalTarget(os.path.dirname(self.input()[0].path) + '/'+local.INPUT_DATA_FILE)
