import functools
import luigi.contrib.hdfs
import luigi.contrib.spark
from luigi import Event
from .alert import alert
luigi.task.namespace(__name__)


class SparkSubmitTask(luigi.contrib.spark.SparkSubmitTask):
    queue = 'rnd'
    num_executors = 4
    executor_memory = '4G'
    deploy_mode = 'cluster'
    master = 'yarn'
    spark_submit = '/zserver/spark-2.2.0-bin-hadoop2.4/bin/spark-submit'
    app = '/zserver/rnd/apps/anonymoususer/spark-transforms.jar'
      
    @functools.lru_cache()
    def _inputs(self):
        return [input.path for input in self.input() if input.exists()]
      
    def run(self):
        if not self.output().exists():
            with self.output().temporary_path() as self.temp_output_path:
                if self._inputs(): # Only run when it valid
                    super().run()
                else:
                    raise Exception("Input paths does not exists!"+str(self.input()))
      
    def app_options(self):
        inputs = ','.join(self._inputs())
        return ['--input', inputs, '--output', self.temp_output_path]


# @SparkSubmitTask.event_handler(Event.FAILURE)
# def failed_alert(task, exception):
#     """
#     send alert on success
#     :param task:
#     :param exception:
#     :return:
#     """
#     alert("Failed Spark Task: " + str(task.__class__.__name__) + ": "+exception)