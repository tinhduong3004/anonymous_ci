from alertbot.alert_client import AlertClient


def alert(message):
    """
    Call alert bot api
    :param message: alert message will be send to user/user group
    :return:
    """
    group_name = "QUYDM"
    client = AlertClient()
    result = client.alert_to_group(group_name, message)
    print("Alert action result: ", result)