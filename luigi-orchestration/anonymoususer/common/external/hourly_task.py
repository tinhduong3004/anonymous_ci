import luigi
import datetime as dt
import abc
from ..alert import alert
from ..target import HDFSTarget
from common.dir import hdfs

luigi.task.namespace(__name__)


class HourlyTask(luigi.ExternalTask, metaclass=abc.ABCMeta):
    date_hour = luigi.DateHourParameter(positional=False)
    template_path = hdfs.SCRIBE_PARQUET + '{}/%Y/%m/%d/%H/'

    # In case the raw log weren't produced in more than 3 hours, we will ignore it by using fallover path
    @property
    @abc.abstractmethod
    def _fall_over_path(self):
        pass

    @property
    @abc.abstractmethod
    def folder(self):
        pass

    def output(self):
        path = self.date_hour.strftime(self.template_path.format(self.folder))
        rnd_target = HDFSTarget(path)
        if rnd_target.exists() or self.date_hour + dt.timedelta(minutes=180) > dt.datetime.today():
            return rnd_target
        else:
            alert("Missing external log: " + path)
            return HDFSTarget(self._fall_over_path)

    def complete(self):
        if self.date_hour + dt.timedelta(minutes=90) <= dt.datetime.today():  # it needs time to produce the log
            return self.output().exists()
        return False
