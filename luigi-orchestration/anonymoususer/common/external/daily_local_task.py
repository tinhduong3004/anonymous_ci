import luigi
import abc
from ..dir import local
luigi.task.namespace(__name__)


class DailyLocalTask(luigi.ExternalTask, metaclass=abc.ABCMeta):
    date = luigi.DateParameter(positional=False)
    template_path = local.LOCAL_DIR + '{}/%Y/%m/%d/'+local.OUPUT_DATA_FILE

    @property
    @abc.abstractmethod
    def folder(self):
        pass

    def output(self):
        path = self.date.strftime(self.template_path.format(self.folder))
        return luigi.LocalTarget(path)

    def complete(self):
        return self.output().exists()
