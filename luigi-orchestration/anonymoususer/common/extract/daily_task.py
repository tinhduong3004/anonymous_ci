import luigi
import abc
from .. import SparkSubmitTask
from ..target import HDFSTarget
from common.dir import hdfs

luigi.task.namespace(__name__)


class DailyTask(SparkSubmitTask, metaclass=abc.ABCMeta):
    date = luigi.DateParameter(positional=False)
    template_path = hdfs.ANONYMOUS_HOME + '{}/%Y/%m/%d/'

    @property
    def entry_class(self):
        pass

    @property
    @abc.abstractmethod
    def folder(self):
        pass

    def output(self):
        path_with_folder = self.template_path.format(self.folder)
        complete_path = self.date.strftime(path_with_folder)
        return HDFSTarget(complete_path)


