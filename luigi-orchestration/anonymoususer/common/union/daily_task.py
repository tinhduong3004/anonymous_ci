import abc
import luigi
from .. import SparkSubmitTask
from ..target import HDFSTarget
luigi.task.namespace(__name__)


class DailyTask(SparkSubmitTask, metaclass=abc.ABCMeta):
    date = luigi.DateParameter(positional=False)

    @property
    @abc.abstractmethod
    def entry_class(self):
        pass

    @property
    @abc.abstractmethod
    def next_task(self):
        pass

    @property
    @abc.abstractmethod
    def template_path(self):
        pass

    @property
    @abc.abstractmethod
    def priority(self):
        pass

    @property
    @abc.abstractmethod
    def folder(self):
        pass

    def output(self):
        path = self.date.strftime(self.template_path.format(self.folder))
        return HDFSTarget(path)