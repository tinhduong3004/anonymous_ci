import abc
from .daily_task import DailyTask
from common.dir import hdfs
import luigi
luigi.task.namespace(__name__)


class DailyInfTask(DailyTask, metaclass=abc.ABCMeta):
    priority = 1
    template_path = hdfs.ANONYMOUS_HOME + 'inf/{}/%Y/%m/%d/'
