import abc
from .daily_task import DailyTask
from common.dir import hdfs
import datetime as dt
import luigi
luigi.task.namespace(__name__)


class DailyD14Task(DailyTask, metaclass=abc.ABCMeta):
    priority = 2
    template_path = hdfs.ANONYMOUS_HOME + 'daily/{}/%Y/%m/%d/'
    num_executors = 8
    executor_memory = '8G'

    def requires(self):
        date_hour = dt.datetime.combine(self.date, dt.time(hour=23))
        for d in range(14):  # It need to be refactored
            for h in range(24):
                previous_hour = date_hour - dt.timedelta(days=d, hours=h)
                yield self.next_task(date_hour=previous_hour)