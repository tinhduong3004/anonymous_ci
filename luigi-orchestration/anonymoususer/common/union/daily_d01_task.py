import abc
from .daily_task import DailyTask
from common.dir import hdfs
import datetime as dt
import luigi
luigi.task.namespace(__name__)


class DailyD01Task(DailyTask, metaclass=abc.ABCMeta):
    priority = 1
    template_path = hdfs.ANONYMOUS_HOME + 'd01/{}/%Y/%m/%d/'

    def requires(self):
        for i in range(24):
            new_date_hour = dt.datetime.combine(self.date, dt.time(hour=i))
            yield self.next_task(date_hour=new_date_hour)