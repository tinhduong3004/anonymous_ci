import abc
import luigi
import functools
from luigi import Event
from ..alert import alert
from ..target import HDFSTarget
luigi.task.namespace(__name__)


class DailyTask(luigi.contrib.spark.SparkSubmitTask, metaclass=abc.ABCMeta):
    date = luigi.DateParameter(positional=False)

    queue = 'rnd'
    deploy_mode = 'cluster'
    master = 'yarn'
    app = '/zserver/rnd/apps/anonymoususer/database-uploads.jar'
    num_executors = 4
    executor_memory = '4G'

    @abc.abstractmethod
    def process_resources(self):
        pass

    @property
    def template_path(self):
        pass

    @property
    def entry_class(self):
        pass

    @property
    @abc.abstractmethod
    def folder(self):
        pass

    @property
    @abc.abstractmethod
    def next_task(self):
        pass

    def run(self):
        if self._inputs():  # Only run when it valid
            super().run()
            self._touchz()
        else:
            raise Exception("Input paths does not exists!" + str(self.input()))

    @functools.lru_cache()
    def _inputs(self):
        return [input.path for input in self.input() if input.exists()]

    def app_options(self):
        inputs = ','.join(self._inputs())
        return [inputs]

    def _touchz(self):
        """
        This shouldbe provided by luigi I think. But whatever.
        """
        luigi.contrib.hdfs.HdfsClient().touchz(self.output().path)

    def output(self):
        path = self.date.strftime(self.template_path.format(self.folder))
        return HDFSTarget(path)

    def requires(self):
        yield self.next_task(date=self.date)

#
# @DailyTask.event_handler(Event.FAILURE)
# def failed_alert(task, exception):
#     """
#     send alert on success
#     :param task:
#     :param exception:
#     :return:
#     """
#     alert("Failed Spark Task: " + str(task.__class__.__name__) + " at " + str(task.date)+": "+exception)