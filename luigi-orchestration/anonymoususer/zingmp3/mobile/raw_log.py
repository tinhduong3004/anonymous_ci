from common import external
from common.dir.hdfs import FALL_OVER_FOLDER
import luigi
luigi.task.namespace(__name__)


class RawLog(external.HourlyTask):
    folder = 'ZMP3_API_STATS'
    _fall_over_path = FALL_OVER_FOLDER + "zmp3_api_stats_empty"