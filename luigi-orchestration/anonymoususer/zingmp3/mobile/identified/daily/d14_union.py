from common.union import DailyD14Task
from ..hourly import RawExtract
import datetime as dt
import luigi
from .config import BASE_FOLDER
luigi.task.namespace(__name__)


class D14Union(DailyD14Task):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.zingmp3.identified.daily.Union'
    folder = BASE_FOLDER
    next_task = RawExtract

    def requires(self):
        date_hour = dt.datetime.combine(self.date, dt.time(hour=23))
        require_range = 6
        for d in reversed(range(require_range)):  # TODO: Remove later
            for h in reversed(range(24)):
                previous_hour = date_hour - dt.timedelta(days=d, hours=h)
                yield RawExtract(date_hour=previous_hour)


