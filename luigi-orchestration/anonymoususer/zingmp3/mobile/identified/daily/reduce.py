from common.reduce import DailyTask
from .copy_to_local import CopyToLocal
import luigi
luigi.task.namespace(__name__)


class Reduce(DailyTask):
    next_task = CopyToLocal
