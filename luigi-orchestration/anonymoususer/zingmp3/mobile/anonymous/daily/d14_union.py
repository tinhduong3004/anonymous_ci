from common.union import DailyD14Task
from ..hourly import RawExtract
import luigi
import datetime as dt
from .config import BASE_FOLDER
luigi.task.namespace(__name__)


class D14Union(DailyD14Task):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.zingmp3.anonymous.daily.Union'
    folder = BASE_FOLDER
    next_task = RawExtract

    def requires(self):
        date_hour = dt.datetime.combine(self.date, dt.time(hour=23))
        require_range = 14
        for d in reversed(range(require_range)):
            for h in reversed(range(24)):
                previous_hour = date_hour - dt.timedelta(days=d, hours=h)
                yield RawExtract(date_hour=previous_hour)