from ... import FOLDER
from ... import RawLog
from common import extract
from all.anonymous import hourly as anonymous
import luigi
luigi.task.namespace(__name__)


class RawExtract(extract.HourlyTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.zingmp3.anonymous.hourly.RawExtractMobile'
    folder = FOLDER+"_anonymous"

    def requires(self):
        yield anonymous.RawExtract(date_hour=self.date_hour)
        yield RawLog(date_hour=self.date_hour)
