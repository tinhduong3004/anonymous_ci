from .anonymous import hourly as anonymous_hourly
from .identified import hourly as identified_hourly
import datetime as dt
import luigi
luigi.task.namespace(__name__)


class HourlyExtract(luigi.task.WrapperTask):
    date_hour = luigi.DateHourParameter(default=dt.datetime.now(), significant=False)
    def requires(self):
        for i in range(24 * 60):
            previous_hour = self.date_hour - dt.timedelta(hours=i)
            yield anonymous_hourly.RawExtract(date_hour=previous_hour)
            yield identified_hourly.RawExtract(date_hour=previous_hour)
