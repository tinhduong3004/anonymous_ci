from ..hourly import RawExtract
from common.union import DailyD01Task
import luigi
luigi.task.namespace(__name__)


class StatUnion(DailyD01Task):
    folder = "anonymous_zalo_mapping_stat"
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.gidmap.daily.StatUnion'
    next_task = RawExtract