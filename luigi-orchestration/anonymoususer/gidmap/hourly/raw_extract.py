from .. import FOLDER
from .. import RawLog
from common import extract
import luigi
luigi.task.namespace(__name__)


class RawExtract(extract.HourlyTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.gidmap.hourly.RawExtract'
    folder = FOLDER

    def requires(self):
        yield RawLog(date_hour=self.date_hour)