from .. import FOLDER
from .. import RawRestore
from common import extract
import luigi
luigi.task.namespace(__name__)


class RestoreExtract(extract.HourlyTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.gidmap.hourly.RawExtract'
    folder = FOLDER
    num_executors = 8

    def requires(self):
        yield RawRestore(date_hour=self.date_hour)
    
    def process_resources(self):
        resources = {"gid_map": 1}
        resources.update(self.resources)
        return resources