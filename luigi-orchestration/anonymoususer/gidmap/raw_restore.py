from common import external
import luigi
luigi.task.namespace(__name__)


class RawRestore(external.HourlyTask):
    folder = 'GID_MAPPING_ZALO_ANONYMOUS'
    _fall_over_path = ""

    def output(self):
        template_path = '/restore_logs/bigbook/{}/%Y/%m/%d/%H/'
        path = self.date_hour.strftime(template_path.format(self.folder))
        return luigi.contrib.hdfs.HdfsTarget(path)
