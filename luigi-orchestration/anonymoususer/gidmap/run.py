from .daily import StatUnion
from .hourly import RestoreExtract
import datetime as dt
import luigi
luigi.task.namespace(__name__)


class DailyUnion(luigi.task.WrapperTask):
    date = luigi.DateParameter(default=dt.datetime.now(), significant=False)

    def requires(self):
        yield StatUnion(date=self.date)
        yesterday = self.date - dt.timedelta(days=1)
        yield StatUnion(date=yesterday)


class HourlyRestore(luigi.task.WrapperTask):
    date_hour = luigi.DateHourParameter(default=dt.datetime.now(), significant=False)

    def requires(self):
        for i in range(24*30):
            previous_hour = self.date_hour - dt.timedelta(hours=i)
            yield RestoreExtract(date_hour=previous_hour)