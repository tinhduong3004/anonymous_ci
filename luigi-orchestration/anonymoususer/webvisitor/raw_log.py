from common import external
from common.dir.hdfs import FALL_OVER_FOLDER
import luigi
luigi.task.namespace(__name__)


class RawLog(external.HourlyTask):
    folder = 'CENTRALIZED_GET_VISITOR'
    _fall_over_path = FALL_OVER_FOLDER + "centralized_get_visitor_empty"