from .anonymous import hourly as anonymous
from .identified import hourly as identified
import datetime as dt
import luigi
luigi.task.namespace(__name__)


class HourlyExtract(luigi.task.WrapperTask):
    date_hour = luigi.DateHourParameter(default=dt.datetime.now(), significant=False)
    
    def requires(self):
        self_date = self.date_hour.date() - dt.timedelta(days=1)
        for i in range(24):
            # previous_hour = self.date_hour - dt.timedelta(hours=i)
            previous_hour = dt.datetime.combine(self_date, dt.time(hour=i))
            yield anonymous.RawExtract(date_hour=previous_hour)
            yield identified.RawExtract(date_hour=previous_hour)
