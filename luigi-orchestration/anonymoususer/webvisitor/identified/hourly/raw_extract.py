from ... import FOLDER
from ... import RawLog
from common import extract
from all.identified import hourly as identified
import luigi
luigi.task.namespace(__name__)


class RawExtract(extract.HourlyTask):
    entry_class = 'com.vng.zalo.zte.rnd.anonymoususer.webvisitor.identified.hourly.RawExtract'
    folder = FOLDER+"_identified"

    def requires(self):
        yield identified.DemographicJoin(date_hour=self.date_hour)
        yield RawLog(date_hour=self.date_hour)