#!/bin/zsh
set -xe
STAGING_MACHINE=zdeploy@10.30.80.18
PRODUCTION_MACHINE=rnd@10.30.22.176
PROJECT_NAME="anonymoususer"

[[ $(basename $PWD) == "anonymoususer" ]]

STAGING_MACHINE="${STAGING_MACHINE:-zdeploy@10.30.80.18}"
UPLOADDIR="/zserver/java-projects/zudm/"$PROJECT_NAME
DESTURL="${STAGING_MACHINE}:${UPLOADDIR}"

RSYNC=rsync
SSH=(ssh $STAGING_MACHINE)
MKDIR=($SSH mkdir -p)

$MKDIR $UPLOADDIR

# Uploading luigi source code
if [[ -d luigi-orchestration ]]; then
    $RSYNC -r --delete luigi-orchestration "${DESTURL}/"
else
    notfound Luigi
fi

# Uploading spark transformer
if [[ -d spark-transforms ]]; then
    (   
        set -e 
        cd spark-transforms
        sbt -no-global 'set test in assembly := {}' assembly
        # $MKDIR "${UPLOADDIR}/spark-transforms"
        $RSYNC target/scala-2.11/spark-transforms-assembly-*.jar "${DESTURL}/spark-transforms.jar"
    )
else
    notfound spark-transforms
fi

#Uploading cron files
if [[ -d crond-schedules ]]; then
    $RSYNC -r --delete crond-schedules "${DESTURL}/"
else
    notfound crond
fi


#Setting up cron
ssh $PRODUCTION_MACHINE /home/rnd/rsync_$PROJECT_NAME.sh
