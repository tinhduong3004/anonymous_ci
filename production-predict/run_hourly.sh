#!/usr/bin/env bash
cd /data/zmining/.anonymous/scripts
DATE=`date '+%Y-%m-%d_%H_%M_%S'`
/home/zdeploy/anaconda3/bin/python hourly.py 2>&1 | tee ../logs/run_hourly_$DATE

