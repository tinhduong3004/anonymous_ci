import datetime

LOCAL_ANONYMOUS_PATH = '/data/zmining/.anonymous/'
LOCAL_INPUT_RELATIVE = 'data/in/'
LOCAL_OUTPUT_RELATIVE = 'data/out/'
FLAG_NAME = '.flag.txt'
OUTPUT_SUCCESS_NAME = 'result.csv'

ADDRESS_22_176 = '10.30.22.176::rnd/.anonymous/'
INPUT_22_176_RELATIVE = 'data/zep_baomoi_tracking_anonymous_production/'
OUTPUT_22_176_RELATIVE = 'data/predicts/'
LUIGI_SUCCESS_NAME = 'LUIGI_SUCCESS.txt'

MAX_HOURS = 24
DATA_TIMESTEP = datetime.timedelta(hours=1)
TIME_PATTERN = "%Y_%m_%d_%H_%M_%S"
MAX_BUSY_TIME = datetime.timedelta(minutes=45)


# TRAIN

LOCAL_TRAIN_RELATIVE = 'data/'
INPUT_22_176_TRAIN_RELATIVE = 'data/zep_baomoi_tracking_anonymous_production/'