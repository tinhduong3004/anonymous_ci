from __future__ import print_function
import hourly_config as configs
import datetime
import logging
import glob
import os
import re

logging.basicConfig(
    format='%(asctime)s %(levelname)s %(module)s - %(message)s ',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger()

COMMAND_RSYNC_GET = ' '.join([
    './rsync.sh',
    configs.ADDRESS_22_176 + configs.INPUT_22_176_TRAIN_RELATIVE,
    configs.LOCAL_ANONYMOUS_PATH + configs.LOCAL_TRAIN_RELATIVE
])


def run_jobs():
    logger.info("Start run hourly anonymous jobs!")
    set_flag('BUSY')
    rsync_get_data()
    schedule()
    set_flag('FREE')


def schedule():
    input_times = inputs_candidates()
    logger.info("Start predict with inputs: "+str(input_times))
    predict(input_times)


def predict(inputs):
    for time in inputs:
        input_path = configs.LOCAL_ANONYMOUS_PATH + configs.LOCAL_INPUT_RELATIVE + time
        output_path = configs.LOCAL_ANONYMOUS_PATH + configs.LOCAL_OUTPUT_RELATIVE + time
        csv_inputs = glob.glob(input_path+"/*.csv")
        if len(csv_inputs)>0:
            input_path = csv_inputs[0]
        command_predict = ' '.join([
            'bash predict.sh',
            input_path,
            output_path + configs.OUTPUT_SUCCESS_NAME
            ])
        os.system("mkdir -p "+output_path)
        os.system(command_predict)


def inputs_candidates():
    time_exp = r'\d{4}/\d{2}/\d{2}/\d{2}/'
    inputs = glob.glob(configs.LOCAL_ANONYMOUS_PATH+configs.LOCAL_INPUT_RELATIVE+"*/*/*/*/")
    inputs = [re.findall(time_exp, path)[0] for path in inputs if len(re.findall(time_exp, path)) > 0]
    recents = []
    now = datetime.datetime.now()
    for i in range(configs.MAX_HOURS):
        past = now - configs.DATA_TIMESTEP * i
        recents.append(configs.LOCAL_ANONYMOUS_PATH
                       + configs.LOCAL_INPUT_RELATIVE
                       + "{:04d}/{:02d}/{:02d}/{:02d}/".format(past.year, past.month, past.day, past.hour))
    recents = [re.findall(time_exp, path)[0] for path in recents if len(re.findall(time_exp, path)) > 0]
    inputs = set(inputs) & set(recents)
    inputs = [x for x in inputs if os.path.exists(configs.LOCAL_ANONYMOUS_PATH
                                                  + configs.LOCAL_INPUT_RELATIVE
                                                  + x
                                                  + configs.LUIGI_SUCCESS_NAME)]
    outputs = glob.glob(configs.LOCAL_ANONYMOUS_PATH+configs.LOCAL_OUTPUT_RELATIVE+"*/*/*/*/")
    outputs = [re.findall(time_exp, path)[0] for path in outputs if len(re.findall(time_exp, path)) > 0]
    outputs = [x for x in outputs if os.path.exists(configs.LOCAL_ANONYMOUS_PATH
                                                    + configs.LOCAL_OUTPUT_RELATIVE
                                                    + x
                                                    + configs.OUTPUT_SUCCESS_NAME)]
    inputs = [inp for inp in inputs if inp not in outputs]
    return sorted(inputs)


def rsync_get_data():
    os.system(COMMAND_RSYNC_GET)


def check_flag_status():
    recover_flag()
    with open(configs.LOCAL_ANONYMOUS_PATH+configs.FLAG_NAME, 'r') as f:
        flag = f.read().split()
        logger.info("Flag " + str(flag))
        if len(flag) == 2:
            if flag[0] == 'BUSY':
                last_time = datetime.datetime.strptime(flag[1], configs.TIME_PATTERN)
                if datetime.datetime.now() - last_time > configs.MAX_BUSY_TIME:
                    logger.info("Flag taken too long from {} > {} . Start recover flag and run!".format(
                        str(last_time),
                        str(configs.MAX_BUSY_TIME))
                    )
                    return -1
                return 0
            elif flag[0] == 'FREE':
                return 1
    return -1


def recover_flag():
    if not os.path.exists(configs.LOCAL_ANONYMOUS_PATH+configs.FLAG_NAME):
        os.system("mkdir -p "+configs.LOCAL_ANONYMOUS_PATH)
        with open(configs.LOCAL_ANONYMOUS_PATH+configs.FLAG_NAME, 'w') as f:
            print("FREE", file=f)


def set_flag(status):
    if status in ['FREE', 'BUSY']:
        with open(configs.LOCAL_ANONYMOUS_PATH+configs.FLAG_NAME, 'w') as f:
            print(status+" "+datetime.datetime.now().strftime(configs.TIME_PATTERN), file=f)
    else:
        raise ValueError("Flag status must be FREE or BUSY. Got {} instead. ".format(status))


if __name__ == "__main__":
    flag_status = check_flag_status()
    if flag_status == 1:
        run_jobs()
    elif flag_status == -1:
        recover_flag()
        run_jobs()
    else:
        logger.info("Flag taken. Not run anything.")
