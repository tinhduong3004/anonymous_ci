#!/usr/bin/env bash
echo -e "rsync.sh $1 -> $2"
rsync -aurv $1 $2 --bwlimit=20000